<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::create ( 'meal_posts', function ( Blueprint $table ) {
            $table -> id ();
            $table -> unsignedInteger ( 'book_meal_id' );
            $table -> string ( 'title' ) -> nullable ();
            $table -> text ( 'body' ) -> nullable ();
            $table -> string ( 'unique_id' ) -> nullable ();
            $table -> timestamps ();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::dropIfExists ( 'meal_posts' );
    }
}
