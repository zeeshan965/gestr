<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnsFromMealDateSlots extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( Schema ::hasColumn ( 'meal_date_slots', 'needed' ) ) {
            Schema ::table ( 'meal_date_slots', function ( Blueprint $table ) {
                $table -> dropColumn ( 'needed' );
            } );
        }
        if ( Schema ::hasColumn ( 'meal_date_slots', 'category' ) ) {
            Schema ::table ( 'meal_date_slots', function ( Blueprint $table ) {
                $table -> dropColumn ( 'category' );
            } );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
    }
}
