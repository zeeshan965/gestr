<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::create ( 'volunteers', function ( Blueprint $table ) {
            $table -> id ();
            $table -> unsignedInteger ( 'slot_id' ) -> nullable ();
            $table -> unsignedInteger ( 'user_id' ) -> nullable ();
            $table -> string ( 'unique_id' ) -> nullable ();
            $table -> text ( 'body' ) -> nullable ();
            $table -> text ( 'assignedUser' ) -> nullable ();
            $table -> string ( 'note' ) -> nullable ();
            $table -> tinyInteger ( 'one_day_reminder' ) ->default ( 0 );
            $table -> timestamps ();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::dropIfExists ( 'volunteers' );
    }
}
