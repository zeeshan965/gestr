<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilepathColumnInBookMeals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::table ( 'book_meals', function ( Blueprint $table ) {
            $table -> text ( 'body' ) -> nullable ();
        } );

        if ( ! Schema ::hasColumn ( 'book_meals', 'photo' ) ) {
            Schema ::table ( 'book_meals', function ( Blueprint $table ) {
                $table -> string ( 'photo' ) -> nullable ();
            } );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::table ( 'book_meals', function ( Blueprint $table ) {
            $table -> dropColumn ( 'body' );
        } );
        if ( Schema ::hasColumn ( 'book_meals', 'photo' ) ) {
            Schema ::table ( 'book_meals', function ( Blueprint $table ) {
                $table -> dropColumn ( 'photo' );
            } );
        }
    }
}
