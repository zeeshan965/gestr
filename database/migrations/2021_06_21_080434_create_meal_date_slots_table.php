<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMealDateSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::create ( 'meal_date_slots', function ( Blueprint $table ) {
            $table -> increments ( 'id' );
            $table -> string ( 'dateslots' ) -> nullable ();
            $table -> unsignedInteger ( 'mt_pack_id' ) -> nullable ();
            $table -> string ( 'category' ) -> nullable ();
            $table -> string ( 'needed' ) -> nullable ();
            $table -> timestamps ();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::dropIfExists ( 'meal_date_slots' );
    }
}
