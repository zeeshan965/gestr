<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsInBookMealTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::table ( 'book_meals', function ( Blueprint $table ) {
            $table -> tinyInteger ( 'intro' ) ->default ( 0 );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::table ( 'book_meals', function ( Blueprint $table ) {
            $table -> dropColumn ( 'intro' );
        } );
    }
}
