<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewTableForMealBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::create ( 'book_meals', function ( Blueprint $table ) {
            $table -> increments ( 'id' );
            $table -> string ( 'unique_id' ) -> nullable ();
            $table -> text ( 'url' );
            $table -> unsignedInteger ( 'organizer' ) -> nullable ();
            $table -> string ( 'recipient_name' ) -> nullable ();
            $table -> string ( 'recipient_email' ) -> nullable ();
            $table -> string ( 'recipient_address', 255 ) -> nullable ();
            $table -> string ( 'recipient_city' ) -> nullable ();
            $table -> string ( 'recipient_state' ) -> nullable ();
            $table -> string ( 'recipient_postal_code' ) -> nullable ();
            $table -> string ( 'recipient_phone' ) -> nullable ();
            $table -> integer ( 'adults_cook_for' ) -> nullable ();
            $table -> integer ( 'kids_cook_for' ) -> nullable ();
            $table -> string ( 'delivery_time' ) -> nullable ();

            $table -> text ( 'special_instructions' ) -> nullable ();
            $table -> text ( 'fave_meals_rest' ) -> nullable ();
            $table -> text ( 'least_fave_meals' ) -> nullable ();
            $table -> text ( 'restrictions' ) -> nullable ();
            $table -> string ( 'package_name' ) -> nullable ();
            $table -> string ( 'package_payment' ) -> nullable ();
            $table -> timestamps ();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::dropIfExists ( 'book_meals' );
    }
}
