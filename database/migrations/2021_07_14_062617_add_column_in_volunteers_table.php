<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInVolunteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::table ( 'volunteers', function ( Blueprint $table ) {
            $table -> tinyInteger ( 'one_week_reminder' ) ->default ( 0 );
            $table -> unsignedInteger ( 'event_id' ) -> nullable ();
        } );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::table ( 'volunteers', function ( Blueprint $table ) {
            $table -> dropColumn ( 'one_week_reminder' );
            $table -> dropColumn ( 'event_id' );
        } );
    }
}
