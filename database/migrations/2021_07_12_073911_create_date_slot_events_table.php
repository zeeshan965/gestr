<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDateSlotEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::create ( 'date_slot_events', function ( Blueprint $table ) {
            $table -> id ();
            $table -> unsignedInteger ( 'date_slot_id' ) -> nullable ();
            $table -> string ( 'category' ) -> nullable ();
            $table -> string ( 'needed' ) -> nullable ();
            $table -> timestamps ();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::dropIfExists ( 'date_slot_events' );
    }
}
