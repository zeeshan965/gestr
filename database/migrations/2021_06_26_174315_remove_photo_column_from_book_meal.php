<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePhotoColumnFromBookMeal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( Schema ::hasColumn ( 'book_meals', 'photo' ) ) {
            Schema ::table ( 'book_meals', function ( Blueprint $table ) {
                $table -> dropColumn ( 'photo' );
            } );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        if ( Schema ::hasColumn ( 'book_meals', 'photo' ) ) {
            Schema ::table ( 'book_meals', function ( Blueprint $table ) {
                $table -> dropColumn ( 'photo' );
            } );
        }
    }
}
