<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdColumnInMealPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema ::table ( 'meal_posts', function ( Blueprint $table ) {
            $table -> unsignedInteger ( 'user_id' ) -> nullable ();
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema ::table ( 'meal_posts', function ( Blueprint $table ) {
            $table -> dropColumn ( 'user_id' );
        } );
    }
}
