( function () {
    $ ( "#profile_form" ).validate ( {
        rules : {
            first_name : {
                required : true
            },
            last_name : {
                required : true,
            }
        },
        submitHandler : function ( form ) {
            console.log ( form )
            form.submit ();
        }
    } );
    $ ( "#update-password" ).validate ( {
        rules : {
            currentPassword : {
                required : true
            },
            password : {
                required : true,
            },
            password_confirmation : {
                required : true,
                equalTo : "#password"
            },
        },
        submitHandler : function ( form ) {
            console.log ( form )
            let formData = prepareData ();
            api_call ( 'update_password', 'post', formData ).then ( ( response ) => {
                console.log ( response );

                if ( typeof ( response.status ) !== 'undefined' && response.status == 'success' ) {
                    successMessage ( response.message, window.location.origin + "/dashboard" )
                    form.reset ();
                    $ ( '#password-modal' ).modal ( "toggle" );
                } else {
                    swal ( {
                        title : "Error!",
                        text : response.messages,
                        type : "error",
                        confirmButtonColor : "#DD6B55",
                    } );
                }
            } ).catch ( err => {
                console.log ( err )
            } );
            return false; // extra insurance preventing the default form action

        }
    } );
    $ ( '#btn-change-password' ).on ( "click", function ( e ) {
        $ ( '#password-modal' ).modal ( "toggle" );
    } )
} ) ();

function prepareData () {
    const formData = new FormData ();
    formData.append ( "currentPassword", btoa ( $ ( '#currentPassword' ).val () ) );
    formData.append ( "password", btoa ( $ ( '#password' ).val () ) );
    formData.append ( "password_confirmation", btoa ( $ ( '#password_confirmation' ).val () ) );
    return formData;
}