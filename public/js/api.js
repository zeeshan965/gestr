/**
 * @param url
 * @param type
 * @param body
 * @returns {*}
 */
function api_call ( url, type, body ) {
    return fetch ( url, {
        method : type, // *GET, POST, PUT, DELETE, etc.
        headers : {
            // 'Content-Type' : 'application/json',
            // 'Content-Type' : 'application/x-www-form-urlencoded',
            'X-Requested-With' : 'XMLHttpRequest',
            'X-CSRF-TOKEN' : $ ( 'meta[name="csrf-token"]' ).attr ( 'content' )
        },
        body : body
    } ).then ( ( response ) => {
        return response.json ();
    } );
}

/**
 * @param response
 * @param message
 */
function showErrors ( response, message = '' ) {
    swal ( {
        title : "Error!",
        text : message == null || message == '' ? "Something went wrong, Please contact support!" : message,
        type : "error",
        confirmButtonColor : "#DD6B55",
    } );
}

/**
 * @param message
 * @param redirect_to
 */
function successMessage ( message, redirect_to ) {
    swal ( {
        title : "SUCCESS!",
        text : message,
        type : "success",
        confirmButtonColor : "#566b8a",
        closeOnConfirm : false
    }, function () {
        window.location.assign ( redirect_to );
    } );
}

/**
 *
 * @param errors
 */
function errorMessage ( errors ) {
    let html = '';
    if ( typeof ( errors.messages ) !== 'undefined' ) {
        for ( let item in errors.messages ) {
            html += '<p>' + errors.messages[ item ] + '</p>';
        }
    }
    if ( typeof ( errors.errors ) !== 'undefined' ) {
        for ( let item in errors.errors ) {
            html += '<p>' + errors.errors[ item ][ 0 ] + '</p>';
        }
    }

    swal ( {
        html : true,
        title : "Error!",
        text : html,
        type : "warning",
        confirmButtonColor : "#DD6B55",
    } );
}