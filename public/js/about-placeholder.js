var placeholder = 'Please share why this Gestr page is being organized.  You can share regular updates and photos on the Updates tab.\n\nInclude the recipient email address here to help with gift card giving.';
( function ( $, window, document ) {
    $ ( function () {
        if ( $ ( "textarea.about-placeholder" ).val () === '' )
            setAboutPlaceHolder ( $ ( "textarea.about-placeholder" ) );
    } );
} ( window.jQuery, window, document ) );
$ ( "body" ).on ( "focus", "textarea.about-placeholder", function ( e ) {
    e.preventDefault ();
    if ( $ ( this ).val () === placeholder ) {
        $ ( this ).val ( '' )
            .removeClass ( "placeholder" );
    }
} );
$ ( "body" ).on ( "blur", "textarea.about-placeholder", function ( e ) {
    setAboutPlaceHolder ( this );
} );

$ ( "body" ).on ( "click", ".btn-storyedit-save", function ( e ) {
    e.preventDefault ();
    if ( $ ( "#Story" ).val () == '' ) return;
    if ( $ ( "#train_id" ).val () == '' ) return;
    let l = $ ( this ).ladda ();
    l.ladda ( 'start' );
    let url = window.location.origin;
    url = url + '/trains/' + $ ( "#train_id" ).val () + '/edit/story';
    const formData = new FormData ();
    formData.append ( 'body', $ ( "#Story" ).val () );
    api_call ( url, 'post', formData ).then ( ( response ) => {
        console.log ( response );
        l.ladda ( 'stop' );
        $ ( "#pg-storyedit-modal" ).modal ( 'hide' );
        $ ( ".pg-train-story > p" ).text ( response.message );
        $ ( ".pg-train-story-add" ).hide ();
        $ ( ".pg-train-story" ).show ();
    } ).catch ( err => {
        l.ladda ( 'stop' );
        console.log ( err )
    } );
} );

function setAboutPlaceHolder ( objAbout ) {
    if ( $ ( objAbout ).val () === '' ) {
        $ ( objAbout ).val ( placeholder )
            .addClass ( "placeholder" );
    }
}