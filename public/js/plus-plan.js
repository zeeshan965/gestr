/**
 * @type {*[]}
 */
var dates = [],
    needTypes = [ "Dinner", "Gallon 2% milk, 3 bananas, and wheat bread", "Morning dog walk", "Afternoon Visit",
        "Ride to MD pick up at 3:00pm", "Childcare from 2-4 pm", "Phone call check in" ], events = [],
    settings = {}, element = document.getElementById ( 'caleandar' );

/**
 * @param id
 */
function setCategory ( id ) {
    let needText = needTypes[ id - 1 ];
    $ ( ".category-example" ).html ( needText == null ? "Example: Dinner" : "Example: " + needTypes[ id - 1 ] );
}

/**
 * @param selectedDate
 * @private
 */
function _modal ( selectedDate ) {
    $ ( "#needed" ).val ( "" );
    $ ( "#category" ).val ( "" );
    setCategory ( 1 );
    dates = [];

    if ( selectedDate != null ) setTimeout ( x => $ ( '#mini-' + selectedDate ).click (), 500 );
    if ( selectedDate == null ) selectedDate = new Date ();
    let selectedMonth = moment ( new Date ( selectedDate ) );
    $ ( "#calendarCurrent" ).mtcalendar ( "refresh", selectedMonth );
    $ ( "#calendarNext" ).mtcalendar ( "refresh", selectedMonth.add ( { months : 1 } ) );
    $ ( "#pg-create-modal" ).modal ( { show : true } );
}

/**
 * @param date
 * @returns {number}
 */
function compareDates ( date ) {
    date += " 23:59:59";
    let g1 = new Date (), g2 = new Date ( date ), result;
    if ( g1.getTime () < g2.getTime () ) result = 1;
    else if ( g1.getTime () > g2.getTime () ) result = 0;
    return result;
}

/**
 * @param id
 * @private
 */
function _action ( id ) {
    let date_elem = $ ( "#" + id ), date = date_elem.attr ( "id" );
    if ( compareDates ( date ) === 0 ) return swal ( {
        title : "Error!",
        text : "Sorry! You can not select previous date.",
        type : "error",
        confirmButtonColor : "#DD6B55",
    } );
    let matches = $.grep ( dates, function ( element, index ) {
        return element.date == date;
    } );

    if ( matches.length <= 0 ) {
        dates.push ( { date : date, booked : false } );
        date_elem.addClass ( "selected" );
    } else {
        dates = $.grep ( dates, function ( element, index ) {
            return element.date == date;
        }, true );
        date_elem.removeClass ( "selected" );
    }
}

/**
 * Onload
 */
( function () {
    const nextMonth = moment ().add ( { months : 1 } );
    caleandar ( element, events, settings );
    $ ( "#calendarCurrent" ).mtcalendar ( {
        selections : function ( date, callback ) {
            callback ( dates );
        }, action : function () {
            _action ( this.id );
        }
    } );
    $ ( "#calendarNext" ).mtcalendar ( {
        date : nextMonth, selections : function ( date, callback ) {
            callback ( dates );
        }, action : function () {
            _action ( this.id );
        }
    } );
    $ ( "#btnCalPrev" ).click ( function ( e ) {
        e.preventDefault ();
        $ ( ".mt-calendar" ).mtcalendar ( "prev" );
    } );
    $ ( "#btnCalNext" ).click ( function ( e ) {
        e.preventDefault ();
        $ ( ".mt-calendar" ).mtcalendar ( "next" );
    } );
} () );

/**
 * Jquery on click
 */
$ ( "#btnAddEvent" ).click ( function ( e ) {
    e.preventDefault ();
    _modal ();
} );

/**
 * Jquery on change
 */
$ ( document ).on ( "change", '.category-dropdown', function ( e ) {
    e.preventDefault ();
    setCategory ( $ ( this ).val () );
} );

/**
 * Jquery on click
 */
$ ( document ).on ( "click", "#btnDeleteEvent", function ( e ) {
    e.preventDefault ();
    let id = $ ( '#editId' ).val ();
    let index = $ ( '#editIndex' ).val ();
    delete events[ id ].event_data[ index ];
    caleandar ( element, events, settings );
    $ ( "#pg-update-modal" ).modal ( "hide" );
} );

/**
 * Jquery on submit
 */
$ ( "#updateForm" ).on ( "submit", function ( e ) {
    e.preventDefault ();
    if ( $ ( "#updateForm" ).valid () ) {
        let formData = $ ( "#updateForm" ).serializeObject ();
        formData.Category = $ ( "#editCategory option:selected" ).text ();
        let id = $ ( '#editId' ).val ();
        let index = $ ( '#editIndex' ).val ();
        events[ id ].event_data[ index ].needed = formData.editNeed;
        events[ id ].event_data[ index ].title = formData.Category + ': ' + formData.editNeed;
        events[ id ].event_data[ index ].category = formData.editCategory;
        caleandar ( element, events, settings );
        $ ( "#pg-update-modal" ).modal ( "hide" );
    }
} );

/**
 * Jquery on click
 */
$ ( document ).on ( "click", "#btnCreateEvent", function ( e ) {
    e.preventDefault ();
    if ( $ ( "#createForm" ).valid () ) {
        if ( dates.length === 0 ) return swal ( {
            title : "Error!",
            text : "Please choose Dates First.",
            type : "error",
            confirmButtonColor : "#DD6B55",
        } );
        let formData = $ ( "#createForm" ).serializeObject ();
        formData.Category = $ ( "#category option:selected" ).text ();
        $.each ( dates, function ( i, val ) {
            let dd = val.date.split ( "mini-" )[ 1 ];
            if ( events.hasOwnProperty ( dd ) ) {
                events[ dd ].event_data.push ( {
                    'title' : formData.Category + ": " + formData.needed,
                    'link' : 'javascript:;', 'category' : formData.category, 'needed' : formData.needed
                } );
            } else {
                events[ dd ] = {
                    'Date' : new Date ( val.date ), 'event_data' : [ {
                        'title' : formData.Category + ": " + formData.needed,
                        'link' : 'javascript:;', 'category' : formData.category, 'needed' : formData.needed
                    } ]
                };
            }
        } );
        caleandar ( element, events, settings );
        $ ( "#pg-create-modal" ).modal ( "hide" );
    }
} );

/**
 * Jquery on click
 */
$ ( document ).on ( "click", ".edit-need", function ( e ) {
    if ( e.target !== e.currentTarget ) return;
    let id = $ ( this ).attr ( 'data-date' );
    let index = $ ( this ).attr ( 'data-index' );
    let data = events[ id ].event_data[ index ];
    let ss = new Date ( id );
    const month = ss.toLocaleString ( 'default', { month : 'long' } );
    $ ( '#editDate' ).text ( month + ' ' + ss.getDate () + ', ' + ss.getFullYear () );
    $ ( '#editCategory' ).val ( data.category );
    $ ( '#editNeed' ).val ( data.needed );
    $ ( '#editId' ).val ( id );
    $ ( '#editIndex' ).val ( index );
    $ ( "#pg-update-modal" ).modal ( { show : true } );
} );