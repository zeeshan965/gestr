( function () {
    let tempFormData = localStorage.FullFormData;
    let l = $ ( '.ladda-button-demo' ).ladda ();
    $ ( "#signup_form" ).validate ( {
        rules : {
            first_name : {
                required : true
            },
            last_name : {
                required : true,
            },
            email : {
                required : true
            },
            password : {
                required : true,
            },
            password_confirmation : {
                required : true,
            },
        },
        submitHandler : function ( form ) {
            l.ladda ( 'start' );
            let formData = prepareData ();
            api_call ( 'register', 'post', formData ).then ( ( response ) => {
                l.ladda ( 'stop' );
                if ( typeof ( response.status ) !== 'undefined' && response.status == 'success' ) {
                    let userID = response.data.id;
                    console.log ( response.csrf )
                    $ ( 'meta[name="csrf-token"]' ).attr ( 'content', response.csrf );
                    localStorage.UserSession = "Session";
                    localStorage.organizer_id = response.data.id;
                    localStorage.organizer_name = response.data.first_name + " " + response.data.last_name;
                    let tempFormData = localStorage.FullFormData, redirect_to = response.intended;
                    if ( tempFormData ) {
                        const fullFormData = JSON.parse ( localStorage.FullFormData );
                        const formData2 = new FormData ();
                        for ( let i = 0 ; i < fullFormData.length ; ++ i ) {
                            const item = fullFormData[ i ];
                            if ( item[ 0 ] == "organizer" ) {
                                formData2.append ( item[ 0 ], userID );
                            } else {
                                formData2.append ( item[ 0 ], item[ 1 ] );
                            }
                        }
                        formData2.append ( '_token', response.csrf );
                        api_call ( 'checkout', 'post', formData2 ).then ( ( response ) => {
                            l.ladda ( 'stop' );
                            if ( response.status == "success" ) {
                                localStorage.FullFormData = "";
                                localStorage.removeItem ( "FullFormData" );
                                l.ladda ( 'stop' );
                                successMessage ( "Logged in successfully.", redirect_to )
                            } else if ( response.status == "failed" ) {
                                l.ladda ( 'stop' );
                                showErrors ( response, response.error );
                                setTimeout ( x => location.reload (), 1500 );
                            }
                        } ).catch ( err => {
                            console.log ( err )
                            l.ladda ( 'stop' );
                        } );
                    } else {
                        localStorage.FullFormData = "";
                        localStorage.removeItem ( "FullFormData" );
                        l.ladda ( 'stop' );
                        successMessage ( "Logged in successfully.", response.intended )
                    }
                } else {
                    //validation errors
                    if ( response.status == 'error' ) {
                        errorMessage ( response );
                    } else {
                        showErrors ( response );
                    }
                }
            } ).catch ( err => {
                console.log ( err )
                l.ladda ( 'stop' );
            } );
            return false; // extra insurance preventing the default form action
        }
    } );
    $ ( "#signin_form" ).validate ( {
        rules : {
            email : {
                required : true
            },
            password : {
                required : true,
            },
        },
        submitHandler : function ( form ) {
            l.ladda ( 'start' );
            const formData = new FormData ();
            formData.append ( "email", $ ( '#email' ).val () );
            formData.append ( "password", $ ( '#password' ).val () );
            api_call ( 'login', 'post', formData ).then ( ( response ) => {
                l.ladda ( 'stop' );
                if ( typeof ( response.status ) !== 'undefined' && response.status == 'success' ) {
                    let userID = response.data.id;
                    console.log ( response )
                    console.log ( $ ( 'meta[name="csrf-token"]' ).attr ( 'content' ) );
                    $ ( 'meta[name="csrf-token"]' ).attr ( 'content', response.csrf );
                    localStorage.UserSession = "Session";
                    localStorage.organizer_id = userID;
                    localStorage.organizer_name = response.data.first_name + " " + response.data.last_name;
                    let tempFormData = localStorage.FullFormData, redirect_to = response.intended;
                    if ( tempFormData ) {
                        const fullFormData = JSON.parse ( localStorage.FullFormData );
                        const formData2 = new FormData ();
                        for ( let i = 0 ; i < fullFormData.length ; ++ i ) {
                            const item = fullFormData[ i ];
                            if ( item[ 0 ] == "organizer" ) {
                                formData2.append ( item[ 0 ], userID );
                            } else {
                                formData2.append ( item[ 0 ], item[ 1 ] );
                            }
                        }
                        formData2.append ( '_token', response.csrf );
                        api_call ( 'checkout', 'post', formData2 ).then ( ( response ) => {
                            l.ladda ( 'stop' );
                            if ( response.status == "success" ) {
                                localStorage.FullFormData = "";
                                localStorage.removeItem ( "FullFormData" );
                                l.ladda ( 'stop' );
                                successMessage ( "Logged in successfully.", redirect_to )
                            } else if ( response.status == "failed" ) {
                                l.ladda ( 'stop' );
                                showErrors ( response, response.error );
                                setTimeout ( x => location.reload (), 1500 );
                            }
                            // if ( typeof ( response.status ) !== 'undefined' && response.status == 'success' ) {
                            //     successMessage ( "Account created successfully.", response.intended )
                            //     localStorage.UserSession = "Session";
                            //     localStorage.organizer_id = response.data.id;
                            //     localStorage.organizer_name = response.data.first_name + " " + response.data.last_name;
                            // } else {
                            //     //validation errors
                            //     if ( response.status == 'error' ) {
                            //         errorMessage ( response );
                            //     } else {
                            //         showErrors ( response );
                            //     }
                            // }
                        } ).catch ( err => {
                            console.log ( err )
                            l.ladda ( 'stop' );
                        } );
                    } else {
                        localStorage.FullFormData = "";
                        localStorage.removeItem ( "FullFormData" );
                        l.ladda ( 'stop' );
                        successMessage ( "Logged in successfully.", response.intended )
                    }
                } else {
                    console.log ( response )
                    //validation errors
                    if ( response.status == 'error' ) {
                        errorMessage ( response );
                    } else if ( response.message == "The given data was invalid." ) {
                        errorMessage ( response );
                    } else {
                        showErrors ( response );
                    }
                }
            } ).catch ( err => {
                console.log ( err )
                l.ladda ( 'stop' );
            } );
            return false; // extra insurance preventing the default form action
        }
    } );
} ) ();

function prepareData () {
    const formData = new FormData ();
    formData.append ( "first_name", $ ( '#first_name' ).val () );
    formData.append ( "last_name", $ ( '#last_name' ).val () );
    formData.append ( "email", $ ( '#email' ).val () );
    formData.append ( "password", btoa ( $ ( '#password' ).val () ) );
    formData.append ( "password_confirmation", btoa ( $ ( '#password_confirmation' ).val () ) );
    return formData;
}