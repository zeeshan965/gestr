$.fn.mtcalendar = function (options) {
    var args = Array.prototype.slice.call(arguments, 1); // for a possible method call
    var res = this;
    this.each(function (i, _element) {
        var element = $(_element);
        var calendar = element.data('mtCalendar');

        if (typeof options === 'string') {
            if (calendar && $.isFunction(calendar[options])) {
                calendar[options].apply(calendar, args);
            }
        }
        else if (!calendar) {
            calendar = new mtCalendar(element, options);
            element.data('mtCalendar', calendar);
            calendar.render();
        }
    });

    return res;
};

function mtCalendar(element, instanceOptions) {
    var t = this;

    var defaults = {
        date: moment(),
        hidePastDates: false
    };

    var options = $.extend({}, defaults, instanceOptions);
    var date = options.date || moment();
    var hidePastDates = options.hidePastDates || false;

    t.refresh = refresh;
    t.render = render;
    t.next = next;
    t.prev = prev;
    t.date = date;
    t.loadData = loadData;

    function date() {
        return date.clone();
    }

    function next() {
        date.add({ months: 1 });
        render();
    }

    function prev() {
        date.add({ months: -1 });
        render();
    }

    function refresh(startDate) {
        if (startDate != null)
            date = startDate.clone();
        render();
    }

    function render() {
        var $tableObj = $('<table class="table table-bordered"></table>');
        $tableObj = drawTable($tableObj);

        element.empty();
        element.append($tableObj);

        loadEvents();
    }

    function loadData(events) {
        renderEvents(events);
    }

    function drawTable($tableObj) {
        $tableObj.empty();
        $tableObj = appendMonthHeader($tableObj);
        $tableObj = appendDayOfWeekHeader($tableObj);
        $tableObj = appendDaysOfMonth($tableObj);
        return $tableObj;
    }

    function appendMonthHeader($tableObj) {
        var monthLabels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

        var $currMonthLabel = $('<span>' + monthLabels[date.month()] + ' ' + date.year() + '</span>');
        var $currMonthCell = $('<th colspan="7"></th>');
        var $monthHeaderRow = $('<tr class="calendar-month-header"></tr>');

        $currMonthCell.append($currMonthLabel);
        $monthHeaderRow.append($currMonthCell);

        $tableObj.append($monthHeaderRow);
        return $tableObj;
    }

    function appendDayOfWeekHeader($tableObj) {
        var dowLabels = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

        var $dowHeaderRow = $('<tr class="calendar-dow-header"></tr>');
        $(dowLabels).each(function (index, value) {
            $dowHeaderRow.append('<th>' + value + '</th>');
        });
        $tableObj.append($dowHeaderRow);

        return $tableObj;
    }

    function appendDaysOfMonth($tableObj) {
        var weeksInMonth = calcWeeksInMonth(date.year(), date.month());
        var lastDayinMonth = calcLastDayInMonth(date.year(), date.month());
        var firstDow = calcDayOfWeek(date.year(), date.month(), 1);
        var lastDow = calcDayOfWeek(date.year(), date.month(), lastDayinMonth);
        var currDayOfMonth = 1;

        if (lastDow == 6) {
            weeksInMonth++;
        }
        if (firstDow == 6 && (lastDow == 0 || lastDow == 1 || lastDow == 5)) {
            weeksInMonth--;
        }
        firstDow++;
        if (firstDow == 7) {
            firstDow = 0;
        }

        for (var wk = 0; wk < weeksInMonth; wk++) {
            var $dowRow = $('<tr class="calendar-dow"></tr>');
            for (var dow = 0; dow < 7; dow++) {
                if (dow < firstDow || currDayOfMonth > lastDayinMonth) {
                    $dowRow.append('<td></td>');
                } else {
                    var isPast = moment({ y: date.year(), M: date.month(), d: currDayOfMonth }).isBefore(moment(), "day");

                    if (hidePastDates == true && isPast == true) {
                        $dowRow.append('<td><div class="day">&nbsp;</div></td>');
                        currDayOfMonth++;
                    }
                    else {
                        var dateId = dateAsString(date.year(), date.month(), currDayOfMonth);
                        var dayId = dateId + '_day';

                        var $dayElement = $('<div id="' + dayId + '" class="day" >' + currDayOfMonth + '</div>');
                        var $dowElement = $('<td id="mini-' + dateId + '"></td>');

                        $dowElement.append($dayElement);

                        $dowElement.data('date', dateAsString(date.year(), date.month(), currDayOfMonth));

                        if (typeof (options.action) === 'function') {
                            $dowElement.addClass('dow-clickable');
                            $dowElement.click(options.action);
                        }

                        $dowRow.append($dowElement);

                        currDayOfMonth++;
                    }
                }
                if (dow == 6) {
                    firstDow = 0;
                }
            }

            $tableObj.append($dowRow);
        }
        return $tableObj;
    }

    function loadEvents() {
        if (options.selections && typeof (options.selections) === 'function') {
            options.selections.call(
                t,
                t.date,
                function (events) {
                    renderEvents(events);
                }
            );
        }
    }

    function renderEvents(events) {
        $(events).each(function (index, value) {
            var $dowElement = $('#' + value.date);

            if(value.booked)
                $dowElement.addClass('booked');
            else
                $dowElement.addClass('selected');
        });
    }

    function dateAsString(year, month, day) {
        d = (day < 10) ? '0' + day : day;
        m = month + 1;
        m = (m < 10) ? '0' + m : m;
        return year + '-' + m + '-' + d;
    }

    function calcDayOfWeek(year, month, day) {
        var dateObj = new Date(year, month, day, 0, 0, 0, 0);
        var dow = dateObj.getDay();
        if (dow == 0) {
            dow = 6;
        } else {
            dow--;
        }
        return dow;
    }

    function calcLastDayInMonth(year, month) {
        var day = 28;
        while (checkValidDate(year, month + 1, day + 1)) {
            day++;
        }
        return day;
    }

    function calcWeeksInMonth(year, month) {
        var daysInMonth = calcLastDayInMonth(year, month);
        var firstDow = calcDayOfWeek(year, month, 1);
        var lastDow = calcDayOfWeek(year, month, daysInMonth);
        var days = daysInMonth;
        var correct = (firstDow - lastDow);
        if (correct > 0) {
            days += correct;
        }
        return Math.ceil(days / 7);
    }

    function checkValidDate(y, m, d) {
        return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
    }
}