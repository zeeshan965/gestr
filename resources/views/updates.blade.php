@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/details.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.fileupload.css' ) }}"/>

    <link rel="stylesheet" href="{{ asset ( 'css/demo.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/theme2.css' ) }}"/>

    <style>
        .mt {
            margin-top: 20px;
        }

        .pg-photo-box {
            background-color: #C9C9C9;
            border: 0;
            border-radius: 5px;
            color: #fff;
            font-size: 20px;
            margin: 0 auto;
            overflow: hidden;
            padding: 20% 0;
            text-align: center;
            width: 80%;
        }

        .f-14 {
            font-size: 14px;
        }

        .tab-content > .active {
            display: block;
            visibility: visible;
        }

        .height-center {
            position: relative;
            top: 50%;
            transform: translateY(25%);
        }

    </style>
@endsection
@section( 'extra_js' )
    <script type="text/javascript" src="{{ asset ( 'js/api.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.ui.widget.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.fileupload.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/about-placeholder.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/caleandar.js' ) }}"></script>

    <script type="text/javascript">
        let url = '{{ url ( "trains/{$meal -> unique_id}" ) }}';
        $ ( "#fileupload" ).fileupload ( {
            url : url + '/upload',
            dataType : "json",
            autoUpload : false,
            maxNumberOfFiles : 1,
            formData : { _token : $ ( 'meta[name="csrf-token"]' ).attr ( 'content' ) },
            done : function ( e, data ) {
                $ ( ".progress.fileupload-progress, .pg-photo-box" ).hide ();
                $ ( "#file-upload-img" ).attr ( "src", data.result.data ).show ();
                $ ( ".btn-remove" ).show ();
                fileUploadDone ( data.result.data );
            },
            add : function ( e, data ) {
                let elem = $ ( "#file-upload-status" );
                elem.hide ();
                if ( data.files && data.files[ 0 ] && data.files[ 0 ].size && data.files[ 0 ].size > 8388608 ) {
                    let message = "File size limit is 8 MB.Please reduce the size of the image and try again.";
                    elem.removeClass ( "hidden alert-success alert-danger" )
                        .html ( message || "The action did not complete. Please try again." ).addClass ( "alert-danger" ).fadeIn ( "fast" );
                } else {
                    console.log ( data );
                    data.submit ();
                }
            },
            fail : function ( e, data ) {
                console.log ( data )
                $ ( ".progress.fileupload-progress" ).hide ();
                let message = data.jqXHR.responseJSON.messages;
                $ ( "#file-upload-status" ).removeClass ( "hidden alert-success alert-danger" )
                    .html ( message || "The action did not complete. Please try again." ).addClass ( "alert-danger" ).fadeIn ( "fast" );
            },
            progressall : function ( e, data ) {
                var progress = parseInt ( data.loaded / data.total * 100, 10 );
                $ ( ".progress.fileupload-progress .progress-bar" ).css ( "width", progress + "%" );
            },
            start : function ( e, data ) {
                $ ( ".progress.fileupload-progress .progress-bar" ).css ( "width", "0%" );
                $ ( ".progress.fileupload-progress" ).show ();
            }
        } ).prop ( 'disabled', ! $.support.fileInput ).parent ().addClass ( $.support.fileInput ? undefined : 'disabled' );

        $ ( ".btn-remove" ).click ( function ( e ) {
            e.preventDefault ();
            api_call ( url + '/remove_photo', 'post', new FormData () ).then ( ( response ) => {
                $ ( "#file-upload-img" ).attr ( "src", "" ).hide ();
                $ ( ".pg-photo-box" ).show ();
                fileUploadDone ( '' );
            } ).catch ( err => {
                console.log ( err )
            } );
        } );

        $ ( ".btn-story-add" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( "#pg-storyedit-modal" ).modal ( { show : true } );
        } );

        $ ( ".btn-volunteer-info, .btn-volunteer-info" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( "#pg-info-modal" ).modal ( { show : true } );
        } );

        $ ( ".btn-viewmode" ).on ( "click", function ( e ) {
            e.preventDefault ();
            $ ( this ).tab ( "show" );
            $ ( ".btn-viewmode" ).toggleClass ( "active" );
        } );

        $ ( '.btn-delete-entry' ).on ( "click", function () {
            $ ( "#pg-journaldelete-modal" ).modal ( { show : true } );
            $ ( '.post_title' ).text ( $ ( this ).attr ( 'data-title' ) )
            ///m71813/delete/
            let form = $ ( '#pg-journaldelete-modal' ).find ( '.modal-dialog .modal-content form' );
            let url = form.attr ( 'action' );
            form.attr ( 'action', url + $ ( this ).attr ( 'data-id' ) + "/delete" )
        } );

        @if(request () ->get ('intro'))
        $ ( "#pg-trainintro-modal" ).modal ( { show : true } );
        @endif
        function fileUploadDone ( path ) {
            $ ( "#pg-train-photo > img" ).attr ( "src", path );
            if ( path == null || path == "" ) {
                $ ( "#pg-train-photo" ).addClass ( 'hide' );
                $ ( ".btn-photo-box" ).removeClass ( 'hide' );
            } else {
                $ ( ".btn-photo-box" ).addClass ( 'hide' );
                $ ( "#pg-train-photo" ).removeClass ( 'hide' );
            }
        }

    </script>
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __("Gestr Updates for {$meal -> recipient_name}") }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                    @if( Auth::check () && Auth::id () == $user -> id )
                        <div class="col-md-12">
                            <a class="btn btn-info btn-lg" title="Post an update" href="{{ $url.'/updates/new' }}">
                                <i class="fa fa-comments-o"></i>&nbsp;Post an update
                            </a>
                            <a class="btn btn-info btn-lg" title="Make changes" href="{{ $url.'/edit' }}">
                                <i class="fa fa-pencil"></i>&nbsp;Make changes
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </x-slot>
    <input type="hidden" id="train_id" value="{{ $meal -> unique_id ?? '' }}"/>
    <div class="container-fluid">
        <div class="row mt">
            <div class="col-sm-3 col-md-4">
                @if( Auth::check () && Auth::id () == $user -> id )
                    <div class="btn-photo-container">
                        <div class="btn-admin-box btn-photo-box btn-block @if( $meal -> attachment != null ) hide @endif">
                            <button type="button" id="btn-photo-new" data-toggle="modal" data-target="#pg-modal"
                                    class="btn btn-primary">
                                <i class="fa fa-plus"></i>&nbsp;Add a Photo
                            </button>
                            <br>
                            <p>Adding a photo can lead<br>to greater participation</p>
                        </div>
                        <div id="pg-train-photo"
                             class="text-center round5 @if( $meal -> attachment == null ) hide @endif">
                            <img @if( $meal -> attachment != null ) src="{{ url ( "uploads/{$meal -> attachment -> url}" ) }}" @endif>
                        </div>
                    </div>
                @elseif( $meal -> attachment != null )
                    <div class="btn-photo-container">
                        <div id="pg-train-photo"
                             class="text-center round5 @if( $meal -> attachment == null ) hide @endif">
                            <img @if( $meal -> attachment != null ) src="{{ url ( "uploads/{$meal -> attachment -> url}" ) }}" @endif>
                        </div>
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-body">
                        <strong class="f-14">Organizer</strong>
                        <p style="margin: 0;line-height: 0">
                            <a class="f-14" href="{{ $url.'/message' }}">
                                <i class="fa fa-envelope"></i>&nbsp;{{ $user -> first_name }}
                            </a>
                        </p>

                        <strong class="f-14">Recipient</strong>
                        <p style="margin: 0 0 10px 0;line-height: 0">
                            <a class="f-14" href="{{ url ( "trains/{$meal -> unique_id}/message" ) }}">
                                <i class="fa fa-envelope"></i>&nbsp;{{ $meal -> recipient_name }}
                            </a>
                        </p>
                        <p style="margin: 0;line-height: 0">
                            <a href="javascript:;" class="btn-participants">View/Email all participants&nbsp;
                                <span class="badge f-14">{{ count($meal -> recipients) }}</span>
                            </a>
                        </p>

                        <div class="hidden-xs">
                            <br>
                            <p>
                                <input id="url" name="url" type="text" class="form-control"
                                       value="{{ $url }}" onfocus="this.select();">
                            </p>
                            <a class="btn btn-mt btn-block" href="{{ $url.'/invite' }}">
                                <i class="fa fa-envelope"></i>&nbsp;Invite</a>
                            <a class="btn btn-fb btn-block"><i class="fa fa-facebook"></i>&nbsp;Share</a>
                            <a class="btn btn-tw btn-block"><i class="fa fa-twitter"></i>&nbsp;Tweet</a>
                            <div><small><a data-toggle="modal" href="#pg-invitehelp-modal"><i
                                                class="fa fa-question-circle"></i>&nbsp;Invitation help?</a></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 col-md-8">
                @if( Auth::check () && Auth::id () == $user -> id )
                    <div class="hidden-xs">
                        <div class="pg-train-story halfpad-bottom"
                             @if( $meal -> body == '' || $meal -> body == null ) style="display:none;" @endif>
                            <h4><strong>About this Gestr page</strong></h4>
                            <p style="white-space: pre-wrap;">{{ $meal -> body }}</p>
                        </div>
                        <div class="pg-train-story-add halfpad-bottom"
                             @if( $meal -> body != '' && $meal -> body != null ) style="display:none;" @endif>
                            <div class="btn-admin-box pg-story-edit-box">
                                <p>Let everyone know why you are organizing this Gestr page.</p>
                                <button class="btn btn-primary btn-story-add">
                                    <i class="fa fa-pencil"></i>&nbsp;Tell the story
                                </button>
                            </div>
                        </div>
                    </div>
                @elseif( $meal -> body != null )
                    <div class="hidden-xs">
                        <div class="pg-train-story halfpad-bottom">
                            <h4><strong>About this Gestr page</strong></h4>
                            <p style="white-space: pre-wrap;">{{ $meal -> body }}</p>
                        </div>
                    </div>
                @endif

                <div class="hidden-xs">
                    <div class="nav-header-container" style="height: 46px;">
                        <ul class="nav nav-justified-sm nav-header affix" data-toggle="affix">
                            <li>
                                <a href="{{ url ( "trains/{$meal -> unique_id}" ) }}">
                                    <i class="fa fa-calendar"></i>
                                    <span class="hidden-xs">&nbsp;</span><span class="visible-xs-inline"><br></span>Calendar
                                    <span class="visible-xs-inline"><br>&nbsp;</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="{{ $url.'/updates' }}">
                                    <i class="fa fa-comment-o"></i>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <span class="visible-xs-inline"><br></span>Updates
                                    <span class="visible-xs-inline"><br></span>
                                    @if( $meal -> posts -> count() > 0 )
                                        <span class="badge">{{ $meal -> posts -> count() }}</span>
                                    @else
                                        <span class="visible-xs-inline"><br></span><span class="text-mt">New!</span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <h4><strong>Updates</strong></h4>
                @if( Auth::check () && Auth::id () == $user -> id )
                    @if( $meal -> posts -> count() == 0 )
                        <div class="jumbotron">
                            <strong>Your friends want to hear from you!</strong><br><br>
                            Use the Updates tab to share updates and photos as they occur.<br><br>
                            Gestr pages with frequent updates have higher participation rates.<br><br>
                            <div class="text-center">
                                <a class="btn btn-success btn-responsive-block btn-new-entry"
                                   title="Post an update/photo"
                                   href="{{ url ( "/trains/{$meal -> unique_id}/updates/new" ) }}">
                                    <i class="fa fa-pencil"></i>&nbsp;Post an update/photo
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="halfpad-bottom row hidden-xs">
                            <div class="col-sm-5 text-center">
                                <a class="btn btn-success btn-block btn-sm btn-new-entry" title="Post an update/photo"
                                   href="{{ $url."/updates/new" }}">
                                    <i class="fa fa-pencil"></i>&nbsp;Post an update/photo</a>
                                <small>Only the organizers and the recipient may post.</small>
                            </div>
                        </div>
                        @foreach( $meal -> posts as $k => $post )
                            <div class="halfpad-bottom">
                                <h2>{{ $post -> title }}</h2>
                                <p><i>Posted {{ $post -> created_at -> format ( 'M d, Y' ) }}
                                      by {{ $post -> user -> full_name }}</i></p>
                                <p><span style="white-space: pre-wrap;">{{ $post -> body }}</span></p>
                                @if( $post -> attachment != null )
                                    <div class="halfpad-bottom">
                                        <img src="{{ url ( "uploads/{$post -> attachment -> url}" ) }}"
                                             class="round5" alt="{{ $meal -> recipient_name }}"
                                             title="{{ $meal -> recipient_name }}">
                                    </div>
                                @endif
                                <a class="btn btn-sm btn-fb btn-fb-share-post"
                                   data-url="{{ $url."/updates/{$post -> unique_id}" }}">
                                    <i class="fa fa-facebook"></i>&nbsp;Share</a>
                                <a title="Make changes" class="btn btn-primary btn-sm btn-edit-entry"
                                   data-id="{{ $post -> unique_id }}"
                                   href="{{ $url."/updates/{$post -> unique_id}/edit" }}">
                                    <i class="fa fa-pencil"></i>&nbsp;Make changes</a>
                                <a title="Delete" class="btn btn-default btn-sm btn-delete-entry"
                                   data-title="{{ $post -> title }}"
                                   data-id="{{ $post -> unique_id }}">
                                    <i class="fa fa-trash"></i>&nbsp;Delete</a>
                            </div>
                            @if($k +1  !== $meal -> posts -> count() )
                                <hr>
                            @endif
                        @endforeach
                    @endif
                @elseif( $meal -> posts -> count() > 0 )
                    @foreach( $meal -> posts as $k => $post )
                        <div class="halfpad-bottom">
                            <h2>{{ $post -> title }}</h2>
                            <p><i>Posted {{ $post -> created_at -> format ( 'M d, Y' ) }}
                                  by {{ $post -> user -> full_name }}</i></p>
                            <p><span style="white-space: pre-wrap;">{{ $post -> body }}</span></p>
                            @if( $post -> attachment != null )
                                <div class="halfpad-bottom">
                                    <img src="{{ url ( "uploads/{$post -> attachment -> url}" ) }}"
                                         class="round5" alt="{{ $meal -> recipient_name }}"
                                         title="{{ $meal -> recipient_name }}">
                                </div>
                            @endif
                            <a class="btn btn-sm btn-fb btn-fb-share-post"
                               data-url="{{ $url."/updates/{$post -> unique_id}" }}">
                                <i class="fa fa-facebook"></i>&nbsp;Share</a>
                        </div>
                        @if($k +1  !== $meal -> posts -> count() )
                            <hr>
                        @endif
                    @endforeach
                @else
                    <div class="jumbotron">There are no updates yet for this Gestr page</div>
                @endif
            </div>

            <div class="modal fade" id="pg-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                            <h4 class="modal-title">Add a Photo</h4>
                        </div>
                        <div class="modal-body">
                            <form id="mt-form-photo" action="" method="post">
                                <div class="text-center">
                                    <style>

                                    </style>
                                    <div class="pg-photo-box"><i class="fa fa-photo fa-2x"></i></div>
                                    <img id="file-upload-img" style="display:none;"/>
                                    <p>
                                    <div class="btn btn-success btn-responsive-block fileinput-button">
                                        <i class="fa fa-plus"></i>&nbsp;Select new photo...
                                        <input id="fileupload" type="file" name="files" accept="image/*"/>
                                    </div>
                                    <a class="btn btn-default btn-responsive-block btn-remove" style="display:none;"
                                       href="javascript:;" data-loading-text="Removing...">
                                        <i class="fa fa-trash"></i>&nbsp;Remove photo</a>
                                    </p>
                                    <div class="progress fileupload-progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                             aria-valuemax="100" style="width:0%;"></div>
                                    </div>
                                    <div id="file-upload-status" class="alert hidden"></div>
                                </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-invitehelp-modal" tabindex="-1" role="dialog"
                 aria-labelledby="pg-ok-modal-label" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Invitation Options</h4>
                        </div>
                        <div class="modal-body">
                            <h4>Here is the unique link for this Gestr Page</h4>
                            <p><input id="InviteHelpUrl" name="InviteHelpUrl" type="text" class="form-control"
                                      value="{{ url ( "trains/{$meal -> unique_id}" ) }}" onfocus="this.select();"></p>
                            <br>
                            <p><strong>Personal Email</strong><br>Copy and paste the link above into your favorite email
                                                                  program (gmail, yahoo mail, outlook, etc).</p>
                            <br>
                            <p><strong>Via Gestr.com</strong>&nbsp;(PC or Tablet)<br>Click the orange Invite button
                                                             on the Calendar tab.</p>
                            <br>
                            <p><strong>Mobile Device</strong><br>Option 1: Click the orange Invite button to access your
                                                                 phone's address book.<br>Option 2: Click "View all
                                                                 participants" then the "Invite Others" button to invite
                                                                 via Gestr.com.</p>
                            <br>
                            <p><strong>Text Message</strong><br>While viewing the Gestr, click your phone's Share
                                                                button (usually
                                <i class="fa fa-share-alt"></i> or
                                <i class="fa fa-share-square-o"></i>).</p>
                            <br>
                            <p><strong>Facebook</strong><br>Click the blue "f Share" button.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-participants-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        {{--                        <script type="text/javascript" language="javascript"--}}
                        {{--                                src="/content/js/app/trains/participant-remove.js?v=071817"></script>--}}
                        <form id="mt-form-participants" class="form-horizontal" autocomplete="off">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Participants</h4>
                            </div>
                            <div class="modal-body" style="max-height:400px; overflow-y:scroll;">
                                <div class="alert alert-info">
                                    The list contains those who were invited from within MealTrain.com, signed up to
                                    help, or made a cash donation (if active). It does not include those who sent a gift
                                    card.
                                    <br><br><strong>Privacy Note:</strong> As an Organizer/Recipient, you are able to
                                    see the name, email and message/delete buttons. General participants will only see
                                    the name and message button.
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">chris narducci</div>
                                    <div class="col-sm-4">hello@gmail.com</div>
                                    <div class="col-sm-2">
                                        <a href="/trains/1oqdzq/message/kv5r60/" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-envelope" alt="Send email" title="Send email"></i></a>
                                        <a href="#" data-id="58vovv7"
                                           class="btn btn-participants-remove btn-danger btn-sm">
                                            <i class="fa fa-trash"
                                               alt="Remove" title="Remove"></i>
                                        </a>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">{{ $meal -> user -> full_name }}</div>
                                    <div class="col-sm-4">{{ $meal -> user -> email }}</div>
                                    <div class="col-sm-2">
                                        <a href="/trains/1oqdzq/message/e7n319o/" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-envelope" alt="Send email" title="Send email"></i></a>
                                        <a href="#" data-id="2qnr035"
                                           class="btn btn-participants-remove btn-danger btn-sm">
                                            <i class="fa fa-trash" alt="Remove" title="Remove"></i>
                                        </a>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">Zeeshan Iqbal</div>
                                    <div class="col-sm-4">zeeshanbutt223@gmail.com</div>
                                    <div class="col-sm-2">
                                        <a href="/trains/1oqdzq/message/k94wz92/" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-envelope" alt="Send email" title="Send email"></i></a>
                                        <a href="#" data-id="y2q0351"
                                           class="btn btn-participants-remove btn-danger btn-sm">
                                            <i class="fa fa-trash" alt="Remove" title="Remove"></i>
                                        </a>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>

                                <a class="btn btn-default" href="/trains/1oqdzq/invite/">
                                    <i class="fa fa-envelope"></i>&nbsp;Invite Others</a>

                                <a class="btn btn-default" title="Email all" href="/trains/1oqdzq/message/new/">
                                    <i class="fa fa-comments"></i>&nbsp;Email all</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-journaldelete-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="{{ $url."/updates/" }}"
                              method="post">
                            @csrf
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Confirm Delete</h4>
                            </div>
                            <div class="modal-body">
                                <br>
                                <p><strong>You are deleting the following update</strong></p>
                                <p class="post_title"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Back</button>
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-storyedit-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title">About this Gestr page</h4>
                        </div>
                        <div class="modal-body">
                            <form id="mt-form-story" autocomplete="off">
                                <div id="pg-storyedit-status" class="alert hidden"></div>
                                <div class="form-group">
                                    <label for="PostText">Tell the story</label>
                                    <div>
                                        <textarea id="Story" class="form-control about-placeholder placeholder"
                                                  name="Story" rows="15"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-success btn-storyedit-save">
                                <i class="fa fa-check"></i>&nbsp;Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>