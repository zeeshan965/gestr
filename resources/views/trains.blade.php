@section( 'extra_css' )
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset ( 'css/details.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.fileupload.css' ) }}"/>

    <link rel="stylesheet" href="{{ asset ( 'css/demo.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/theme2.css' ) }}"/>

    <style>
        .ev-margin {
            margin: 0px 0 10px 0px;
        }

        .event-wrapper {
            width: 100%;
        }

        .event-wrapper .height-center {
            transform: translateY(5px);
        }

        .mt {
            margin-top: 20px;
        }

        .pg-photo-box {
            background-color: #C9C9C9;
            border: 0;
            border-radius: 5px;
            color: #fff;
            font-size: 20px;
            margin: 0 auto;
            overflow: hidden;
            padding: 20% 0;
            text-align: center;
            width: 80%;
        }

        .f-14 {
            font-size: 14px;
        }

        .tab-content > .active {
            display: block;
            visibility: visible;
        }

        .height-center {
            position: relative;
            top: 50%;
            transform: translateY(25%);
        }

    </style>

    @if( $meal -> package_payment == 'Plus' )
        <style>
            .span-wrap {
                width: 100%;
                height: 100px;
                float: left;
                display: none;
            }

            .cld-day {
                height: 150px;
                border-right: none;
            }

            .cld-main a {
                display: table;
                width: 100%;
                margin-bottom: 2px;
                font-size: 12px;
                word-break: break-word;
            }

            .cld-number.eventday {
                height: 100%;
            }

            .nav-header-container {
                height: 0px;
            }

            .nav-justified-sm.nav-header {
                background-color: #fff;
                border-bottom: 3px solid #01467F;
                border-top: 3px solid #01467F;
                margin: 0 -15px 0 -15px;
            }

            @media (max-width: 767px) {
                .nav-header-container {
                    height: auto;
                }
            }

        </style>
    @endif
@endsection
@section( 'extra_js' )
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/api.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.ui.widget.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.fileupload.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/about-placeholder.js' ) }}"></script>

    @if( $meal -> package_payment == 'Plus' )
        <script type="text/javascript" src="{{ asset ( 'js/plus-calendar.js' ) }}"></script>
    @else
        <script type="text/javascript" src="{{ asset ( 'js/caleandar.js' ) }}"></script>
    @endif

    <script type="text/javascript">
        var dateArr = [];
        let url = window.location.href;
        const link = new URL ( url );
        // if ( link.searchParams.get ( 'intro' ) == 'true' ) {
        //     url = window.location.href.split ( "?" )[ 0 ];
        //     history.replaceState ( null, "", url );
        //     console.log ( url );
        // }

        $ ( "#fileupload" ).fileupload ( {
            url : url + '/upload',
            dataType : "json",
            autoUpload : false,
            maxNumberOfFiles : 1,
            formData : { _token : $ ( 'meta[name="csrf-token"]' ).attr ( 'content' ) },
            done : function ( e, data ) {
                $ ( ".progress.fileupload-progress, .pg-photo-box" ).hide ();
                $ ( "#file-upload-img" ).attr ( "src", data.result.data ).show ();
                $ ( ".btn-remove" ).show ();
                fileUploadDone ( data.result.data );
            },
            add : function ( e, data ) {
                let elem = $ ( "#file-upload-status" );
                elem.hide ();
                if ( data.files && data.files[ 0 ] && data.files[ 0 ].size && data.files[ 0 ].size > 8388608 ) {
                    let message = "File size limit is 8 MB.Please reduce the size of the image and try again.";
                    elem.removeClass ( "hidden alert-success alert-danger" )
                        .html ( message || "The action did not complete. Please try again." ).addClass ( "alert-danger" ).fadeIn ( "fast" );
                } else {
                    console.log ( data );
                    data.submit ();
                }
            },
            fail : function ( e, data ) {
                console.log ( data )
                $ ( ".progress.fileupload-progress" ).hide ();
                let message = data.jqXHR.responseJSON.messages;
                $ ( "#file-upload-status" ).removeClass ( "hidden alert-success alert-danger" )
                    .html ( message || "The action did not complete. Please try again." ).addClass ( "alert-danger" ).fadeIn ( "fast" );
            },
            progressall : function ( e, data ) {
                var progress = parseInt ( data.loaded / data.total * 100, 10 );
                $ ( ".progress.fileupload-progress .progress-bar" ).css ( "width", progress + "%" );
            },
            start : function ( e, data ) {
                $ ( ".progress.fileupload-progress .progress-bar" ).css ( "width", "0%" );
                $ ( ".progress.fileupload-progress" ).show ();
            }
        } ).prop ( 'disabled', ! $.support.fileInput ).parent ().addClass ( $.support.fileInput ? undefined : 'disabled' );

        $ ( ".btn-remove" ).click ( function ( e ) {
            e.preventDefault ();
            api_call ( url + '/remove_photo', 'post', new FormData () ).then ( ( response ) => {
                $ ( "#file-upload-img" ).attr ( "src", "" ).hide ();
                $ ( ".pg-photo-box" ).show ();
                fileUploadDone ( '' );
            } ).catch ( err => {
                console.log ( err )
            } );
        } );

        $ ( ".btn-story-add" ).click ( function ( e ) {
            e.preventDefault ();
            console.log ( 123123 )
            $ ( "#pg-storyedit-modal" ).modal ( { show : true } );
        } );

        $ ( ".btn-volunteer-info, .btn-volunteer-info" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( "#pg-info-modal" ).modal ( { show : true } );
        } );

        $ ( ".btn-viewmode" ).on ( "click", function ( e ) {
            e.preventDefault ();
            $ ( this ).tab ( "show" );
            $ ( ".btn-viewmode" ).toggleClass ( "active" );
        } );

        ( function () {
            api_call ( url + '/fetch_events', 'get' ).then ( ( response ) => {
                let events = [];
                $.each ( response.data, function ( index, item ) {
                    dateArr.push ( item.Date.replaceAll ( '/', '-' ) )
                    item.Date = new Date ( item.Date );
                    //let obj = { 'Date' : new Date ( item.Date ), 'Title' : item.Title, 'Link' : item.Link };
                    events.push ( item )

                } );
                var settings = {};
                var element = document.getElementById ( 'caleandar' );
                caleandar ( element, events, settings );
            } ).catch ( err => {
                console.log ( err )
            } );


        } ) ();

        @if( Auth::check () && (Auth::id () == $meal->organizer && $meal -> intro == 0 || $meal->users->where('id', Auth::user() -> id)) )
        $ ( "#pg-trainintro-modal" ).modal ( { show : true } );
        @endif

        function disableTipsPopup () {
            api_call ( url + '/disable_tips', 'get' ).then ( ( response ) => {
                $ ( "#pg-trainintro-modal" ).modal ( 'hide' );
            } ).catch ( err => {
                console.log ( err )
            } );
        }

        function fileUploadDone ( path ) {
            $ ( "#pg-train-photo > img" ).attr ( "src", path );
            if ( path == null || path == "" ) {
                $ ( "#pg-train-photo" ).addClass ( 'hide' );
                $ ( ".btn-photo-box" ).removeClass ( 'hide' );
            } else {
                $ ( ".btn-photo-box" ).addClass ( 'hide' );
                $ ( "#pg-train-photo" ).removeClass ( 'hide' );
            }
        }

        $ ( ".btn-view-past" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( '#past-view' ).toggleClass ( 'hide' );
        } );

    </script>
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __("Gestr for {$meal -> recipient_name}") }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                    @if( Auth::check () && (Auth::id () == $user -> id || $meal->users->where('id', Auth::user() -> id)) )
                        <div class="col-md-12">
                            <a class="btn btn-info btn-lg" title="Post an update" href="{{ $url.'/updates/new' }}">
                                <i class="fa fa-comments-o"></i>&nbsp;Post an update
                            </a>
                            <a class="btn btn-info btn-lg" title="Make changes" href="{{ $url.'/edit' }}">
                                <i class="fa fa-pencil"></i>&nbsp;Make changes
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </x-slot>

    <input type="hidden" id="train_id" value="{{ $meal -> unique_id ?? '' }}"/>
    <div class="container-fluid">
        <div class="row mt">
            <div class="col-sm-3 col-md-4">
                @if( Auth::check () && (Auth::id () == $user -> id || $meal->users->where('id', Auth::user() -> id)) )
                    <div class="btn-photo-container">
                        <div class="btn-admin-box btn-photo-box btn-block @if( $meal -> attachment != null ) hide @endif">
                            <button type="button" id="btn-photo-new" data-toggle="modal" data-target="#pg-modal"
                                    class="btn btn-primary">
                                <i class="fa fa-plus"></i>&nbsp;Add a Photo
                            </button>
                            <br>
                            <p>Adding a photo can lead<br>to greater participation</p>
                        </div>
                        <div id="pg-train-photo"
                             class="text-center round5 @if( $meal -> attachment == null ) hide @endif">
                            <img @if( $meal -> attachment != null ) src="{{ url ( "uploads/{$meal -> attachment -> url}" ) }}" @endif>
                        </div>
                    </div>
                @elseif( $meal -> attachment != null )
                    <div class="btn-photo-container">
                        <div id="pg-train-photo"
                             class="text-center round5 @if( $meal -> attachment == null ) hide @endif">
                            <img @if( $meal -> attachment != null ) src="{{ url ( "uploads/{$meal -> attachment -> url}" ) }}" @endif>
                        </div>
                    </div>
                @endif

                <div class="visible-xs-block">
                    <div class="nav-header-container">
                        <ul class="nav nav-justified-sm nav-header affix" data-toggle="affix">
                            <li class="active">
                                <a href="{{ $url }}">
                                    <i class="fa fa-calendar"></i>
                                    <span class="visible-xs-inline"><br></span>
                                    Calendar
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ $url.'/updates' }}">
                                    <i class="fa fa-comment-o"></i>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <span class="visible-xs-inline"><br></span>Updates
                                    @if( $meal -> posts -> count() > 0 )
                                        <span class="badge">{{ $meal -> posts -> count() }}</span>
                                    @else
                                        <br> <span class="text-mt">New!</span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <strong class="f-14">Organizer</strong>
                        <p style="margin: 0;line-height: 0">
                            <a class="f-14" href="{{ $url.'/message?user='.$user -> id }}">
                                <i class="fa fa-envelope"></i>&nbsp;{{ $user -> first_name }}
                            </a>
                        </p>

                        <strong class="f-14">Recipient</strong>
                        <p style="margin: 0 0 10px 0;line-height: 0">
                            @if( $meal -> meal_recipient !== null )
                                <a href="{{ $url.'/message?meal='.$meal -> meal_recipient -> id }}">
                                    <i class="fa fa-envelope"></i>&nbsp;{{ $meal -> recipient_name }}
                                </a>
                            @else
                                <a href="{{ $url.'/message?user='.$meal -> user_id }}">
                                    <i class="fa fa-envelope"></i>&nbsp;{{ $meal -> recipient_name }}
                                </a>
                            @endif
                        </p>
                        <p style="margin: 0;line-height: 0">
                            <a href="javascript:;" class="btn-participants">View/Email all participants&nbsp;
                                <span class="badge f-14">{{ count($meal -> recipients) }}</span>
                            </a>
                        </p>

                        <div class="hidden-xs">
                            <br>
                            <p>
                                <input id="url" name="url" type="text" class="form-control"
                                       value="{{ $url }}" onfocus="this.select();">
                            </p>
                            <a class="btn btn-mt btn-block" href="{{ $url.'/invite' }}">
                                <i class="fa fa-envelope"></i>&nbsp;Invite</a>
                            <a class="btn btn-fb btn-block"><i class="fa fa-facebook"></i>&nbsp;Share</a>
                            <a class="btn btn-tw btn-block"><i class="fa fa-twitter"></i>&nbsp;Tweet</a>
                            <div><small><a data-toggle="modal" href="#pg-invitehelp-modal"><i
                                                class="fa fa-question-circle"></i>&nbsp;Invitation help?</a></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 col-md-8">
                @if( Auth::check () && (Auth::id () == $user -> id || $meal->users->where('id', Auth::user() -> id)) )
                    <div class="hidden-xs">
                        <div class="pg-train-story halfpad-bottom"
                             @if( $meal -> body == '' || $meal -> body == null ) style="display:none;" @endif>
                            <h4><strong>About this Gestr page</strong></h4>
                            <p style="white-space: pre-wrap;">{{ $meal -> body }}</p>
                        </div>
                        <div class="pg-train-story-add halfpad-bottom"
                             @if( $meal -> body != '' && $meal -> body != null ) style="display:none;" @endif>
                            <div class="btn-admin-box pg-story-edit-box">
                                <p>Let everyone know why you are organizing this Gestr page.</p>
                                <button class="btn btn-primary btn-story-add">
                                    <i class="fa fa-pencil"></i>&nbsp;Tell the story
                                </button>
                            </div>
                        </div>
                    </div>
                @elseif( $meal -> body != null )
                    <div class="hidden-xs">
                        <div class="pg-train-story halfpad-bottom">
                            <h4><strong>About this Gestr page</strong></h4>
                            <p style="white-space: pre-wrap;">{{ $meal -> body }}</p>
                        </div>
                    </div>
                @endif
                <div class="hidden-xs">
                    <div class="nav-header-container" style="height: 46px;">
                        <ul class="nav nav-justified-sm nav-header affix" data-toggle="affix">
                            <li class="active">
                                <a href="{{ $url }}">
                                    <i class="fa fa-calendar"></i>
                                    <span class="hidden-xs">&nbsp;</span><span class="visible-xs-inline"><br></span>Calendar
                                    <span class="visible-xs-inline"><br>&nbsp;</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ $url.'/updates' }}">
                                    <i class="fa fa-comment-o"></i>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <span class="visible-xs-inline"><br></span>Updates
                                    <span class="visible-xs-inline"><br></span>
                                    @if( $meal -> posts -> count() > 0 )
                                        <span class="badge">{{ $meal -> posts -> count() }}</span>
                                    @else
                                        <span class="visible-xs-inline"><br></span><span class="text-mt">New!</span>
                                    @endif
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-default hidden-xs">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <strong>Location</strong><br>
                                {{ $meal -> recipient_address }} <br>
                                {{ $meal -> recipient_city }} {{ $meal -> recipient_state }} {{ $meal -> recipient_postal_code }}
                                <br>
                                {{ $meal -> recipient_phone }} <br>
                                <a href="https://maps.google.com/maps?f=q&amp;hl=en&amp;q={{ urlencode ( $meal -> recipient_address ) }}"
                                   target="_blank">
                                    <i class="fa fa-map-marker"></i>&nbsp;Map</a>
                            </div>
                            <div class="col-sm-4">
                                <strong>Number of People</strong><br>
                                <p>
                                    Adults:&nbsp;{{ $meal -> adults_cook_for }}&nbsp;&nbsp;
                                    Kids:&nbsp;{{ $meal -> kids_cook_for }}
                                </p>
                                <strong>Preferred delivery time</strong><br>
                                {{ $meal -> delivery_time }}
                            </div>
                            <div class="col-sm-4">
                                <strong>Special Instructions</strong><br>
                                <span id="special_instructions">{{ $meal -> special_instructions }}</span>
                                <a class="btn-volunteer-info" href="javascript:;">read more</a>
                            </div>
                        </div>
                    </div>
                </div>

                <h4><strong>Calendar</strong></h4>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="btn pull-right hidden-xs" title="Print" href="{{ $url."/print" }}"
                           target="_blank"><i class="fa fa-lg fa-print"></i></a>
                        <div style="">
                            <a class="btn btn-mt-yellow btn-volunteer-info" title="Review All Instructions" href="#"><i
                                        class="fa fa-info-circle"></i>&nbsp;Review All Instructions</a>
                        </div>
                        <ul class="tabs-slide hidden-xs">
                            <li>
                                <a class="btn-viewmode active" data-viewmode="0" title="View as list"
                                   href="#pg-calendar-list"
                                   role="tab" data-toggle="tab">
                                    <i class="fa fa-bars"></i><span class="hidden-xs">&nbsp;List View</span>
                                </a>
                            </li>
                            <li>
                                <a class="btn-viewmode" data-viewmode="1" title="View as calendar"
                                   href="#pg-calendar-month" role="tab" data-toggle="tab">
                                    <i class="fa fa-calendar"></i><span class="hidden-xs">&nbsp;Calendar View</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="tab-content">
                                    <div id="pg-calendar-list" class="tab-pane fade active in">
                                        <div class="text-center panel-view-past">
                                            <a class="btn-view-past btn-sm" href="#">view any past dates&nbsp;
                                                <i class="fa fa-caret-down"></i>
                                            </a>
                                        </div>
                                        <div id="past-view" class="hide">
                                            @foreach( $meal -> oldDateSlots as $old_slot )
                                                @if( $old_slot -> events -> count() > 0 )
                                                    @include( 'partials.plus-plan-events', [ 'slot' => $old_slot, 'old' => true, 'str' => 'This date has passed' ] )
                                                @else
                                                    @include( 'partials.basic-plan-events', [ 'slot' => $old_slot, 'url' => $url, 'old' => true, 'str' => 'This date has passed' ] )
                                                @endif
                                            @endforeach
                                        </div>

                                        <div id="pg-calendar-list-view">
                                            @foreach( $meal -> dateSlots as $slot )
                                                @if( $slot -> events -> count() > 0 )
                                                    @include( 'partials.plus-plan-events', [ 'slot' => $slot, 'old' => false, 'str' => 'This date is available' ] )
                                                @else
                                                    @include( 'partials.basic-plan-events', [ 'slot' => $slot, 'url' => $url, 'old' => false, 'str' => 'This date is available' ] )
                                                @endif
                                            @endforeach
                                        </div>

                                        @if( $meal -> dateSlots -> count() == 0 )
                                            <div id="pg-calendar-full" class="halfpad-top">
                                                <div class="jumbotron">
                                                    <strong>There are no available dates to select on this Gestr
                                                            page</strong>
                                                    <br><br>
                                                    <div>
                                                        Please
                                                        <a href="{{ $url.'/message/e7n319o' }}">
                                                            contact the organizer</a> to request more dates.
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div id="pg-calendar-month" class="tab-pane fade">
                                        <div class="row">
                                            <div class="col-sm-12 text-right">
                                                <strong>Key:</strong>&nbsp;
                                                <i class="fa fa-square" style="color:#5cb85c;"></i>&nbsp;Available&nbsp;&nbsp;
                                                <i class="fa fa-square" style="color:#3a87ad;"></i>&nbsp;Booked
                                                <div id="caleandar"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="pg-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                            <h4 class="modal-title">Add a Photo</h4>
                        </div>
                        <div class="modal-body">
                            <form id="mt-form-photo" action="" method="post">
                                <div class="text-center">
                                    <div class="pg-photo-box"><i class="fa fa-photo fa-2x"></i></div>
                                    <img id="file-upload-img" style="display:none;"/>
                                    <p>
                                    <div class="btn btn-success btn-responsive-block fileinput-button">
                                        <i class="fa fa-plus"></i>&nbsp;Select new photo...
                                        <input id="fileupload" type="file" name="files" accept="image/*"/>
                                    </div>
                                    <a class="btn btn-default btn-responsive-block btn-remove" style="display:none;"
                                       href="javascript:;" data-loading-text="Removing...">
                                        <i class="fa fa-trash"></i>&nbsp;Remove photo</a>
                                    </p>
                                    <div class="progress fileupload-progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                             aria-valuemax="100" style="width:0%;"></div>
                                    </div>
                                    <div id="file-upload-status" class="alert hidden"></div>
                                </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-trainintro-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Quick Tip</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row pad-bottom">
                                <div class="col-sm-offset-2 col-sm-8">
                                    <p><strong>You can enhance your Gestr page any time:</strong></p>
                                    <p>Look for the <a href="javascript:;" class="btn btn-xs btn-default"><i
                                                    class="fa fa-pencil"></i>&nbsp;Make changes</a> button.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-offset-2 col-sm-8">
                                    <p><strong>Click the button to:</strong></p>
                                    <ul class="checklist">
                                        <li>Add dates to the calendar</li>
                                        <li>Edit event details</li>
                                        <li>Activate donations</li>
                                        <li>Upgrade to Gestr Plus</li>
                                        <li>Remove Ads</li>
                                        <li>And more...</li>
                                    </ul>
                                    <br>
                                    <p><small>Note: You must be signed in to see the button</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="disableTipsPopup()" class="btn btn-default">Do not show tips!
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">View My Gestr
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-invitehelp-modal" tabindex="-1" role="dialog"
                 aria-labelledby="pg-ok-modal-label" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Invitation Options</h4>
                        </div>
                        <div class="modal-body">
                            <h4>Here is the unique link for this Gestr Page</h4>
                            <p><input id="InviteHelpUrl" name="InviteHelpUrl" type="text" class="form-control"
                                      value="{{ $url }}" onfocus="this.select();"></p>
                            <br>
                            <p><strong>Personal Email</strong><br>Copy and paste the link above into your favorite email
                                                                  program (gmail, yahoo mail, outlook, etc).</p>
                            <br>
                            <p><strong>Via Gestr.com</strong>&nbsp;(PC or Tablet)<br>Click the orange Invite button
                                                             on the Calendar tab.</p>
                            <br>
                            <p><strong>Mobile Device</strong><br>Option 1: Click the orange Invite button to access your
                                                                 phone's address book.<br>Option 2: Click "View all
                                                                 participants" then the "Invite Others" button to invite
                                                                 via Gestr.com.</p>
                            <br>
                            <p><strong>Text Message</strong><br>While viewing the Gestr, click your phone's Share
                                                                button (usually <i class="fa fa-share-alt"></i> or <i
                                        class="fa fa-share-square-o"></i>).</p>
                            <br>
                            <p><strong>Facebook</strong><br>Click the blue "f Share" button.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-storyedit-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title">About this Gestr page</h4>
                        </div>
                        <div class="modal-body">
                            <form id="mt-form-story" autocomplete="off">
                                <div id="pg-storyedit-status" class="alert hidden"></div>
                                <div class="form-group">
                                    <label for="PostText">Tell the story</label>
                                    <div>
                                        <textarea id="Story" class="form-control about-placeholder placeholder"
                                                  name="Story" rows="15"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="button"
                                    class="btn btn-success btn-storyedit-save ladda-button ladda-button-demo"
                                    data-loading-text="Saving..." data-style="zoom-in">
                                <span class="ladda-label">Save Changes&nbsp;
                                    <i class="fa fa-check"></i>
                                </span>
                                <span class="ladda-spinner"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="pg-info-modal" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Gestr Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label">Organizer</label>
                                <p class="form-control-static">
                                    <a href="{{ $url.'/message' }}">
                                        <i class="fa fa-envelope"></i>
                                        &nbsp;{{ $meal -> user -> full_name }}
                                    </a>
                                </p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Recipient</label>
                                <p class="form-control-static">
                                    @if( $meal -> meal_recipient !== null )
                                        <a href="{{ $url.'/message?meal='.$meal -> meal_recipient -> id }}">
                                            <i class="fa fa-envelope"></i>&nbsp;{{ $meal -> recipient_name }}
                                        </a>
                                    @else
                                        <a href="{{ $url.'/message?user='.$meal -> user_id }}">
                                            <i class="fa fa-envelope"></i>&nbsp;{{ $meal -> recipient_name }}
                                        </a>
                                    @endif
                                </p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Location</label>
                                <p class="form-control-static">{{ $meal -> recipient_address }}<br>
                                    {{ $meal -> recipient_city }} &nbsp; {{ $meal -> recipient_state }} &nbsp;
                                    {{ $meal -> recipient_postal_code }} <br> {{ $meal -> recipient_phone }}
                                    <br>
                                    <a href="https://maps.google.com/maps?f=q&amp;hl=en&amp;q={{ urlencode ( $meal -> recipient_address ) }}"
                                       target="_blank">
                                        <i class="fa fa-map-marker"></i>&nbsp;Map
                                    </a>
                                </p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Preferred delivery time</label>
                                <p class="form-control-static">{{ $meal -> delivery_time }}</p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Number of People</label>
                                <p class="form-control-static">
                                    Adults:&nbsp;{{ $meal -> adults_cook_for }}&nbsp;&nbsp;
                                    Kids:&nbsp;{{ $meal -> kids_cook_for }}
                                </p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Special Instructions</label>
                                <p class="form-control-static">
                                    <span style="white-space: pre-wrap;">{{ $meal -> special_instructions }}</span>
                                </p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Favorite meals/restaurants</label>
                                <p class="form-control-static"><span
                                            style="white-space: pre-wrap;">{{ $meal -> fave_meals_rest }}</span></p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Least Favorite meals</label>
                                <p class="form-control-static"><span
                                            style="white-space: pre-wrap;">{{ $meal -> least_fave_meals }}</span></p>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Allergies or dietary restrictions</label>
                                <p class="form-control-static"><span
                                            style="white-space: pre-wrap;">{{ $meal -> restrictions }}</span></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</x-app-layout>
