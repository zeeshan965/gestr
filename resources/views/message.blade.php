@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.tagit.css' ) }}"/>
    <style>
        .mt-buttonbar > .mt-buttonbar-buttons {
            padding: 10px 0;
        }

    </style>
@endsection

@section( 'extra_js' )
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script type="text/javascript">
        $ ( ".form-invite" ).validate ( {
            rules : {},
            submitHandler : function ( form ) {
                console.log ( form )
                form.submit ();
            }
        } );
    </script>
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __('Message') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="container-fluid">
        <h1>Send a Message</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-invite form-horizontal" action="{{ url ( "trains/{$id}/message" ) }}" method="post">
                    @csrf
                    <input type="hidden" name="email" value="{{ $email }}" />
                    <div class="form-group">
                        <label for="To" class="col-sm-2 control-label">To</label>
                        <div class="col-sm-8">
                            <p style="padding-top: 7px;margin: 0">{{ $email }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Subject" class="col-sm-2 control-label">Subject</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="Subject" name="Subject"
                                   value="" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Body" class="col-sm-2 control-label">Message</label>
                        <div class="col-sm-8">
                            <textarea id="Body" class="form-control" name="Body"
                                      rows="7" required></textarea>
                            <small>TIP: A link to the Gestr page will automatically be added to the bottom of this
                                   message.</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Message" class="col-sm-2 control-label">CC</label>
                        <div class="col-sm-8">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" id="CC" name="CC" value="true" checked>
                                    Send me a copy of this message.
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div style="margin-left: 17.8%;">
                            <div class="mt-buttonbar mt-buttonbar-default margin-bottom">
                                <div class="mt-buttonbar-buttons">
                                    <a class="btn btn-default btn-back-confirm"
                                       href="{{ url ( "trains/{$id}/activation" ) }}"><i
                                                class="fa fa-caret-left"></i>&nbsp;Back</a>
                                    <button class="btn btn-success btn-submit">
                                        <i class="fa fa-check"></i>&nbsp;Send
                                    </button>
                                    <div class="alert hidden alert-status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
