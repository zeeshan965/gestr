@section( 'extra_css' )
    <style>
        .btn-fb {
            color: #fff;
            background-color: #3a5795;
            border-color: #274E9F;
        }

        .btn-fb:hover {
            color: #fff;
            background-color: #3a5795e3;
            border-color: #3a5795e3;
        }

        .btn-tw {
            color: #fff;
            background-color: #04ACEE;
            border-color: #04ACEE;
        }

        .btn-tw:hover {
            color: #fff;
            background-color: #04aceee3;
            border-color: #04aceee3;
        }
    </style>
@endsection

@section( 'extra_js' )
    <script type="text/javascript">
        ( function ( $, window, document ) {
            $ ( function () {
                $ ( ".btn-fb" ).click ( function ( e ) {
                    e.preventDefault ();
                    //FB.ui ( { method : "share", href : "https://mealtrain.com/1oqdzq" }, function ( response ) {} );
                } );
                $ ( ".btn-tw" ).click ( function ( e ) {
                    e.preventDefault ();
                    //window.open ( "https://twitter.com/intent/tweet?text=Meal%20Train%20for%20Smith%20Family&url=https%3A%2F%2Fmealtrain.com%2F1oqdzq&via=mealtrain" );
                    return false;
                } );
                console.log ( $ ( "#pg-ok-modal" ) )
                $ ( "#pg-ok-modal" ).modal ( { show : true } );
            } );
        } ( window.jQuery, window, document ) );
    </script>
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __('Setup is Complete') }}</x-slot>
    <x-slot name="headerContent"></x-slot>

    <div class="container-fluid">
        <h1>Congratulations</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="alert alert-success">
                    <i class="fa fa-check"></i>&nbsp;Your Gestr page is complete. Time to share!
                </div>
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <h4>Here is the unique link for your Gestr page:</h4>
                        <div class="input-group margin-bottom-tiny">
                            <span class="input-group-addon"><i class="fa fa-link"></i></span>
                            <input id="url" name="url" type="text" class="form-control"
                                   value="{{ url ('/trains',[ $id ]) }}" onfocus="this.select();">
                        </div>
                        <br>
                        <p><i class="fa fa-caret-right"></i>&nbsp;For easy invitations, simply copy &amp; paste this
                                                            link into an email.</p>
                        <p><i class="fa fa-caret-right"></i>&nbsp;TIP: You can always find this link right on your Gestr
                                                            page.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-offset-3 col-sm-6">
                        <h2>Next Step: Invite Others</h2>
                    </div>
                </div>
                <div class="row pad-bottom">
                    <div class="col-sm-offset-3 col-sm-6">
                        <a class="btn btn-success btn-block visible-xs-block"
                           href="mailto:?subject={{ rawurlencode ( "Gestr for {$meal -> recipient_name}" ) }}&amp;body={{ $invitation_body }}"><i
                                    class="fa fa-envelope"></i>&nbsp;Email</a>
                        <a class="btn btn-success btn-block hidden-xs" href="{{ url ( "trains/{$id}/invite" ) }}"><i
                                    class="fa fa-envelope"></i>&nbsp;Email Invitations</a>
                        <a class="btn btn-fb btn-block"><i class="fa fa-facebook"></i>&nbsp;Share</a>
                        <a class="btn btn-tw btn-block"><i class="fa fa-twitter"></i>&nbsp;Tweet</a>
                        <br>
                        <a class="btn btn-default btn-block" href="{{ url ( "trains/{$id}?intro=true" ) }}">
                            View My Gestr page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pg-ok-modal" tabindex="-1" role="dialog" aria-labelledby="pg-ok-modal-label"
         aria-hidden="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Quick Tip</h4>
                </div>
                <div class="modal-body">
                    <div class="text-center pad-bottom pad-top">
                        <img alt="My Profile Button" title="My Profile Button"
                             src="{{ asset ( 'images/Profile_Button.gif' ) }}"/>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <p>You can manage all of your events by clicking <span
                                        class=" hidden-xs">your name</span><span
                                        class="visible-xs-inline">the menu icon</span> in the upper right corner of
                               every page.</p>
                            <p>It is an easy way to quickly jump back to your Gestr dashboard.</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
