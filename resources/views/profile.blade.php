@section('extra_css')
    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <style type="text/css">
        form {
            border: 3px solid #f1f1f1;
        }

        fieldset {
            display: none;
        }

        input[type=text], input[type=email], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
            height: 55px;
        }

        #register_btn {
            background-color: #04AA6D;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        #register_btn:hover {
            opacity: 0.8;
        }

        .btn-secondary {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
        }

        .btn-secondary:hover {
            color: #fff;
            background-color: #5a6268;
            border-color: #545b62;
        }

        .pass-label {
            position: relative;
            top: 50%;
            transform: translateY(95%);
        }
    </style>
@endsection

@section( 'extra_js' )
    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/api.js' ) }}"></script>
    <script src="{{ asset ( 'js/profile.js' ) }}"></script>

@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __("Profile") }}</x-slot>
    <x-slot name="headerContent"></x-slot>
    <div class="container-fluid" style="margin-top: 8em;">
        <div class="row">
            <div class="col-sm-offset-3 col-sm-9">
                <h2 style="font-weight: 400;">My Account</h2>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-3">
                <div class="list-group">
                    <a class="list-group-item active" href="javascript:;">
                        <i class="fa fa-user fa-fw"></i>&nbsp; About Me
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <form method="POST" action="{{ route('profile') }}" id="profile_form" autocomplete="off"
                      style="padding: 10px">
                    <input type="hidden" name="_token" value="{{ csrf_token () }}"/>
                    <!-- First Name -->
                    <div class="form-group">
                        <x-label for="first_name" :value="__('First Name')"/>
                        <x-input type="text" class="form-control" id="first_name" placeholder="Enter First Name"
                                 name="first_name" value="{{ $user -> first_name }}"
                                 required autofocus/>
                    </div>
                    <div class="form-group">
                        <x-label for="last_name" :value="__('Last Name')"/>
                        <x-input type="text" class="form-control" id="last_name" placeholder="Enter Last Name"
                                 name="last_name" value="{{ $user -> last_name }}"
                                 required autofocus/>
                    </div>
                    <!-- Email Address -->
                    <div class="form-group">
                        <x-label for="email" :value="__('Email')"/>
                        <x-input type="email" class="form-control" id="email" placeholder="Enter email"
                                 value="{{ $user -> email }}" readonly autofocus/>
                    </div>
                    <div class="form-group">
                        <label for="Password" class="control-label">Password</label>
                        <div><a id="btn-change-password" class="btn btn-secondary">Change password</a></div>
                    </div>

                    <div class="form-group">
                        <button id="register_btn" class="ladda-button ladda-button-demo" type="submit"
                                data-style="zoom-in">
                            <span class="ladda-label">Update Profile</span><span class="ladda-spinner"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="password-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="update-password" action="" method="post" class="form-horizontal" autocomplete="off">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="CurrentPassword" class="col-sm-3 pass-label">Current password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="currentPassword" name="currentPassword"
                                       placeholder="Current password" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Password" class="col-sm-3 pass-label">New password</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="New password" required="" aria-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Password2" class="col-sm-3 pass-label">Re-enter</label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"
                                       placeholder="Re-enter new password" required="" aria-required="true">
                            </div>
                        </div>
                        <div id="pg-password-status" class="alert hidden"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary btn-password-save">
                            Save Changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
