<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!--======================================================== Title =========================================================-->
    <title>{{ __("Gestr for {$meal -> recipient_name}") }} - {{ config('app.name', 'GESTR') }}</title>

    <!--======================================================== Header CSS Files =========================================================-->
    <link href="{{ asset( 'css/bootstrap.min.css' ) }}" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset( 'css/style.css' ) }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset ( 'css/details.css' ) }}"/>
    <style>
        .pageHeader {
            text-align: center;
            z-index: 2;
            position: relative;
            font-size: 30px;
            text-transform: uppercase;
            padding-top: 20px;
            padding-bottom: 22px;
            font-weight: 700;
            font-family: Dosis;
            color: #000;
        }

        .ev-margin {
            margin: 0px 0 5px 0px;
        }

        .event-wrapper {
            width: 100%;
        }

        .event-wrapper .height-center {
            transform: translateY(5px);
        }
    </style>
</head>
<body onload="javascript:window.print();">
<header>
    <div class="wrapper">
        <!-- Navigation Bar -->
        <div class="container">
            <div class="navbar-header">
                <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle"
                        type="button">
                    <i class="fa fa-bars"></i>
                    <span class="sr-only">Toggle navigation</span>
                </button>
                <img src="{{ asset ( 'images/logo.gif' ) }}" id="logo-header">
            </div>
            <div id="navbar-collapse-02" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="propClone">
                        <a class="in-active" id="btnPrint" title="Print" href="#print"
                           onclick="window.print();"><i class="fa fa-print"></i></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="pageHeader">
                        <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                            <strong>Gestr for</strong>
                            <p>{{ $meal -> recipient_name }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="row">
    <div class="col-sm-12">
        <div class="well">
            <label>Invitation Link:</label>
            <p>{{ url ( "/trains/{$meal -> unique_id}" ) }}</p>
            <label>Organizer:</label>
            <p>{{ $meal -> user -> full_name }}</p>
            <label>Location</label>
            <p>
                {{ $meal -> recipient_address }} <br>
                {{ $meal -> recipient_city }} {{ $meal -> recipient_state }} {{ $meal -> recipient_postal_code }}
                <br>
                {{ $meal -> recipient_phone }} <br></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        @foreach( $meal -> allSlots as $slot )
            @if( $slot -> events -> count() > 0 )
                <div class="row pg-list-row">
                    <div class="col-sm-12 col-xs-12 margin-bottom">
                        <h4 class="h-tight">
                            {{ $slot -> dateslots -> format ( 'l' ) }}
                            {{ $slot -> dateslots -> format ( 'd M, Y' ) }}
                        </h4>
                    </div>
                    @foreach( $slot -> events as $event )
                        <div class="event-wrapper">
                            <div class="col-sm-12">
                                <h5 class="ev-margin">
                                    {{ $event -> category }} :
                                    @if( $event -> volunteer == null )
                                        @if( \Carbon\Carbon::now () -> format ( 'Y-m-d' ) >= $slot -> dateslots -> format ( 'Y-m-d' ) )
                                            <span style="font-weight: 400;">This date is passed</span>
                                        @else
                                            <a class="text-success" href="javascript:;">
                                                <span style="font-weight: 400;">This date is available</span>
                                            </a>
                                        @endif
                                    @else
                                        <span>
                                            <a href="javascript:;" class="text-mt">
                                                <i class="fa fa-envelope"></i>&nbsp;{{ $event -> volunteer -> assignedUser }}
                                            </a>
                                        </span>
                                        | {{ $event -> volunteer -> body }} |
                                        <small>({{ $event -> volunteer -> note }})</small>
                                    @endif
                                </h5>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="row pg-list-row">
                    <div class="col-sm-3 col-xs-4 text-center">
                        <h4 class="h-tight">{{ $slot -> dateslots -> format ( 'M d Y' ) }}</h4>
                        {{ $slot -> dateslots -> format ( 'l' ) }}
                    </div>
                    @if( $slot -> volunteer == null )
                        <div class="col-sm-5 col-xs-8 height-center">
                            @if( \Carbon\Carbon::now () -> format ( 'Y-m-d' ) >= $slot -> dateslots -> format ( 'Y-m-d' ) )
                                <strong>This date is passed</strong>
                            @else
                                <a class="text-success" href="javascript:;">
                                    <strong>This date is available</strong>
                                </a>
                            @endif
                        </div>
                    @else
                        <div class="col-sm-5 col-xs-8">
                            @php
                                $assignedUser = json_decode ( $slot -> volunteer -> assignedUser, true );
                                if( isset( $assignedUser['id'] ) && $assignedUser['id'] != null ){
                                    $name = $assignedUser['first_name'] .' '.$assignedUser['last_name'];
                                }else{
                                    $name = $slot -> volunteer -> user -> full_name;
                                }
                            @endphp
                            <strong>
                                <a href="javascript:;" class="text-mt">
                                    <i class="fa fa-envelope"></i>&nbsp;{{ $name }}
                                </a>
                            </strong>
                            <br>{{ $slot -> volunteer -> body }}<br>
                            <small>({{ $slot -> volunteer -> note }})</small>
                        </div>
                    @endif

                </div>
            @endif

        @endforeach

    </div>
</div>
<footer id="footer">
    <div id="footer-container" class="container-fluid">
        <div id="copyright" class="row">
            <div class="col-sm-12 text-center">
                Gestr © 2021
            </div>
        </div>
    </div>
</footer>

</body>
</html>