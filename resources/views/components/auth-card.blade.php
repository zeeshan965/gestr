<section class="item content" style="margin-top: 200px;">
    <div class="container toparea toparea-contact">
        <div class="underlined-title">
            <div class="editContent">
                <h1 class="text-center latestitems">{{ $authLabel }}</h1>
            </div>
            <div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
            </div>
        </div>
        {{ $slot }}
    </div>
</section>