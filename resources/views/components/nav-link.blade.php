@props(['active','name'])

@php
    $classes = ($active ?? false) ? 'active' : 'in-active';
    $id = $name ?? '';
@endphp

@if($name == 'drop_down')
    <li class="active dropdown">
        <a href="`javascript:;`" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ Auth::user()->first_name }} &nbsp;<span class="caret"></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-right" role="menu">
            <li><a href="{{ route ( 'dashboard' ) }}">My Dashboard</a></li>
            <li><a href="{{ route ( 'profile' ) }}">My Account</a></li>
            <li class="divider"></li>
            <li>
                <form method="POST" action="{{ route( 'logout' ) }}">
                    @csrf
                    <a href="{{ route( 'logout' ) }}"
                       onclick="event.preventDefault();localStorage.removeItem('UserSession');localStorage.removeItem('organizer_id');
                       localStorage.removeItem('organizer_name'); this.closest( 'form' ).submit();">
                        Sign Out
                    </a>
                </form>
            </li>
        </ul>
    </li>
@else
    <li class="propClone" id="{{ $id }}">
        <a {{ $attributes->merge(['class' => $classes]) }}> {{ $slot }} </a>
    </li>
@endif