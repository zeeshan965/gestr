<!-- Navigation Bar -->
<nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                <i class="fa fa-bars"></i>
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a href="{{ route ( 'home' ) }}" class="navbar-brand brand">
                <img id="logo-header">
            </a>
        </div>
        <div id="navbar-collapse-02" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <x-nav-link :href="route('home')" :name="__('home')" :active="request()->routeIs('home')">
                    {{ __('Home') }}
                </x-nav-link>
                <x-nav-link :href="route('shop')" :name="__('shop')" :active="request()->routeIs('shop')">
                    {{ __('Shop') }}
                </x-nav-link>
                <x-nav-link :href="route('about')" :name="__('about')" :active="request()->routeIs('about')">
                    {{ __('About') }}
                </x-nav-link>
                <x-nav-link :href="route('member')" :name="__('member')" :active="request()->routeIs('member')">
                    @if( ! \Illuminate\Support\Facades\Auth::check () ) {{ __('Become A Member') }} @else {{ __('Get Started') }} @endif
                </x-nav-link>
                <x-nav-link :href="route('contact.index')" :name="__('contact')"
                            :active="request()->routeIs('contact')">
                    {{ __('Contact') }}
                </x-nav-link>

                @if( ! \Illuminate\Support\Facades\Auth::check () )
                    <x-nav-link :href="route('login')" :name="__('member_login_list')"
                                :active="request()->routeIs('login')">
                        {{ __('Member Login') }}
                    </x-nav-link>
                @endif
                @if( \Illuminate\Support\Facades\Auth::check () )
                    <x-nav-link :href="route('login')" :name="__('drop_down')"></x-nav-link>
                @endif
            </ul>
        </div>
    </div>
</nav>