<header class="item header margin-top-0">
    <div class="wrapper">
        <!-- Adding Nav Bar -->
        <x-navbar name="navbar"></x-navbar>
        <div class="container">
            <div class="row">
                {{ $headerContent }}
            </div>
        </div>
    </div>
</header>