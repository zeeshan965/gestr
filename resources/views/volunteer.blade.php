@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/details.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.fileupload.css' ) }}"/>

    <link rel="stylesheet" href="{{ asset ( 'css/demo.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/theme2.css' ) }}"/>

    <style>
        .mt {
            margin-top: 20px;
        }

        .pg-photo-box {
            background-color: #C9C9C9;
            border: 0;
            border-radius: 5px;
            color: #fff;
            font-size: 20px;
            margin: 0 auto;
            overflow: hidden;
            padding: 20% 0;
            text-align: center;
            width: 80%;
        }

        .f-14 {
            font-size: 14px;
        }

        .tab-content > .active {
            display: block;
            visibility: visible;
        }

        .height-center {
            position: relative;
            top: 50%;
            transform: translateY(25%);
        }

    </style>
@endsection
@section( 'extra_js' )
    <script type="text/javascript" src="{{ asset ( 'js/api.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>

    <script type="text/javascript">
        $ ( "#mt-form" ).validate ( {
            submitHandler : function ( form ) {
                console.log ( form )
                form.submit ();
            }
        } );

        $ ( "#itemVolunteer" ).change ( function () {
            if ( $ ( this ).val () == - 1 ) {
                $ ( "#first_name" ).rules ( "add", { required : true } );
                $ ( "#last_name" ).rules ( "add", { required : true } );
                $ ( "#email" ).rules ( "add", { required : true } );
                $ ( "#volunteer-info" ).collapse ( "show" );
                $ ( "#first_name" ).focus ();
            } else {
                $ ( "#first_name" ).rules ( "remove" );
                $ ( "#last_name" ).rules ( "remove" );
                $ ( "#email" ).rules ( "remove" );
                $ ( "#volunteer-info" ).collapse ( "hide" );
            }
        } );

        $ ( ".btn-delete-volunteer" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( "#pg-volunteer-remove-modal" ).find ( "p.heading strong" ).text ( "The scheduled participant will be cancelled AND the item will be deleted from the calendar. This item will not be available for others to book once deleted." );
            let url = '{{ url ( "trains/{$id}/volunteer/{$slotId}/delete/{$v_id}" ) }}';
            $ ( "#pg-volunteer-remove-modal .modal-dialog .modal-content .modal-footer .btn-danger" ).html ( '<i class="fa fa-close"></i>&nbsp;Delete Booking' );
            $ ( "#pg-volunteer-remove-modal .modal-dialog .modal-content form" ).attr ( 'action', url );
            $ ( "#pg-volunteer-remove-modal" ).modal ( { show : true } );
        } );

        $ ( ".btn-remove-volunteer" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( "#pg-volunteer-remove-modal" ).find ( "p.heading strong" ).text ( "The scheduled participant will be cancelled. This item will become available for others to book." );
            let url = '{{ url ( "trains/{$id}/volunteer/{$slotId}/cancel/{$v_id}" ) }}';
            $ ( "#pg-volunteer-remove-modal .modal-dialog .modal-content .modal-footer .btn-danger" ).html ( '<i class="fa fa-close"></i>&nbsp;Cancel Booking' );
            $ ( "#pg-volunteer-remove-modal .modal-dialog .modal-content form" ).attr ( 'action', url );
            $ ( "#pg-volunteer-remove-modal" ).modal ( { show : true } );
        } );

    </script>
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __("Gestr Updates for {$meal -> recipient_name}") }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="container-fluid">
        <h1>Volunteer</h1>
        <h4>Great! Your friend will appreciate the support!</h4>
        <div class="row">
            <div class="col-sm-4 col-md-3 hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label">Number of People</label>
                            <p class="form-control-static">
                                Adults:&nbsp;{{ $meal -> adults_cook_for }}&nbsp;&nbsp;
                                Kids:&nbsp;{{ $meal -> kids_cook_for }}
                            </p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Preferred delivery time</label>
                            <p class="form-control-static">{{ $meal -> delivery_time }}</p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Favorite meals/restaurants</label>
                            <p class="form-control-static">
                                <span style="white-space: pre-wrap;">{{ $meal -> fave_meals_rest }}</span>
                            </p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Least Favorite meals</label>
                            <p class="form-control-static">
                                <span style="white-space: pre-wrap;">{{ $meal -> least_fave_meals }}</span></p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Allergies or dietary restrictions</label>
                            <p class="form-control-static">
                                <span style="white-space: pre-wrap;">{{ $meal -> restrictions }}</span></p>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Special Instructions</label>
                            <p class="form-control-static">
                                <span style="white-space: pre-wrap;">{{ $meal -> special_instructions }}</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form id="mt-form" action="{{ $url }}" method="post" class="form-horizontal volunteer-form"
                              autocomplete="off">
                            @csrf
                            <input type="hidden" name="event_id" value="{{ $event -> id ?? '' }}"/>
                            <div class="alert alert-info">Please enter a meal description and any notes.</div>
                            <div id="status" class="alert hidden"></div>
                            <div class="form-group">
                                <label for="ItemDate" class="col-sm-3 control-label">Date</label>
                                <div class="col-sm-9">
                                    <p id="ItemDate" name="ItemDate"
                                       class="form-control-static">{{ $slot -> dateslots -> format ( "M d, Y" ) }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="itemVolunteer" class="col-sm-3 control-label">* Volunteer</label>
                                <div class="col-sm-9">
                                    @if( $volunteer == null )
                                        @if( Auth::id() ==  $meal -> user_id )
                                            <select id="itemVolunteer" name="assignedUser[id]" class="form-control">
                                                <option value="" selected="">Me ({{ Auth::user () -> full_name }})
                                                </option>
                                                <option value="-1">Add someone new</option>
                                            </select>
                                            <small>As the organizer or recipient, you can book this date for someone
                                                   else</small>
                                        @else
                                            <p id="ItemVolunteer"
                                               class="form-control-static">{{ Auth::user () -> full_name }}</p>
                                        @endif

                                    @else
                                        <p id="ItemVolunteer"
                                           class="form-control-static">{{ $volunteer -> assignedUser }}</p>
                                    @endif
                                </div>
                            </div>
                            <div id="volunteer-info" class="collapse" aria-expanded="false">
                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">* First name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="first_name"
                                               name="assignedUser[first_name]" placeholder="First name"
                                               maxlength="255"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-sm-3 control-label">* Last name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="last_name"
                                               name="assignedUser[last_name]" placeholder="Last name" maxlength="255"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label">* Email address</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email"
                                               name="assignedUser[email]" placeholder="Email" maxlength="100"/>
                                    </div>
                                </div>
                            </div>

                            @if($event == null)
                                <div class="form-group">
                                    <label for="itemDescription" class="col-sm-3 control-label">* Meal</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="itemDescription" name="description"
                                               value="{{ $volunteer -> body ?? '' }}" maxlength="200" autofocus
                                               required/>
                                    </div>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="ItemNeedType" class="col-sm-3 control-label">Need Type</label>
                                    <div class="col-sm-9">
                                        <p id="ItemNeedType" class="form-control-static">{{ $event -> category }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ItemNeed" class="col-sm-3 control-label">Need</label>
                                    <div class="col-sm-9">
                                        <p id="ItemNeed" class="form-control-static">{{ $event -> needed }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="itemDescription" class="col-sm-3 control-label">* Description</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="itemDescription" name="description"
                                               value="{{ $volunteer -> body ?? '' }}" maxlength="200" autofocus
                                               required/>
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="itemNotes" class="col-sm-3 control-label">Notes</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="itemNotes" name="notes" maxlength="200"
                                           placeholder="Optional" value="{{ $volunteer -> note ?? '' }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ItemReminder" class="col-sm-3 control-label">Reminder Email</label>
                                <div class="col-sm-9">
                                    <div class="checkbox">
                                        <label>
                                            @if( $volunteer != null && $volunteer -> one_day_reminder == 0 )
                                                <input type="checkbox" id="OneDayReminder" name="OneDayReminder"/>
                                                One day before
                                            @else
                                                <input type="checkbox" id="OneDayReminder" name="OneDayReminder"
                                                       checked/> One day before
                                            @endif
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            @if( $volunteer != null && $volunteer -> one_week_reminder == 0 )
                                                <input type="checkbox" id="OneWeekReminder" name="OneWeekReminder"/>
                                                One week before
                                            @else
                                                <input type="checkbox" id="OneWeekReminder" name="OneWeekReminder"
                                                       checked/> One week before
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" id="btnSubmit"
                                        class="btn btn-lg btn-success btn-responsive-block pull-right">
                                    <i class="fa fa-check"></i>&nbsp;
                                    @if( $volunteer == null ) Volunteer @else Save Changes @endif
                                </button>
                                @if( $volunteer != null )
                                    @if( $volunteer -> dateSlot -> meal -> user_id == Auth::id () )
                                        <a class="btn btn-lg btn-default btn-delete-volunteer btn-responsive-block pull-right"
                                           href="javascript:;">
                                            <i class="fa fa-trash"></i>&nbsp;Delete
                                        </a>
                                    @endif
                                    <a class="btn btn-lg btn-default btn-remove-volunteer btn-responsive-block pull-right"
                                       href="javascript:;">
                                        <i class="fa fa-close"></i>&nbsp;Cancel Booking
                                    </a>
                                @endif
                                <a class="btn btn-lg btn-default btn-responsive-block pull-right"
                                   href="{{ url ( "trains/{$id}" ) }}">
                                    <i class="fa fa-caret-left"></i>&nbsp;Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pg-volunteer-remove-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post">
                    @csrf
                    <input type="hidden" name="event_id" value="{{ $event -> id ?? '' }}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirm</h4>
                    </div>
                    <div class="modal-body">
                        <br>
                        <p class="heading"><strong></strong></p>
                        <br>
                        <p>
                            <strong>{{ $slot -> dateslots -> format ( "M d, Y" ) }}</strong>
                            &nbsp; {{ $volunteer -> body ?? '' }}
                        </p>
                        <br>
                        <p class="desc">
                            The recipient will be notified of the cancellation. To include a personal message, enter it
                            below.
                        </p>
                        <div class="form-group">
                            <textarea class="form-control" name="removal_message" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger"></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-app-layout>