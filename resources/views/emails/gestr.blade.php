<div style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;height:100%;margin:0;line-height:1.4;background-color:#f2f4f6;color:#74787e">
    <table style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:100%;margin:0;padding:0;background-color:#f2f4f6"
           width="100%" cellpadding="0" cellspacing="0">
        <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
        <tr>
            <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box" align="center">
                <table style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:100%;margin:0;padding:0"
                       width="100%" cellpadding="0" cellspacing="0">

                    <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                    <tr>
                        <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;padding:25px 0;text-align:center">
                            <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#3869d4;max-width:400px;border:0"><img
                                        style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box"
                                        src="{{ asset ( "images/logo.gif" ) }}" width="220" height="80"
                                        class="CToWUd"></a>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:100%;margin:0;padding:0;border-top:1px solid #edeff2;border-bottom:1px solid #edeff2;background-color:#fff"
                            width="100%">
                            <table class="m_-3023616800246213347email-body_inner"
                                   style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:570px;margin:0 auto;padding:0"
                                   align="center" width="570" cellpadding="0" cellspacing="0">

                                <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                <tr>
                                    <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                            <strong style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                                Important! Please verify your Gestr page.</strong>
                                        </p>
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                            {{ $data['user'] -> full_name }} created a Gestr page for you!
                                        </p>

                                        <table style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:100%;margin:30px auto;padding:0;text-align:center"
                                               align="center" width="100%" cellpadding="0" cellspacing="0">
                                            <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                            <tr>
                                                <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box"
                                                    align="center">
                                                    <div style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                                        <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#fff;display:inline-block;width:200px;background-color:#dc4d2f;border-radius:3px;font-size:15px;line-height:45px;text-align:center;text-decoration:none"
                                                           href="{{ url ("trains/9wro0l/recipient/ed1e4297-6951-4968-8c84-9cb2a488306a") }}"
                                                           target="_blank">Verify Me</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                            <strong style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                                What is Gestr?</strong>
                                        </p>
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                            There are times in our lives when friends and family ask, "What can I do to
                                            help out?" The answer is usually to help them with a meal. When many friends
                                            provide support through a meal, Gestr keeps everyone organized.</p>
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                            Gestr.com is a free meal calendar tool that makes planning meals among a
                                            wide group easy and less stressful.</p>
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                            <strong style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                                Quick Tip</strong>
                                        </p>
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                            NOTE: This email contains a special, keyed code just for you. Please do NOT
                                            forward this email, or share it with others.</p>

                                        <table style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:25px;padding-top:25px;border-top:1px solid #edeff2">
                                            <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                            <tr>
                                                <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:12px;line-height:1.5em;text-align:left">
                                                        If you’re having trouble clicking the Open button above, copy
                                                        and paste the URL below into your web browser.</p>
                                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:12px;line-height:1.5em;text-align:left">
                                                        <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#3869d4"
                                                           href="{{ url ( "/trains/{$data['id']}" ) }}" target="_blank">
                                                            {{ url ( "/trains/{$data['id']}" ) }}
                                                        </a>
                                                    </p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                            <table class="m_-3023616800246213347email-footer"
                                   style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:570px;margin:0 auto;padding:0;text-align:center"
                                   align="center" width="570" cellpadding="0" cellspacing="0">
                                <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                <tr>
                                    <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#aeaeae;font-size:12px;line-height:1.5em;text-align:center">
                                            Please add <a href="mailto:mail@gestr.com"
                                                          target="_blank">mail@gestr.com</a>
                                            to your address book to ensure our emails reach your inbox! If you'd like to
                                            update your notification preferences related to this Gestr, simply <a
                                                    style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#3869d4"
                                                    href="{{ url ( "/trains/{$data['id']}/participants//prefs/" ) }}"
                                                    target="_blank">click here</a>.
                                        </p>
                                        <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#aeaeae;font-size:12px;line-height:1.5em;text-align:center">
                                            © Gestr</p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>