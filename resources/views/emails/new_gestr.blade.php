<table style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:100%;margin:0;padding:0;background-color:#f2f4f6"
       width="100%" cellpadding="0" cellspacing="0">
    <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
    <tr>
        <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box" align="center">
            <table style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:100%;margin:0;padding:0"
                   width="100%" cellpadding="0" cellspacing="0">

                <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                <tr>
                    <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;padding:25px 0;text-align:center">
                        <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#3869d4;max-width:400px;border:0">
                            <img style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box"
                                 src="{{ asset ( "images/logo.gif" ) }}" width="220" height="80"
                                 class="CToWUd"></a>
                    </td>
                </tr>

                <tr>
                    <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:100%;margin:0;padding:0;border-top:1px solid #edeff2;border-bottom:1px solid #edeff2;background-color:#fff"
                        width="100%">
                        <table class="m_-3023616800246213347email-body_inner"
                               style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;width:570px;margin:0 auto;padding:0"
                               align="center" width="570" cellpadding="0" cellspacing="0">

                            <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                            <tr>
                                <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                                    <h1 style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#2f3133;font-size:19px;font-weight:bold;text-align:left">
                                        Hi {{ $data['user']->first_name }} {{ $data['user']->last_name }},</h1>
                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                        Thank you for creating a Gestr page for {{ $data['meal']->recipient_name }}. The
                                        unique link associated with this Gestr page is:
                                        <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#3869d4"
                                           href="{{ url ("/trains{$data['meal']->unique_id}") }}"
                                           target="_blank">{{ url ("/trains{$data['meal']->unique_id}") }}</a>
                                    </p>
                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                        <strong style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                            Tip #1: Invite Others</strong>
                                    </p>
                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                        Each volunteer must click or enter the above unique web address. You can provide
                                        it to them by:</p>
                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                        1.) Pasting the link above into an email<br>
                                        2.) Sharing the link to Facebook or Twitter<br>
                                        3.) Clicking the orange "Invite" button on the Meal Train page.</p>
                                    <br>
                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                        <strong style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                            Tip #2: Activate Donations</strong>
                                    </p>
                                    <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                        Provide an option for those volunteers that do not live close by or do not cook.
                                        Allowing financial donations is a great way to increase participation and
                                        support (Available only in USA).
                                        <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#3869d4"
                                           href="{{ url ("users/sign_in/?ReturnUrl=%2ftrains%2f9wro0l%2fdonate%2fcreate%2f") }}"
                                           target="_blank">Click here to learn more</a>.
                                    </p>

                                    <table style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:25px;padding-top:25px;border-top:1px solid #edeff2">
                                        <tbody style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                        <tr>
                                            <td style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box">
                                                <p style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;margin-top:0;color:#74787e;font-size:16px;line-height:1.5em;text-align:left">
                                                    Organized an event in the past? Reuse the participant list.
                                                    <a style="font-family:Arial,'Helvetica Neue',Helvetica,sans-serif;box-sizing:border-box;color:#3869d4"
                                                       href="{{ url ( "/help/faqs/#invite" ) }}"
                                                       target="_blank">Click here to learn more</a>.
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>