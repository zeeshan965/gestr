@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.tagit.css' ) }}"/>
    <style>
        ul.checklist li:before {
            content: "\f00c";
            font-family: "FontAwesome";
            display: inline-block;
            width: 1.2em;
            margin-left: -1.2em;
        }

        ul.checklist li {
            padding-left: 1.2em;
        }

        .alert > p, .alert > ul {
            margin-bottom: 0;
        }

        .halfpad-bottom {
            padding-bottom: 18px;
        }

        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .btn-fb:hover, .btn-fb:focus, .btn-fb:active, .btn-fb.active {
            color: #fff;
            background-color: #3a5795;
            border-color: #274E9F;
        }

        .btn-fb {
            color: #fff;
            background-color: #3a5795;
            border-color: #274E9F;
        }
    </style>
@endsection

@section( 'extra_js' )
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __('Message Confirmation') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="container-fluid">
        <div class="col-sm-offset-2 col-sm-8">
            <h1>Confrmed</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-success">
                        <i class="fa fa-check"></i>&nbsp;Your message have been sent.
                    </div>
                    <div class="text-center">
                        <a class="btn btn-default btn-responsive-block" href="{{ url ( "trains/{$id}" ) }}">Return to Gestr Page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
