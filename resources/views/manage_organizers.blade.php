@section('extra_css')
    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <style type="text/css">
        a:focus {
            outline: none;
        }

        .required_star {
            color: red;
        }

        .disabled {
            pointer-events: none;
        }

        input {
            height: 50px !important;
        }

        .activeSM {
            background-color: white !important; /* #38283c !important;*/
        }

        .container {
            margin-top: 15px;
        }
    </style>
@endsection

@section( 'extra_js' )
    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/api.js' ) }}"></script>
    <script type="text/javascript">
        const formData = new FormData ();
        let train_url = "{{ $url }}";
        let l = $ ( '.ladda-button-demo' ).ladda ();
        $ ( document ).ready ( function () {
            $ ( "#recepient_form" ).validate ( {
                rules : {
                    EventName : {
                        required : true,
                    },
                    EventEmail : {
                        required : true,
                    },
                    EventAddr1 : {
                        required : true,
                    },
                    EventCity : {
                        required : true,
                    },
                    EventState : {
                        required : true,
                    },
                    EventZip : {
                        required : true,
                    },
                    EventPhone : {
                        required : true,
                    },
                    EventNumAdults : {
                        required : true,
                        number : true,
                    },
                    EventNumKids : {
                        required : true,
                        number : true,
                    },
                    EventTime : {
                        required : true,
                    },
                    EventInstructions : {
                        required : true,
                    },
                    EventLikes : {
                        required : true,
                    },
                    EventDislikes : {
                        required : true,
                    },
                    EventRestrictions : {
                        required : true,
                    }
                },
                submitHandler : function ( form, event ) {
                    console.log ( form, event )
                    event.preventDefault ();
                    l.ladda ( 'start' );
                    let EventName = $ ( '#EventName' ).val ();
                    let EventEmail = $ ( '#EventEmail' ).val ();
                    let EventAddr1 = $ ( '#EventAddr1' ).val ();
                    let EventCity = $ ( '#EventCity' ).val ();
                    let EventState = $ ( '#EventState' ).val ();
                    let EventZip = $ ( '#EventZip' ).val ();
                    let EventPhone = $ ( '#EventPhone' ).val ();
                    let EventNumAdults = $ ( '#EventNumAdults' ).val ();
                    let EventNumKids = $ ( '#EventNumKids' ).val ();
                    let EventTime = $ ( '#EventTime' ).val ();
                    let EventInstructions = $ ( '#EventInstructions' ).val ();
                    let EventLikes = $ ( '#EventLikes' ).val ();
                    let EventDislikes = $ ( '#EventDislikes' ).val ();
                    let EventRestrictions = $ ( '#EventRestrictions' ).val ();
                    let story = $ ( '#about' ).text ();

                    formData.append ( "recipient_name", EventName );
                    formData.append ( "recipient_email", EventEmail );
                    formData.append ( "recipient_address", EventAddr1 );
                    formData.append ( "recipient_city", EventCity );
                    formData.append ( "recipient_state", EventState );
                    formData.append ( "recipient_postalcode", EventZip );
                    formData.append ( "recipient_phone", EventPhone );
                    formData.append ( "adults_cook_for", EventNumAdults );
                    formData.append ( "kids_cook_for", EventNumKids );
                    formData.append ( "delivery_time", EventTime );
                    formData.append ( "special_instructions", EventInstructions );
                    formData.append ( "fave_meals_rest", EventLikes );
                    formData.append ( "least_fave_meals", EventDislikes );
                    formData.append ( "restrictions", EventRestrictions );
                    formData.append ( "body", story );
                    formData.append ( "_token", $ ( 'meta[name="csrf-token"]' ).attr ( 'content' ) );
                    api_call ( 'update', 'post', formData ).then ( ( response ) => {
                        console.log ( response );
                        l.ladda ( 'stop' );
                        if ( typeof ( response.status ) !== 'undefined' && response.status == 'success' ) {
                            successMessage ( response.message, train_url )
                        } else {
                            if ( response.status == 'error' ) {
                                errorMessage ( response );
                            } else {
                                showErrors ( response );
                            }
                        }
                    } ).catch ( err => {
                        l.ladda ( 'stop' );
                        console.log ( err )
                    } );
                    return false;
                }
            } );
        } );

    </script>
@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('Edit Gestr') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                </div>
            </div>
        </div>
    </x-slot>

    <!-- CONTENT -->
    <!-- MultiStep Form -->
    <section class="item content">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body allForms">

                </div>
            </div>
        </div>
    </section>

    <!-- Successfully Registered Employee -->
    <button id="swal_btn" type="button" style="display:none"></button>
    <!-- Employee not  Register -->
    <button id="swal_btn2" type="button" style="display:none"></button>

</x-app-layout>