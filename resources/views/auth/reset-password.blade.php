@section('extra_css')
    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <style type="text/css">
        .text-red-600 {
            color: red;
            list-style: none;
        }

        .font-medium {
            font-weight: 700;
        }

        .text-green-600 {
            color: #fff;
            background-color: #5cb85c;
            border-color: #4cae4c;
            width: 100%;
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        form {
            border: 3px solid #f1f1f1;
        }

        input[type=email], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            box-sizing: border-box;
            height: 55px;
        }

        #signin_btn {
            background-color: #04AA6D;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        #signin_btn:hover {
            opacity: 0.8;
        }
    </style>
@endsection

@section( 'extra_js' )
    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>

    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script>
        ( function () {
            $ ( "#resetPassword" ).validate ( {
                rules : {
                    email : {
                        required : true,
                        email : true
                    }
                },
                submitHandler : function ( form ) {
                    console.log ( form )
                    form.submit ();
                }
            } );
        } ) ();
    </script>
@endsection

<x-guest-layout>
    <x-slot name="page_title">{{ __('Forgot Password') }}</x-slot>
    <x-slot name="headerContent"></x-slot>
    <x-auth-card>
        <x-slot name="authLabel">Reset Password</x-slot>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <form method="POST" action="{{ route('password.update') }}" id="resetPassword" style="padding: 10px;">
                    @csrf
                    <input type="hidden" name="token" value="{{ $request->route('token') }}">

                    <!-- Email Address -->
                    <div class="form-group">
                        <x-label for="email" :value="__('Email')"/>
                        <x-input id="email" class="form-control" type="email" name="email"
                                 :value="old('email', $request->email)" readonly/>
                    </div>
                    <!-- Password -->
                    <div class="form-group">
                        <x-label for="password" :value="__('Password')"/>
                        <x-input id="password" class="form-control" type="password"
                                 placeholder="Enter password" name="password" required/>
                    </div>
                    <!-- Confirm Password -->
                    <div class="form-group">
                        <x-label for="password_confirmation" :value="__('Confirm Password')"/>
                        <x-input id="password_confirmation" class="form-control"
                                 type="password" placeholder="Repeat password" name="password_confirmation" required/>
                    </div>
                    <div class="form-group">
                        <button id="signin_btn" class="ladda-button ladda-button-demo" type="submit"
                                data-style="zoom-in">
                            <span class="ladda-label">Reset Password</span><span class="ladda-spinner"></span>
                        </button>
                    </div>
                    <div class="form-group">
                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors"/>
                    </div>
                </form>
            </div>
        </div>
    </x-auth-card>
</x-guest-layout>
