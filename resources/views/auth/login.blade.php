@section('extra_css')
    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <style type="text/css">
        .text-red-600 {
            color: red;
            list-style: none;
        }

        .font-medium {
            font-weight: 700;
        }

        .text-green-600 {
            color: #fff;
            background-color: #5cb85c;
            border-color: #4cae4c;
            width: 100%;
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        form {
            border: 3px solid #f1f1f1;
        }

        input[type=email], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            box-sizing: border-box;
            height: 55px;
        }

        #signin_btn {
            background-color: #04AA6D;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        #signin_btn:hover {
            opacity: 0.8;
        }
    </style>
@endsection

@section( 'extra_js' )
    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>

    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/api.js' ) }}"></script>
    <script src="{{ asset ( 'js/auth.js' ) }}"></script>

@endsection

<x-guest-layout>
    <x-slot name="page_title">{{ __('Login') }}</x-slot>
    <x-slot name="headerContent"></x-slot>
    <x-auth-card>
        <x-slot name="authLabel">Sign In</x-slot>

        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div style="background-color:#f1f1f1; padding: 10px">
                    <span>New to GESTR? <a href="{{ route ( 'register' ) }}"><b>Sign Up</b></a></span>
                </div>

                <form method="POST" action="{{ route('login') }}" id="signin_form" autocomplete="false"
                      style="padding: 10px">

                    <div class="form-group">
                        <!-- Session Status -->
                        <x-auth-session-status class="mb-4" :status="session('status')"/>
                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors"/>
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token () }}"/>

                    <!-- Email Address -->
                    <div class="form-group">
                        <x-label for="email" :value="__('Email')"/>
                        <x-input type="email" class="form-control" id="email" placeholder="Enter email" name="email"
                                 :value="old('email')" required autofocus/>
                    </div>
                    <!-- Password -->
                    <div class="form-group">
                        <x-label for="password" :value="__('Password')"/>
                        <x-input type="password" class="form-control" id="password" placeholder="Enter password"
                                 name="password" required autocomplete="current-password"/>
                    </div>
                    <div class="flex items-center justify-end mt-4">
                        @if (Route::has('password.request'))
                            <a class="underline text-sm text-gray-600 hover:text-gray-900"
                               href="{{ route('password.request') }}">
                                {{ __('Forgot your password?') }}
                            </a>
                        @endif
                        <div class="form-group">
                            <button id="signin_btn" class="ladda-button ladda-button-demo" type="submit"
                                    data-style="zoom-in">
                                <span class="ladda-label">LOGIN</span><span class="ladda-spinner"></span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </x-auth-card>
</x-guest-layout>
