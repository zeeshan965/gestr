@section('extra_css')
    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <style type="text/css">
        form {
            border: 3px solid #f1f1f1;
        }

        fieldset {
            display: none;
        }

        input[type=text], input[type=email], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
            height: 55px;
        }

        #register_btn {
            background-color: #04AA6D;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
        }

        #register_btn:hover {
            opacity: 0.8;
        }
    </style>
@endsection

@section( 'extra_js' )
    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/api.js' ) }}"></script>
    <script src="{{ asset ( 'js/auth.js' ) }}"></script>

@endsection

<x-guest-layout>
    <x-slot name="page_title">{{ __('Register') }}</x-slot>
    <x-slot name="headerContent"></x-slot>
    <x-auth-card>
        <x-slot name="authLabel">Sign Up</x-slot>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div style="background-color:#f1f1f1; padding: 10px">
                    <span>Already have an Account? <a href="{{ route ( 'login' ) }}"><b>Sign In</b></a></span>
                </div>
                <!-- Validation Errors -->
                <x-auth-validation-errors class="mb-4" :errors="$errors"/>
                <form method="POST" action="{{ route('register') }}" id="signup_form" autocomplete="false"
                      style="padding: 10px">
                    <input type="hidden" name="_token" value="{{ csrf_token () }}"/>

                    <!-- First Name -->
                    <div class="form-group">
                        <x-label for="first_name" :value="__('First Name')"/>
                        <x-input type="text" class="form-control" id="first_name" placeholder="Enter First Name"
                                 name="first_name"
                                 required autofocus/>
                    </div>
                    <div class="form-group">
                        <x-label for="last_name" :value="__('Last Name')"/>
                        <x-input type="text" class="form-control" id="last_name" placeholder="Enter Last Name"
                                 name="last_name"
                                 required autofocus/>
                    </div>
                    <!-- Email Address -->
                    <div class="form-group">
                        <x-label for="email" :value="__('Email')"/>
                        <x-input type="email" class="form-control" id="email" placeholder="Enter email" name="email"
                                 required autofocus/>
                    </div>
                    <!-- Password -->
                    <div class="form-group">
                        <x-label for="password" :value="__('Password')"/>
                        <x-input type="password" class="form-control" id="password" placeholder="Enter password"
                                 name="password" required autocomplete="new-password"/>
                    </div>

                    <div class="form-group">
                        <x-label for="password_confirmation" :value="__('Confirm Password')"/>
                        <x-input id="password_confirmation" class="form-control" placeholder="Enter confirm password"
                                 type="password" name="password_confirmation" required/>
                    </div>

                    <div class="form-group">
                        <button id="register_btn" class="ladda-button ladda-button-demo" type="submit"
                                data-style="zoom-in">
                            <span class="ladda-label">REGISTER</span><span class="ladda-spinner"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </x-auth-card>
</x-guest-layout>
