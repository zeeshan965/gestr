@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.tagit.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/details.css' ) }}"/>
    <style>

        .round5 {
            border-radius: 5px;
            overflow: hidden;
        }

        ul.checklist li:before {
            content: "\f00c";
            font-family: "FontAwesome";
            display: inline-block;
            width: 1.2em;
            margin-left: -1.2em;
        }

        ul.checklist li {
            padding-left: 1.2em;
        }

        .alert > p, .alert > ul {
            margin-bottom: 0;
        }

        .halfpad-bottom {
            padding-bottom: 18px;
        }

        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section( 'extra_js' )
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __('Update Confirmation') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="container-fluid">
        <div class="col-sm-offset-2 col-sm-8">
            <h1>Confrmed</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="alert alert-success">
                        <i class="fa fa-check"></i>&nbsp;Your update have been saved.
                    </div>
                    <div class="halfpad-bottom">
                        <div class="text-center">
                            @if( $post -> attachment != null )
                                <img src="{{ url ( "/uploads/{$post -> attachment -> url}" ) }}"
                                     class="round5" alt="" title="">
                            @endif
                        </div>
                    </div>
                    <h4 class="text-center">Would you also like to share to Facebook and Twitter?</h4>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 margin-bottom-tiny">
                            <a class="btn btn-fb btn-block"><i class="fa fa-facebook"></i>&nbsp;Post to Facebook</a>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <a class="btn btn-tw btn-block"><i class="fa fa-twitter"></i>&nbsp;Post to Twitter</a>
                        </div>
                    </div>
                    <div class="text-center">
                        (This is a one-time post)
                    </div>
                    <div class="text-center">
                        <a class="btn btn-default btn-responsive-block" href="{{ $url."/updates" }}">
                            Return to the Calendar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
