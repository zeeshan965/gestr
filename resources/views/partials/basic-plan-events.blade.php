<div class="row pg-list-row">
    <div class="col-sm-3 col-xs-4 text-center">
        <h4 class="h-tight">{{ $slot -> dateslots -> format ( 'M d Y' ) }}</h4>
        {{ $slot -> dateslots -> format ( 'l' ) }}
    </div>
    @if( $slot -> volunteer == null )
        <div class="col-sm-5 col-xs-8 height-center">
            @if($old == true)
                <strong>{{ $str }}</strong>
            @else
                <a class="text-success"
                   href="{{ $url."/volunteer/".base64_encode ( $slot -> id ) }}">
                    <strong>{{ $str }}</strong>
                </a>
            @endif
        </div>
    @else
        <div class="col-sm-5 col-xs-8">
            @php
                $assignedUser = json_decode ( $slot -> volunteer -> assignedUser, true );
                if( isset( $assignedUser['id'] ) && $assignedUser['id'] != null ){
                    $name = $assignedUser['first_name'] .' '.$assignedUser['last_name'];
                }else{
                    $name = $slot -> volunteer -> user -> full_name;
                }
            @endphp
            <strong>
                <a href="{{ $url."/message" }}" class="text-mt">
                    <i class="fa fa-envelope"></i>&nbsp;{{ $name }}
                </a>
            </strong>
            <br>{{ $slot -> volunteer -> body }}<br>
            <small>({{ $slot -> volunteer -> note }})</small>
        </div>
    @endif
    @if($old == true)
        <div class="col-sm-4 col-xs-12"></div>
    @else
        <div class="col-sm-4 col-sm-offset-0 col-xs-offset-4 col-xs-8 height-center">
            @if( $slot -> volunteer == null )
                <a class="btn btn-success btn-block btn-xs hidden-xs"
                   href="{{ $url."/volunteer/".base64_encode ( $slot -> id ) }}">
                    Volunteer for this</a>
            @else
                @php
                    $slotID = base64_encode ( $slot -> id );
                    $ev = "/volunteer/".$slotID."/edit/".$slot -> volunteer -> unique_id;
                @endphp
                <a class="btn btn-primary btn-block btn-xs"
                   href="{{ $url.$ev }}">Make Changes</a>
            @endif
        </div>
    @endif
</div>