<div class="row pg-list-row">
    <div class="col-sm-12 col-xs-12">
        <h4 class="h-tight">
            {{ $slot -> dateslots -> format ( 'l' ) }}
            {{ $slot -> dateslots -> format ( 'd M, Y' ) }}
        </h4>
    </div>
    @foreach( $slot -> events as $event )
        <div class="event-wrapper @if( $old == false ) ev-margin @endif">
            <div class="col-sm-2 col-xs-3 text-center">
                <h5>{{ $event -> category }}</h5>
            </div>
            @if( $event -> volunteer == null )
                <div class="col-sm-6 col-xs-9 height-center">
                    @if( $old == true )
                        <strong>{{ $str }}</strong>
                    @else
                        <a class="text-success"
                           href="{{ $url."/volunteer/".base64_encode ( $slot -> id )."?event_id=".base64_encode ( $event -> id ) }}">
                            <strong>{{ $event -> needed }}</strong> ({{ $str }})
                        </a>
                    @endif
                </div>
            @else
                <div class="col-sm-6 col-xs-8">
                    <strong>
                        <a href="{{ $url."/message" }}" class="text-mt">
                            <i class="fa fa-envelope"></i>&nbsp;{{ $event -> volunteer -> assignedUser }}
                        </a>
                    </strong>
                    <br>{{ $event -> volunteer -> body }}<br>
                    <small>({{ $event -> volunteer -> note }})</small>
                </div>
            @endif
            @if($old == true)
                <div class="col-sm-4 col-xs-12"></div>
            @else
                <div class="col-sm-4 col-sm-offset-0 col-xs-offset-4 col-xs-8 height-center">
                    @if( $event -> volunteer == null )
                        <a class="btn btn-success btn-block btn-xs hidden-xs"
                           href="{{ $url."/volunteer/".base64_encode ( $slot -> id )."?event_id=".base64_encode ( $event -> id ) }}">
                            Volunteer for this</a>
                    @else
                        @php
                            $slotID = base64_encode ( $slot -> id );
                            $ev = "/volunteer/".$slotID."/edit/".$event -> volunteer -> unique_id."?event_id=".base64_encode ( $event -> id );
                        @endphp
                        <a class="btn btn-primary btn-block btn-xs"
                           href="{{ $url.$ev }}">Make Changes</a>
                    @endif
                </div>
            @endif
        </div>
    @endforeach
</div>