<x-app-layout>
    <x-slot name="page_title">{{ __('Shop') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    Shop
                </div>
            </div>
        </div>
    </x-slot>

    <!-- CONTENT -->
    <section class="item content">
        <div class="container toparea">
            <div class="underlined-title">
                <div class="editContent">
                    <h1 class="text-center latestitems">The Acts of GESTR</h1>
                    <center><small>(Our Products & Services)</small></center>
                </div>
                <div class="wow-hr type_short">
                    <span class="wow-hr-h">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>This is a short excerpt to generally describe what the item is about.</p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset( 'images/service1.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Meal Preparation</h1></a>
                            <span class="price"><span class="edd_price">$49.00</span></span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>PRODUCT</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service3.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Personalised Gifts</h1></a>
                            <span class="price"><span class="edd_price">$69.00</span></span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service2.png' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Cleaning</h1></a>
                            <span class="price"><span class="edd_price">$79.00</span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service1.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Meal Preparation</h1></a>
                            <span class="price"><span class="edd_price">$49.00</span></span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>PRODUCT</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service3.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Personalised Gifts</h1></a>
                            <span class="price"><span class="edd_price">$69.00</span></span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service2.png' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Cleaning</h1></a>
                            <span class="price"><span class="edd_price">$79.00</span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service1.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Meal Preparation</h1></a>
                            <span class="price"><span class="edd_price">$49.00</span></span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>PRODUCT</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service3.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Personalised Gifts</h1></a>
                            <span class="price"><span class="edd_price">$69.00</span></span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service2.png' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#"><h1>Cleaning</h1></a>
                            <span class="price"><span class="edd_price">$79.00</span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</x-app-layout>
