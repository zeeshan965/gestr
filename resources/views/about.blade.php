@section('extra_css')
    <link href="{{ asset ( 'css/aboutstyle.css' ) }}" rel="stylesheet"/>
@endsection

@section( 'extra_js' )
    <script src="{{ asset ( 'js/validate.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/about.js' ) }}"></script>
@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('About') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center"></div>
    </x-slot>

    <!-- intro header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <!-- start typer hello  -->
                        <div class="brand-heading">
                            <span class="txt-rotate" data-period="100"
                                  data-rotate='[ "Hello,", "Olá,", "Hallo,", "Salut,", "Ciao,", "Hola,", "Hej,", "Bonjour," ]'></span>
                        </div>
                        <!-- end typer hello -->
                        <p class="intro-text">My name is Marvin.
                            <br> I'm a Businessman.
                            <br>based in the beautiful city of <span class="city">London</span>, United Kingdom.</p>
                    </div>
                    <!-- end column wrapping -->
                </div>
                <!-- end row header -->
            </div>
            <!-- end container header -->
        </div>
        <!-- end intro body -->
    </header>
    <!-- end intro header -->

    <!-- start about me -->
    <section id="about-me">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 about-intro">
                    <div class="about-inner">
                        <div class="about-ct">
                            <img src="{{ asset ( 'images/about-intro.jpg' ) }}" alt="Marina Marques"
                                 class="img-responsive">
                            <div class="textbox-about hidden-xs">
                                <div class="btn button-social">
                                    <a alt="Facebook profile">
                                        <span class="fa fa-facebook" aria-hidden="true"></span>
                                    </a>
                                    <a alt="LinkedIn profile">
                                        <span class="fa fa-linkedin" aria-hidden="true"></span>
                                    </a>
                                    <a alt="Twitter profile"><span class="fa fa-twitter" aria-hidden="true"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 about-intro-text">
                    <h2>How To Make A Gestr</h2>
                    <p>You can make a <span>GESTR</span> by joining our
                        <span> CLUBS <span class="fa fa-beer" aria-hidden="true"></span>.</span>
                       We provide a number of services and packages.
                       Our Mission is to provide quality services such as:

                    <li>Providing user friendly technology to support the creation of community groups.</li>
                    <li>Support the local service economy to grow, sustain and innovate.</li>
                    <li>Source quality local services to connect with local communities.</li>
                    <li>Provide support to the vulnerable.</li>
                    <li>Bringing smiles across London.</li>
                    </p>
                    <a href="{{ route ( 'member' ) }}" alt="Marvin" target="_blank">
                        <div class="button-group">
                            <div class="text">GET STARTED</div>
                            <div class="icon-cv"><span class="fa fa-arrow-circle-right" aria-hidden="true"></span></div>
                        </div>
                    </a>
                </div>
            </div>
            <!-- end row about -->
        </div>
        <!-- end container about -->
        <!-- start count stats -->
        <section id="counter-stats">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 stats">
                        <span class="fa fa-building" aria-hidden="true"></span>
                        <div class="counter" data-count="3">0</div>
                        <h5>Clubs</h5>
                    </div>
                    <div class="col-lg-3 stats">
                        <span class="fa fa-taxi" aria-hidden="true"></span>
                        <div class="counter" data-count="200">0</div>
                        <h5>Services</h5>
                    </div>
                    <div class="col-lg-3 stats">
                        <span class="fa fa-gift" aria-hidden="true"></span>
                        <div class="counter" data-count="10">0</div>
                        <h5>Gift Packages</h5>
                    </div>
                    <div class="col-lg-3 stats">
                        <span class="fa fa-heart" aria-hidden="true"></span>
                        <div class="counter" data-count="10000000">0</div>
                        <h5>Kindness</h5>
                    </div>
                </div>
                <!-- end row counter stats -->
            </div>
            <!-- end container counter stats -->
        </section>
        <!-- end cont stats -->
    </section>
    <!-- end about me -->

    <!-- start work experience -->
    <section id="experience">
        <div class="container">
            <h2>ROADMAP</h2>
            <div class="experience-ct">
                <div class="row">
                    <div class="col-md-6 timeline-label">
                        <h4>Partnership</h4>
                        <p>This is some text regarding partnership. This is some text regarding partnership. This is
                           some text regarding partnership.</p>
                        <a alt="Partnerhip Details" target="_blank">
                            <div class="button-group-li">
                                <div class="text-timeline">View Details</div>
                                <div class="icon-li"><span class="fa fa-link" aria-hidden="true"></span></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6 timeline col-exp">
                        <div class="timeline-year">
                            <time datetime="2021">2021</time>
                            <div class="timeline-experience">
                                <span class="timeline-circle"></span>
                                <div class="timeline-experience-img"><img
                                            src="http://marinamarques.pt/img/bnp_paribas.png" alt="BNP Paribas"></div>
                                <div class="timeline-experience-info clear-after">
                                    <h5>IDEA</h5>
                                    <div class="timeline-role">This is Idea Phase.</div>
                                    <p>January 2021 - December 2021</p>
                                </div>
                                <!-- experience-info -->
                            </div>
                            <!-- experience -->
                            <time datetime="2015">2022</time>
                            <div class="timeline-experience">
                                <span class="timeline-circle"></span>
                                <div class="timeline-experience-img"><img
                                            src="http://marinamarques.pt/img/bnp_paribas.png" alt="BNP Paribas"></div>
                                <div class="timeline-experience-info clear-after">
                                    <h5>Development</h5>
                                    <div class="timeline-role">This is Development Phase.</div>
                                    <p>January 2022 - December 2022</p>
                                </div>
                                <!-- experience-info -->
                            </div>
                            <!-- experience -->
                        </div>
                        <!-- year -->
                        <div class="timeline-year timeline-year1">
                            <time datetime="2014">2023</time>
                            <div class="timeline-experience">
                                <span class="timeline-circle"></span>
                                <div class="timeline-experience-img"><img
                                            src="http://marinamarques.pt/img/paginas_amarelas.png"
                                            alt="Páginas Amarelas"></div>
                                <div class="timeline-experience-info clear-after">
                                    <h5>Crowdfunding</h5>
                                    <div class="timeline-role">This is Crowdfunding Phase.</div>
                                    <p>January 2023 - December 2023</p>
                                </div>
                                <!-- experience-info -->
                            </div>
                            <!-- experience -->
                            <time datetime="2013">2024</time>
                            <div class="timeline-experience">
                                <span class="timeline-circle"></span>
                                <div class="timeline-experience-img"><img
                                            src="http://marinamarques.pt/img/paginas_amarelas.png"
                                            alt="Páginas Amarelas"></div>
                                <div class="timeline-experience-info clear-after">
                                    <h5>Pre-Launch</h5>
                                    <div class="timeline-role">This is Pre-Launch Phase.</div>
                                    <p>January 2024 - December 2024</p>
                                </div>
                                <!-- experience-info -->
                            </div>
                            <!-- experience -->
                        </div>
                        <!-- year -->
                    </div>
                </div>
                <!-- end row of two columns -->
            </div>
            <!-- end experience container of two columns -->
        </div>
        <!-- end container experience -->
    </section>
    <!-- end work experience -->

    <!-- start services -->
    <section id="services">
        <div class="container">
            <h2>SERVICES</h2>
            <p>We offer a range of services to book either as a one of Gestr or an ongoing recurring Gestr , making
               organising and purchasing services quick and easy. </p>
            <div class="row">
                <div class="ct-services">
                    <div class="col-md-4 services-item">
                        <div class="rotate">
                            <span class="fa fa-briefcase" aria-hidden="true"></span>
                            <!-- <i class="fa fa-file-code-o fa-wrapper" aria-hidden="true"></i> -->
                        </div>
                        <h3>CLEANING</h3>
                        <p>Put some text here according to service type.</p>
                    </div>
                    <div class="col-md-4 services-item">
                        <div class="rotate">
                            <span class="fa fa-bicycle" aria-hidden="true"></span>
                        </div>
                        <h3>DIY</h3>
                        <p>Put some text here according to service type.</p>
                    </div>
                    <div class="col-md-4 services-item">
                        <div class="rotate">
                            <span class="fa fa-cart-plus" aria-hidden="true"></span>
                            <!-- <i class="fa fa-search fa-wrapper" aria-hidden="true"></i>-->
                        </div>
                        <h3>ERRANDS</h3>
                        <p>Put some text here according to service type.</p>
                    </div>
                </div>
                <!-- end row of services -->
            </div>
            <div class="row">
                <div class="ct-services">
                    <div class="col-md-4 services-item">
                        <div class="rotate">
                            <span class="fa fa-dollar" aria-hidden="true"></span>
                            <!-- <i class="fa fa-file-code-o fa-wrapper" aria-hidden="true"></i> -->
                        </div>
                        <h3>LAUNDRY</h3>
                        <p>Put some text here according to service type.</p>
                    </div>
                    <div class="col-md-4 services-item">
                        <div class="rotate">
                            <span class="fa fa-coffee" aria-hidden="true"></span>
                        </div>
                        <h3>CARE</h3>
                        <p>Put some text here according to service type.</p>
                    </div>
                    <div class="col-md-4 services-item">
                        <div class="rotate">
                            <span class="fa fa-child" aria-hidden="true"></span>
                            <!-- <i class="fa fa-search fa-wrapper" aria-hidden="true"></i>-->
                        </div>
                        <h3>HEALTH & BEAUTY</h3>
                        <p>Put some text here according to service type.</p>
                    </div>
                </div>
                <!-- end row of services -->
            </div>
            <!-- end row services section -->
        </div>
        <!-- end div services section -->
    </section>
    <!-- end services -->

    <!-- start testimionals -->
    <section id="testimionals">
        <div class="testimionals-bg">
            <div class="container testimionals-bg-opacity">
                <h2>TESTIMONIALS</h2>
                <div><span class="fa fa-quote-left quotes" aria-hidden="true"></span>
                </div>
                <div id="slider-testimionals" class="carousel slide" data-ride="carousel">
                    <!-- indicators dot nav -->
                    <ol class="carousel-indicators">
                        <li data-target="#slider-testimionals" data-slide-to="0" class="active"></li>
                        <li data-target="#slider-testimionals" data-slide-to="1"></li>
                        <li data-target="#slider-testimionals" data-slide-to="2"></li>
                        <li data-target="#slider-testimionals" data-slide-to="3"></li>
                    </ol>
                    <!-- wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <!-- start first testimional -->
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="carousel-caption">
                                        <p>Marina is always ready to accomplish the impossible in record time.</p>
                                        <p class="client">Emilien, Marketing department - BNP Paribas</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end first testimional -->
                        <!-- start second testimional -->
                        <div class="item">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="carousel-caption">
                                        <p>She's the fastest person in BNPP! Quick as lightning with robust
                                           solutions.</p>
                                        <p class="client">Rogier, Sales department - BNP Paribas</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end second testimional -->
                        <!-- start third testimional -->
                        <div class="item">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="carousel-caption">
                                        <p>She shows real commitment and demonstrates entrepreneurship skills as well as
                                           innovation abilities.</p>
                                        <p class="client">Gillian, Manager - BNP Paribas</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end third testimional -->
                        <!-- start fourth and last testimional -->
                        <div class="item">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="carousel-caption">
                                        <p>She doesn't give up until she finds a solution for what seems to be
                                           impossible.</p>
                                        <p class="client">Ana, Manager - Páginas Amarelas</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end fourth and last testimional -->
                    </div>
                    <!-- end carousel -->
                </div>
                <!-- end slider bootstrap -->
            </div>
        </div>
        <!-- end div container testimionals -->
    </section>
    <!-- end testimionals -->

</x-app-layout>