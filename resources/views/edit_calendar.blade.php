@section('extra_css')
    <link rel="stylesheet" href="{{ asset ( 'css/demo.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/theme2.css' ) }}"/>

    @if( $meal -> package_payment == 'Plus' )

        <link rel="stylesheet" href="{{ asset ( 'css/app-calendar.min.css' ) }}"/>
        <link rel="stylesheet" href="{{ asset ( 'css/full-calendar.min.css' ) }}"/>
        <style type="text/css">
            .cld-days .booked {
                border-radius: 12px;
            }

            #btnUpdateEvent {
                margin-left: 5px;
            }

            .span-wrap {
                width: 100%;
                height: 100px;
                float: left;
                display: none;
            }

            .cld-day {
                height: 150px;
                border-right: none;
            }

            .cld-main a {
                display: table;
                width: 100%;
                margin-bottom: 2px;
                font-size: 12px;
                word-break: break-word;
            }

            .cld-number.eventday {
                height: 100%;
            }
        </style>
    @endif
    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">

    <style type="text/css">
        .cld-days .passed {
            background-color: #F27474;
            color: #ffffff;
        }

        .cld-days .booked {
            background-color: #ddd;
            color: #000000;
        }

        a:focus {
            outline: none;
        }

        .required_star {
            color: red;
        }

        .disabled {
            pointer-events: none;
        }

        input {
            height: 50px !important;
        }

        .activeSM {
            background-color: white !important; /* #38283c !important;*/
        }

        .container {
            margin-top: 15px;
        }

        .tab-group {
            position: relative;
            vertical-align: middle;
            zoom: 1; /* Fix for IE7 */
            *display: inline; /* Fix for IE7 */
        }

        .tab-group > li {
            background-color: #eeeeee;
            border-radius: 4px;
            position: relative;
            float: left;
        }

        .tab-group > li > a {
            color: #337ab7;
            border-radius: 0;
            text-align: center;
        }

        .tab-group > li > a:hover {
            border-radius: 4px;
        }

        .tab-group li + li {
            margin-left: -1px;
        }

        .tab-group > li:not(:first-child):not(:last-child),
        .tab-group > li:not(:first-child):not(:last-child) > a:hover {
            border-radius: 0;
        }

        .tab-group > li:first-child,
        .tab-group > li:first-child > a:hover {
            margin-left: 0;
        }

        .tab-group > li:first-child:not(:last-child),
        .tab-group > li:first-child:not(:last-child) > a:hover {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .tab-group > li:last-child:not(:first-child),
        .tab-group > li:last-child:not(:first-child) > a:hover {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
    </style>
@endsection

@section( 'extra_js' )
    <script type="text/javascript" src="{{ asset ( 'js/jquery.serialize-object.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/moment.min.js' ) }}"></script>

    @if( $meal -> package_payment == 'Plus' )
        <script type="text/javascript" src="{{ asset ( 'js/plus-calendar.js' ) }}"></script>
        <script type="text/javascript" src="{{ asset ( 'js/fullcalendar.min.js' ) }}"></script>
        <script type="text/javascript" src="{{ asset ( 'js/app.calendar.js' ) }}"></script>
        <script type="text/javascript" src="{{ asset ( 'js/plus-plan.js' ) }}"></script>
    @else
        <script type="text/javascript" src="{{ asset ( 'js/caleandar.js' ) }}"></script>
    @endif

    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/api.js' ) }}"></script>

    <script type="text/javascript">
        var formData;
        var dateArr = [];
        var dateslotsArray = [];
        var calendar_form_status = 0;
        var signin;
        var organizer_id = localStorage.organizer_id;
        var tain_id = '';

        ( function () {
            let url = window.location.origin + '/trains/' + '{{ $id }}';
            api_call ( url + '/all_events', 'get' ).then ( ( response ) => {
                let basic_calendar_data = [];
                $.each ( response.data, function ( index, item ) {
                    @if( $meal -> package_payment == 'Plus' )
                        events[ item.Date.replaceAll ( '/', '-' ) ] = item;
                    @endif
                    dateArr.push ( item.Date.replaceAll ( '/', '-' ) )
                    item.Date = new Date ( item.Date );
                    //let obj = { 'Date' : new Date ( item.Date ), 'Title' : item.Title, 'Link' : item.Link };
                    basic_calendar_data.push ( item )
                } );
                var settings = {};
                var element = document.getElementById ( 'caleandar' );
                caleandar ( element, basic_calendar_data, settings );
            } ).catch ( err => {
                console.log ( err )
            } );
        } ) ();

        $ ( document ).ready ( function () {
            @if( $meal -> package_payment == 'Plus' )
            $ ( document ).on ( "click", ".cld-day, .cld-number, .span-wrap", function ( e ) {
                if ( e.target !== e.currentTarget ) return;
                var temp = $ ( this ).parent ().hasClass ( 'cld-number' ) ? $ ( this ).parent ().attr ( 'id' ) : this.id;
                let arr = temp.split ( '-' );
                if ( arr[ 2 ] < 10 ) arr[ 2 ] = '0' + arr[ 2 ];
                var result = compareDates ( arr.join ( '-' ) );
                if ( result ) {
                    var color = $ ( '#' + arr.join ( '-' ) ).css ( "background-color" );
                    _modal ( arr.join ( '-' ) );
                } else {
                    swal ( {
                        title : "Error!",
                        text : "Sorry! You can not select previous date.",
                        type : "error",
                        confirmButtonColor : "#DD6B55",
                    } );
                }
            } );
            @else
            $ ( document ).on ( "click", ".cld-number", function () {
                if ( $ ( this ).parent ().hasClass ( 'passed' ) === true ) return;
                if ( $ ( this ).parent ().hasClass ( 'booked' ) === true ) return;
                var temp = this.id;
                let arr = temp.split ( '-' );
                if ( arr[ 2 ] < 10 ) arr[ 2 ] = '0' + arr[ 2 ];
                var result = compareDates ( temp );
                if ( result ) {
                    var color = $ ( '#' + temp ).css ( "background-color" );
                    if ( color == "rgb(123, 200, 239)" ) {
                        const index = dateArr.indexOf ( arr.join ( '-' ) );
                        if ( index > - 1 ) dateArr.splice ( index, 1 );
                        $ ( '#' + temp ).css ( {
                            'background-color' : 'white', 'color' : '#777777'
                        } );
                    } else {
                        dateArr.push ( temp );
                        $ ( '#' + temp ).css ( {
                            'background-color' : '#7bc8ef', 'color' : 'white'
                        } );
                    }
                } else {
                    swal ( {
                        title : "Error!",
                        text : "Sorry! You can not select previous date.",
                        type : "error",
                        confirmButtonColor : "#DD6B55",
                    } );
                }
            } );
            @endif

        } );

        function buttonClicks ( id ) {
            @if( $meal -> package_payment == 'Plus' )
            //Preparing object
            for ( let j in events ) {
                let value = events[ j ];
                let obj = {};
                obj[ "dateslots" ] = j;
                obj[ "mt_pack_id" ] = 2;
                obj[ 'events' ] = [];
                for ( let l in value[ 'event_data' ] ) {
                    let event_data = value[ 'event_data' ][ l ];
                    obj[ 'events' ].push ( {
                        'category' : event_data.category,
                        'needed' : event_data.needed,
                    } );
                }
                dateslotsArray.push ( obj );
            }
            $ ( "[name='data']" ).val ( JSON.stringify ( dateslotsArray ) );
            @else
            $ ( "[name='data']" ).val ( JSON.stringify ( dateArr ) );
            @endif
            $ ( '#calendar_form' ).submit ();
            $ ( "html, body" ).animate ( { scrollTop : 0 }, "slow" );
        }

        function compareDates ( dateObj ) {
            dateObj += " 23:59:59";

            var g1 = new Date ();
            var g2 = new Date ( dateObj.replace ( /-/g, "/" ) );
            var result;

            if ( g1.getTime () < g2.getTime () ) {
                result = 1;
            } else if ( g1.getTime () > g2.getTime () ) {
                result = 0;
            }

            return result;
        }

    </script>

@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('Edit Calendar') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.0s">
                    Edit Calendar
                </div>
            </div>
        </div>
    </x-slot>

    <!-- CONTENT -->
    <!-- MultiStep Form -->
    <section class="item content">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-body allForms">
                    <!-- Calendar Dates Form -->
                    <form id="calendar_form" action="{{ route ( 'trains.update_calendar', [$id] ) }}" method="post"
                          class="form-horizontal" autocomplete="off">
                        @csrf
                        <input type="hidden" name="data"/>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-calendar"></i>&nbsp;&nbsp;Click all the days when help is needed
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                @if( $meal -> package_payment == 'Plus' )
                                    <div class="halfpad-bottom">
                                        <button id="btnAddEvent" class="btn btn-primary" type="button">
                                            <i class="fa fa-plus"></i>&nbsp;Add to Calendar
                                        </button>
                                    </div>
                                @endif
                                <div id="caleandar"></div>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <div>
                                <button onclick="buttonClicks('Submit')" type="button" id="btnSubmit"
                                        class="ladda-button ladda-button-demo btn btn-lg btn-primary btn-responsive-block pull-right"
                                        data-loading-text="Saving..." data-style="zoom-in">
                                    <span class="ladda-label">Submit &nbsp;
                                        <i class="fa fa-caret-right"></i>
                                    </span>
                                    <span class="ladda-spinner"></span>
                                </button>
                                <a href="{{ url ( '/dashboard' ) }}" style="margin-right: 10px;"
                                   class="btn btn-lg btn-default btn-responsive-block pull-right">
                                    <i class="fa fa-caret-left"></i> &nbsp; Back
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="pg-create-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="createForm" class="form-horizontal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Add to Calendar</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info">
                            <strong><i class="fa fa-info-circle"></i>&nbsp;&nbsp;</strong>
                            Describe what is needed
                        </div>
                        <div class="form-group">
                            <label for="Category" class="col-sm-12">Category<span class="required_star">*</span></label>
                            <div class="col-sm-12">
                                <select class="form-control category-dropdown" id="category" name="category" required>
                                    <option value="" selected>Select Category</option>
                                    <option value="1">Meal</option>
                                    <option value="2">Groceries</option>
                                    <option value="3">Help</option>
                                    <option value="4">Visit</option>
                                    <option value="5">Ride</option>
                                    <option value="6">Child Care</option>
                                    <option value="7">Communication</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Needed" class="col-sm-12">Needed<span class="required_star">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="needed" name="needed" required/>
                                <small><span class="category-example">Example: Dinner</span></small>
                            </div>
                        </div>

                        <div class="alert alert-info">
                            <strong><i class="fa fa-info-circle"></i>&nbsp;&nbsp;</strong>
                            Select the date(s) when this is needed
                        </div>
                        <div class="row" style="padding-bottom:20px;">
                            <div class="col-sm-12">
                                <div class="pull-left">
                                    <a id="btnCalPrev" class="btn btn-default btn-sm" href="#">
                                        <i class="fa fa-caret-left"></i>&nbsp;Prev Month
                                    </a>
                                </div>
                                <div class="pull-right">
                                    <a id="btnCalNext" class="btn btn-default btn-sm" href="#">Next Month&nbsp;
                                        <i class="fa fa-caret-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="calendarCurrent" class="mt-calendar"></div>
                            </div>
                            <div class="col-sm-6">
                                <div id="calendarNext" class="mt-calendar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnCreateEvent" type="submit" data-loading-text="Saving..." class="btn btn-primary">
                            <i class="fa fa-check"></i>&nbsp;Save Changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pg-update-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="updateForm" class="form-horizontal" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Update Calendar</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="editDate" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-10">
                                <p id="editDate" class="form-control-static"></p>
                                <input type="hidden" name="editID" id="editId"/>
                                <input type="hidden" name="editIndex" id="editIndex"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editCategoryId" class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-10">
                                <select class="form-control category-dropdown" id="editCategory" name="editCategory"
                                        required>
                                    <option value="" selected>Select Category</option>
                                    <option value="1">Meal</option>
                                    <option value="2">Groceries</option>
                                    <option value="3">Help</option>
                                    <option value="4">Visit</option>
                                    <option value="5">Ride</option>
                                    <option value="6">Child Care</option>
                                    <option value="7">Communication</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editTitle" class="col-sm-2 control-label">Needed</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="editNeed" name="editNeed" value=""
                                       maxlength="255" required="">
                                <small><span class="category-example">Example: Dinner</span></small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnUpdateEvent" type="submit"
                                class="btn btn-primary btn-responsive-block pull-right">
                            <i class="fa fa-check"></i>&nbsp;Save Changes
                        </button>

                        <button id="btnDeleteEvent" type="button"
                                class="btn  btn-default btn-responsive-block pull-right">
                            <i class="fa fa-trash"></i>&nbsp;Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Successfully Registered Employee -->
    <button id="swal_btn" type="button" style="display:none"></button>
    <!-- Employee not  Register -->
    <button id="swal_btn2" type="button" style="display:none"></button>

</x-app-layout>