@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.tagit.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/details.css' ) }}"/>
    <style>

        .round5 {
            border-radius: 5px;
            overflow: hidden;
        }

        ul.checklist li:before {
            content: "\f00c";
            font-family: "FontAwesome";
            display: inline-block;
            width: 1.2em;
            margin-left: -1.2em;
        }

        ul.checklist li {
            padding-left: 1.2em;
        }

        .alert > p, .alert > ul {
            margin-bottom: 0;
        }

        .halfpad-bottom {
            padding-bottom: 18px;
        }

        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }
    </style>
@endsection

@section( 'extra_js' )
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __('Update Confirmation') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="container-fluid">
        <div class="col-sm-offset-2 col-sm-8">
            <h1>Confrmed</h1>
            <h4>You have scheduled the following. This person will be notified.</h4>
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" autocomplete="off">
                        <div class="form-group">
                            <input type="hidden" id="id" name="id" value="{{ $volunteer -> unique_id }}">
                            <label class="col-sm-3 control-label">Date</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $volunteer -> dateSlot -> dateslots -> format ( 'M d, Y' ) }}</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ItemVolunteer" class="col-sm-3 control-label">Volunteer</label>
                            <div class="col-sm-9">
                                <p id="ItemVolunteer" name="ItemVolunteer" class="form-control-static">
                                    {{ $volunteer -> assignedUser }}
                                </p>
                            </div>
                        </div>
                        @if( $event == null )
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Meal</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">{{ $volunteer -> body }}</p>
                                </div>
                            </div>
                        @else
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Need Type</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">{{ $event -> category }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Need</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">{{ $event -> needed }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-9">
                                    <p class="form-control-static">{{ $volunteer -> body }}</p>
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Notes</label>
                            <div class="col-sm-9">
                                <p class="form-control-static">{{ $volunteer -> note }}</p>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4">
                            <a class="btn btn-default btn-sm  btn-block" target="_blank" rel="nofollow noreferrer"
                               href="{{ $google_invite }}">
                                <i class="fa fa-plus"></i>&nbsp;Add to Google calendar</a>
                            <a class="btn btn-default btn-sm  btn-block" target="_blank"
                               href="{{ $url }}">
                                <i class="fa fa-download"></i>&nbsp;Download iCalendar file</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text-center">
                    @if( $meal -> attachment != null )
                        <div class="halfpad-bottom">
                            <div class="text-center">
                                <img src="{{ url ( "uploads/{$meal -> attachment -> url}" ) }}" class="round5"
                                     alt="{{ $meal -> recipient_name }}"
                                     title="{{ $meal -> recipient_name }}"/>
                            </div>
                        </div>
                    @endif
                    <p>Sharing on Facebook helps increase support and participation.</p>
                    <a class="btn btn-lg btn-fb btn-responsive-block">
                        <i class="fa fa-facebook"></i>&nbsp;Post to Facebook</a>
                    <a class="btn btn-lg btn-default btn-responsive-block" href="{{ url ( "trains/{$id}" ) }}">
                        Return to the Calendar
                    </a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
