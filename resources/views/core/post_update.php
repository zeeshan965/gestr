<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="">
<title>GESTR - Event Page</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">

<!-- Sweet Alert -->
<link href="css/sweetalert.css" rel="stylesheet">

<!-- Ladda style -->
<link href="css/ladda-themeless.min.css" rel="stylesheet">

<style type="text/css">
    #signin_btn {
          background-color: #04AA6D;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
        }

        #signin_btn:hover {
          opacity: 0.8;
        }
</style>
</head>
<body>

<!-- HEADER =============================-->
<header class="margin-top-0">
	<div class="wrapper">
		<!-- Adding Nav Bar -->
		<?php include 'navbar.php';?>
	</div>
</header>

<div id="mt-body" style="margin-top: 120px">
    <div class="container-fluid">
        <div class="pg-header" style="background-color: #01467F;padding: 10px;color: white;">
            <h4 style="color: #F08B1D">Meal Train for</h4>
            <h2 id="recepient_name"></h2>
        </div>
        <h1>Update</h1>
        <div class="mt-buttonbar mt-buttonbar-default">
            <div class="mt-buttonbar-buttons">
                <a class="btn btn-default btn-back-confirm" href="eventpage.php"><i class="fa fa-caret-left"></i>&nbsp;Back</a>
                <!--<a class="btn btn-success btn-submit" data-loading-text="Saving..."><i class="fa fa-check"></i>&nbsp;Post update</a>-->
                <div class="alert hidden alert-status"></div>
            </div>
        </div>
        <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8">
                    <div class="alert alert-info"><p><strong>Your friends want to hear from you</strong></p><p>Use the Updates tab to share updates and photos as they occur.</p><p>Start your post by adding a catchy title below.</p></div>
                </div>
            </div>
            <form id="mt_form" class="form-horizontal" autocomplete="off">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Date Posted</label>
                    <div class="col-sm-8">
                        <p class="form-control-static">May 25, 2021</p>
                    </div>
                </div>
                <div class="form-group">
                    <label for="PostTitle" class="col-sm-2 control-label">Title</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="PostTitle" name="PostTitle" maxlength="255" required="" aria-required="true">
                    </div>
                </div>
                <div class="form-group">
                    <label for="PostText" class="col-sm-2 control-label">Text</label>
                    <div class="col-sm-8">
                        <textarea id="PostText" class="form-control" name="PostText" rows="15" required="" aria-required="true"></textarea>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label for="PostPhoto" class="col-sm-2 control-label">Photo</label>
                    <div class="col-sm-8">                        
                        <div>
                            <img id="file-upload-img" class="thumbnail thumbnail-sm" style="display:none;">
                            <div class="progress fileupload-progress"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;"></div></div>
                            <div id="file-upload-status" class="alert hidden"></div>
                            <div class="btn btn-primary btn-responsive-block fileinput-button"><i class="fa fa-plus"></i>&nbsp;Select new photo...<input id="fileupload" type="file" name="files[]" accept="image/*"></div>
                            <a class="btn btn-default btn-responsive-block btn-remove" style="display:none;" href="#" data-loading-text="Removing..."><i class="fa fa-trash"></i>&nbsp;Remove photo</a>
                        </div>
                        <input type="hidden" id="PostPhotoPath" name="Post[PhotoPath]">
                    </div>
                </div>-->
                <div class="form-group">
                    <label for="PostText" class="col-sm-2 control-label" style="visibility: hidden;">Text</label>
                    <div class="col-sm-8">
                        <button id="signin_btn" class="ladda-button ladda-button-demo" type="submit" data-style="zoom-in">
                            <span class="ladda-label">POST UPDATE</span><span class="ladda-spinner"></span>
                        </button>
                    </div>
                </div>  
            </form>
            
            <div class="row">
                <div class="col-sm-offset-2 col-sm-8">
                    <div class="alert alert-info">Note: An email will be sent to all participants notifying them of the update.</div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

<!-- Adding Call to Action + Footer -->
<?php include 'footer.php';?>

<!-- Successfully Registered Employee -->
<button id="swal_btn" type="button" style="display:none"></button>
<!-- Employee not  Register -->
<button id="swal_btn2" type="button" style="display:none"></button>

<!-- Load JS here for greater good =============================-->
<script src="js/jquery-.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/anim.js"></script>

<!-- Sweet alert -->
<script src="js/sweetalert.min.js"></script>

<!-- Jquery Validate -->
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>

<!-- Ladda -->
<script src="js/spin.min.js"></script>
<script src="js/ladda.min.js"></script>
<script src="js/ladda.jquery.min.js"></script>

<script type="text/javascript">
    $(document).ready(function()
    {
        var recipient_name = localStorage.recipient_name;

        $('#recepient_name').html(recipient_name);

        $("#mt_form").validate({
            rules: 
            {
                PostTitle: {
                    required: true
                },               
                PostText: {
                    required: true,
                },               
            },
            messages: 
            {
            },
            submitHandler: function(form) 
            {
                var l = $( '.ladda-button-demo' ).ladda();
                l.ladda( 'start' );

                var PostTitle = $('#PostTitle').val();
                var PostText = $('#PostText').val();

                var formData = new FormData();
                formData.append('mt_pack_id', localStorage.packageID);
                formData.append("title", PostTitle);
                formData.append("text", PostText);
                formData.append("posted_by", "abc@gmail.com");
                    
                $.ajax
                ({
                    type:'POST',
                    url:"https://yourinnercircles.com/MT_apis/public/api/AddPost",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        if( data.status == "success")
                        {                                
                            l.ladda('stop');
                            swal_btn.click();
                        }
                        else if( data.status == "failed" )
                        {
                            l.ladda('stop');
                            //errorMessage = data.error;
                            swal_btn2.click();
                        }
                    },
                    error: function(data) 
                    {
                        l.ladda('stop');
                        alert("error");
                    }  
                });       
                return false; // extra insurance preventing the default form action
            }
        });

        $('#swal_btn').click(function(){
            swal({
                title: "SUCCESS!",
                text: "POST UPDATED Successfully",
                type: "success",
                confirmButtonColor: "#566b8a",
                closeOnConfirm: false
            }, function () {
                window.location.assign("eventpage.php");
            });
        });

        $('#swal_btn2').click(function()
        {
            swal({
                title: "Unsuccessfull!",
                text: "Operation Unsuccessfull",
                type: "warning",
                confirmButtonColor: "#DD6B55",
            });
        });
    });
</script>

</body>
</html>