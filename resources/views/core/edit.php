<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="">
<title>GESTR - Edit Event Page</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">
</head>
<body>

<!-- HEADER =============================-->
<header class="margin-top-0">
	<div class="wrapper">
		<!-- Adding Nav Bar -->
		<?php include 'navbar.php';?>
	</div>
</header>

<div id="mt-body" style="margin-top: 120px">
    <div class="container-fluid">
        <div class="pg-header" style="background-color: #01467F;padding: 10px;color: white;">
            <h4 style="color: #F08B1D">Meal Train for</h4>
            <h2>Fritz Noel</h2>
        </div><br><br>
        <a class="btn btn-default" href="dashboard.php"><i class="fa fa-caret-left"></i>&nbsp;Back</a>
        <input type="hidden" id="EventId" name="EventId" value="ve6v38">
        <h2>I would like to:</h2>
        <div class="well" style="padding:19px 19px 5px 19px;">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4 text-center">
                    <a class="btn btn-primary btn-block" href="/trains/ve6v38/upgrade/plus/"><i class="fa fa-plus"></i>&nbsp;Upgrade to Meal Train Plus</a>
                    <a class="text-danger" href="/trains/ve6v38/upgrade/plus/">Learn more</a>
                </div>
            </div>
        </div>
        <div class="well" style="padding:19px 19px 5px 19px;">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4 text-center">
                    <a class="btn btn-primary btn-block" href="/trains/ve6v38/donate/create/"><i class="fa fa-dollar"></i>&nbsp;Activate Donations</a>
                    <a class="text-danger" href="/trains/ve6v38/donate/create/">Learn more</a>
                </div>
            </div>
        </div>

        <div class="well" style="padding:19px 19px 5px 19px;">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4 text-center">
                    <a class="btn btn-primary btn-block" href="/trains/ve6v38/upgrade/adfree/"><i class="fa fa-ban"></i>&nbsp;Remove Advertisements</a>
                    <a class="text-danger" href="/trains/ve6v38/upgrade/adfree/">Learn more</a>
                </div>
            </div>
        </div>
        <div class="well">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <a class="btn btn-primary btn-block" href="/trains/ve6v38/edit/info/"><i class="fa fa-map-marker"></i>&nbsp;Edit Details</a>
                </div>
            </div>
        </div>
        <div class="well">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <a class="btn btn-primary btn-block" href="/trains/ve6v38/calendar/"><i class="fa fa-calendar-o"></i>&nbsp;Edit Calendar</a>
                </div>
            </div>
        </div>
        <div class="well">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <a id="btn-photo-new" class="btn btn-primary btn-block" href="#"><i class="fa fa-photo"></i>&nbsp;Add/Edit Main Photo</a>
                </div>
            </div>
        </div>
        <div class="well">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <a id="btn-organizer-train" class="btn btn-primary btn-block" href="/trains/ve6v38/organizers/"><i class="fa fa-user"></i>&nbsp;Manage Organizers</a>
                </div>
            </div>
        </div>
        <div class="well">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <a id="btn-participants-train" class="btn btn-primary btn-block" href="#"><i class="fa fa-group"></i>&nbsp;Manage Participants</a>
                </div>
            </div>
        </div>
        <div class="well">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <a id="btn-delete-train" class="btn btn-danger btn-block" href="#"><i class="fa fa-trash"></i>&nbsp;Delete this Meal Train</a>
                </div>
            </div>
        </div>
        <div id="pg-modal-container"></div>
    </div>
</div>

<!-- Adding Call to Action + Footer -->
<?php include 'footer.php';?>

<!-- Load JS here for greater good =============================-->
<script src="js/jquery-.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/anim.js"></script>

</body>
</html>