<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="generator" content="">
	<title>GESTR - Login Page</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/formStyle.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">
	
	<!-- Sweet Alert -->
    <link href="css/sweetalert.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="css/ladda-themeless.min.css" rel="stylesheet">
    
</head>
<body>

<!-- HEADER =============================-->
<header class="item header margin-top-0">
	<div class="wrapper">
		<!-- Adding Nav Bar -->
		<?php include 'navbar.php';?>
	</div>
</header>
<!-- CONTENT =============================-->
<!-- MultiStep Form -->
<div class="container-fluid" id="grad1" style="margin-top: 100px;">
    <div class="row justify-content-center mt-0">
        <div class="col-11 col-sm-9 col-md-7 col-lg-6 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <h2><strong>Sign Up Your User Account</strong></h2>
                <p>Fill all form field to go to next step</p>
                <div class="row">
                    <div class="col-md-12 mx-0">
                        <form id="msform">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="account"><strong>Account</strong></li>
                                <li id="personal"><strong>Personal</strong></li>
                                <li id="payment"><strong>Payment</strong></li>
                                <li id="confirm"><strong>Finish</strong></li>
                            </ul> <!-- fieldsets -->
                            
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">Account Information</h2> 
                                    <input type="email" name="email" placeholder="Email Id" />
                                    <input type="text" name="uname" placeholder="UserName" />
                                     <input type="password" name="pwd" placeholder="Password" /> 
                                     <input type="password" name="cpwd" placeholder="Confirm Password" />
                                </div> <input type="button" name="next" class="next action-button" value="Next Step" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">Personal Information</h2> <input type="text" name="fname" placeholder="First Name" /> <input type="text" name="lname" placeholder="Last Name" /> <input type="text" name="phno" placeholder="Contact No." /> <input type="text" name="phno_2" placeholder="Alternate Contact No." />
                                </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" value="Next Step" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title">Payment Information</h2>
                                    <div class="radio-group">
                                        <div class='radio' data-value="credit"><img src="https://i.imgur.com/XzOzVHZ.jpg" width="200px" height="100px"></div>
                                        <div class='radio' data-value="paypal"><img src="https://i.imgur.com/jXjwZlj.jpg" width="200px" height="100px"></div> <br>
                                    </div> <label class="pay">Card Holder Name*</label> <input type="text" name="holdername" placeholder="" />
                                    <div class="row">
                                        <div class="col-9"> <label class="pay">Card Number*</label> <input type="text" name="cardno" placeholder="" /> </div>
                                        <div class="col-3"> <label class="pay">CVC*</label> <input type="password" name="cvcpwd" placeholder="***" /> </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3"> <label class="pay">Expiry Date*</label> </div>
                                        <div class="col-9"> <select class="list-dt" id="month" name="expmonth">
                                                <option selected>Month</option>
                                                <option>January</option>
                                                <option>February</option>
                                                <option>March</option>
                                                <option>April</option>
                                                <option>May</option>
                                                <option>June</option>
                                                <option>July</option>
                                                <option>August</option>
                                                <option>September</option>
                                                <option>October</option>
                                                <option>November</option>
                                                <option>December</option>
                                            </select> <select class="list-dt" id="year" name="expyear">
                                                <option selected>Year</option>
                                            </select> </div>
                                    </div>
                                </div> <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="make_payment" class="next action-button" value="Confirm" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <h2 class="fs-title text-center">Success !</h2> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-3"> <img src="https://img.icons8.com/color/96/000000/ok--v2.png" class="fit-image"> </div>
                                    </div> <br><br>
                                    <div class="row justify-content-center">
                                        <div class="col-7 text-center">
                                            <h5>You Have Successfully Signed Up</h5>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Adding Call to Action + Footer -->
<?php include 'footer.php';?>

<!-- Successfully Registered Employee -->
<button id="swal_btn" type="button" style="display:none"></button>
<!-- Employee not  Register -->
<button id="swal_btn2" type="button" style="display:none"></button>

<!-- SCRIPTS =============================-->
<script src="js/jquery-.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/anim.js"></script>
<script src="js/formJs.js"></script>

<!-- Sweet alert -->
<script src="js/sweetalert.min.js"></script>

<!-- Jquery Validate -->
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>

<!-- Ladda -->
<script src="js/spin.min.js"></script>
<script src="js/ladda.min.js"></script>
<script src="js/ladda.jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function()
	{
	    $("#signin_form").validate({
	        rules: 
	        {
	            email: {
	                required: true
	            },               
	            password: {
	                required: true,
	            },               
	        },
	        messages: 
	        {
	        },
	        submitHandler: function(form) 
	        {
	            var l = $( '.ladda-button-demo' ).ladda();
	            l.ladda( 'start' );

	            var email = $('#email').val();
	            var password = $('#password').val();

                var formData = new FormData();
                formData.append("email", email);
                formData.append("password", password);
                    
                $.ajax
                ({
                    type:'POST',
                    url:"https://dev.voxmenu.com/public/api/UserSignin",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        if( data.status == "success")
                        {
                            l.ladda('stop');
                            swal_btn.click();
                        }
                        else if( data.status == "failed" )
                        {
                            l.ladda('stop');
                            errorMessage = data.error;
                            swal_btn2.click();
                        }
                    },
                    error: function(data) 
                    {
                    	l.ladda('stop');
                        alert("error");
                    }  
                });       
	            return false; // extra insurance preventing the default form action
	        }
	    });

	    $('#swal_btn').click(function(){
	        swal({
	            title: "Employee added successfully!",
	            text: "",
	            type: "success",
	            confirmButtonColor: "#566b8a",
	            closeOnConfirm: false
	        }, function () {
	            window.location.assign("http://drycleanlocker.net/view_employees");
	        });
	    });

	    $('#swal_btn2').click(function()
	    {
	        swal({
	            title: "Operation Unsuccessfull!",
	            text: errorMessage,
	            type: "warning",
	            confirmButtonColor: "#DD6B55",
	        });
	    });
	});

</script>

</body>
</html>