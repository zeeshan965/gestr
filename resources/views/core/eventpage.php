<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="">
<title>GESTR - Event Page</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/details.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">
</head>
<body>

<!-- HEADER =============================-->
<header class="margin-top-0">
	<div class="wrapper">
		<!-- Adding Nav Bar -->
		<?php include 'navbar.php';?>
	</div>
</header>

<div id="mt-body" style="margin-top: 120px">
    <div class="container-fluid">
        <div class="pg-header" style="background-color: #01467F;padding: 10px;color: white;">
            <div class="row">
                <div class="col-sm-9">
                    <h4 style="color: #F08B1D">Meal Train for</h4>
                    <h2 id="recepient_name"></h2>
                    <input type="hidden" id="EventId" name="EventId" value="ve6v38">
                </div>
            
                <div class="col-sm-3 hidden-xs">
                    <a class="btn btn-default btn-block btn-sm" title="Post an update" href="post_update.php"><i class="fa fa-comments-o"></i>&nbsp;Post an update</a>
                    <a class="btn btn-default btn-block btn-sm" title="Make changes" href="edit.php"><i class="fa fa-pencil"></i>&nbsp;Make changes</a>
                </div>
            </div>
        </div><br><br>

        <div class="row">
            <div class="col-sm-3 col-md-4">
                <div class="btn-photo-container">
                    <div class="btn-admin-box btn-photo-box btn-block">
                        <a id="btn-photo-new" href="#" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Add a Photo</a>
                        <br><p>Adding a photo can lead<br>to greater participation</p>
                    </div>
                    <div id="pg-train-photo" class="text-center round5" style="display:none;">
                        <img>
                    </div>
                </div>

                <div class="visible-xs-block">                
                    <div class="nav-header-container" style="height: 0px;">
                        <ul class="nav nav-justified-sm nav-header affix" data-toggle="affix">
                            <li class="active">
                                <a>
                                    <i class="fa fa-calendar"></i>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <span class="visible-xs-inline"><br></span>Calendar
                                    <span class="visible-xs-inline"><br>&nbsp;</span>
                                </a>
                            </li>
                            <li class="">
                                <a>
                                    <i class="fa fa-comment-o"></i>
                                    <span class="hidden-xs">&nbsp;</span>
                                    <span class="visible-xs-inline"><br></span>Updates
                                    <span class="hidden-xs">&nbsp;</span>
                                    <span class="visible-xs-inline"><br></span>
                                    <span class="text-mt">New!</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="visible-xs-block text-center">
                            <div>
                                <a class="btn btn-mt"><i class="fa fa-envelope"></i>&nbsp;Invite</a>
                                <a class="btn btn-fb"><i class="fa fa-facebook"></i>&nbsp;Share</a>
                                <a class="btn btn-tw"><i class="fa fa-twitter"></i>&nbsp;Tweet</a>
                            </div>
                            <div><small><a data-toggle="modal" href="#pg-invitehelp-modal"><i class="fa fa-question-circle"></i>&nbsp;Invitation help?</a></small></div>
                        </div>
                        <strong>Organizer</strong><br>
                        <p>
                            <a id="organizer_name">
                            </a>
                        </p>
                        <strong>Recipient</strong><br>
                        <p>
                            <a id="recepient_name2">
                            </a>
                        </p>
                        <p class="visible-xs-block"><a class="btn-volunteer-info" href="#">View contact info, address, delivery time, etc.</a></p>
                        <p><a href="#" class="btn-participants">View/Email all participants&nbsp;<span class="badge">2</span></a></p>
                        <div class="hidden-xs">
                            <br>
                            <p>
                                <input id="url" name="url" type="text" class="form-control" value="https://mealtrain.com/ve6v38" onfocus="this.select();">
                            </p>
                            <a class="btn btn-mt btn-block"><i class="fa fa-envelope"></i>&nbsp;Invite</a>
                            <a class="btn btn-fb btn-block"><i class="fa fa-facebook"></i>&nbsp;Share</a>
                            <a class="btn btn-tw btn-block"><i class="fa fa-twitter"></i>&nbsp;Tweet</a>
                            <div><small><a data-toggle="modal" href="#pg-invitehelp-modal"><i class="fa fa-question-circle"></i>&nbsp;Invitation help?</a></small></div>
                        </div>
                    </div>
                </div>

                <div class="visible-xs-block">                
                    <div class="pg-train-story halfpad-bottom" style="display:none;">
                        <h4><strong>About this Meal Train page</strong></h4>
                        <p style="white-space: pre-wrap;"></p>
                    </div>
                    <div class="pg-train-story-add halfpad-bottom">
                        <div class="btn-admin-box pg-story-edit-box">
                            <p>Let everyone know why you are organizing this Meal Train page.</p>
                            <button class="btn btn-primary btn-story-add">
                                <i class="fa fa-pencil"></i>&nbsp;Tell the story
                            </button>
                        </div>
                    </div>
                </div>

                <div id="pg-ad-slot1" class="ad-responsive-lgrect">
                    <script type="text/javascript">
                        googletag.cmd.push(function () { googletag.display("pg-ad-slot1"); });
                    </script>
                    <div id="google_ads_iframe_/6192803/_mealtrain_rect_768up_0__container__" style="border: 0pt none; width: 160px; height: 0px;">
                    </div>
                </div>
            </div>

            <div class="col-sm-9 col-md-8">
                <div id="pg-ad-slot2" class="ad-responsive-leader">
                        <script type="text/javascript">
                            googletag.cmd.push(function () { googletag.display("pg-ad-slot2"); });
                        </script>
                    <div id="google_ads_iframe_/6192803/_mealtrain_leader_320up_0__container__" style="border: 0pt none; width: 468px; height: 0px;">
                    </div>
                </div>

                <div class="hidden-xs">                                
                    <div class="pg-train-story halfpad-bottom" style="display:none;">
                        <h4><strong>About this Meal Train page</strong></h4>
                        <p style="white-space: pre-wrap;"></p>
                    </div>
                    <div class="pg-train-story-add halfpad-bottom">
                        <div class="btn-admin-box pg-story-edit-box">
                            <p>Let everyone know why you are organizing this Meal Train page.</p>
                            <button class="btn btn-primary btn-story-add">
                                <i class="fa fa-pencil"></i>&nbsp;Tell the story
                            </button>
                        </div>
                    </div>
                </div>

                <div class="hidden-xs">                                
                    <div class="nav-header-container" style="height: 46px;">
                        <ul class="nav nav-justified-sm nav-header affix" data-toggle="affix">
                            <li class="active">
                                <a>
                                    <i class="fa fa-calendar"></i><span class="hidden-xs">&nbsp;</span><span class="visible-xs-inline"><br></span>Calendar
                                    <span class="visible-xs-inline"><br>&nbsp;</span>
                                </a>
                            </li>
                            <li class="">
                                <a>
                                    <i class="fa fa-comment-o"></i><span class="hidden-xs">&nbsp;</span><span class="visible-xs-inline"><br></span>Updates
                                    <span class="hidden-xs">&nbsp;</span><span class="visible-xs-inline"><br></span><span class="text-mt">New!</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="panel panel-default hidden-xs">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <strong>Location</strong><br>
                                <span id="recipient_address"></span><br>
                                <span id="recipient_city"></span>, 
                                <span id="recipient_state"></span> 
                                <span id="recipient_postalcode"></span>
                                <br>
                                <span id="recipient_phone"></span>
                                <br>
                            </div>
                            <div class="col-sm-4">
                                <strong>Number of People</strong><br>
                                <p>Adults:<span id="adults_cook_for"></span> Kids:<span id="kids_cook_for"></span></p>
                                <strong>Preferred delivery time</strong><br>
                                <span id="delivery_time"></span>
                            </div>
                            <div class="col-sm-4">
                                <strong>Special Instructions</strong><br>
                                <span id="special_instructions"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <h4><strong>Calendar</strong></h4>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="btn pull-right hidden-xs" title="Print" href="/trains/ve6v38/print/" target="_blank"><i class="fa fa-lg fa-print"></i></a>
                        <div style="">
                            <a class="btn btn-mt-yellow btn-volunteer-info" title="Review All Instructions" href="#"><i class="fa fa-info-circle"></i>&nbsp;Review All Instructions</a>
                        </div>
                        <ul class="tabs-slide hidden-xs">
                            <li><a class="btn-viewmode" data-viewmode="0" title="View as list" href="#pg-calendar-list" role="tab" data-toggle="tab"><i class="fa fa-bars"></i><span class="hidden-xs">&nbsp;List View</span></a></li>
                            <li class="active"><a class="btn-viewmode active" data-viewmode="1" title="View as calendar" href="#pg-calendar-month" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-calendar"></i><span class="hidden-xs">&nbsp;Calendar View</span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="tab-content">
                                    <div id="pg-calendar-list" class="tab-pane fade">
                                        <div class="text-center panel-view-past"><a class="btn-view-past btn-sm" href="#">view any past dates&nbsp;<i class="fa fa-caret-down"></i></a></div>
                                        <div id="pg-calendar-past-view"></div>
                                        <div id="pg-calendar-list-view"></div>
                                        <div id="pg-calendar-full" class="halfpad-top" style="display:none;">
                                            <div class="jumbotron">
                                                    <strong>There are no available dates to select on this Meal Train page</strong><br><br><div>Please <a href="/trains/ve6v38/message/r3z62mv/">contact the organizer</a> to request more dates.</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="pg-calendar-month" class="tab-pane fade active in">
                                        <div class="row">
                                            <div class="col-sm-12 text-right">
                                                <strong>Key:</strong>&nbsp;<i class="fa fa-square" style="color:#5cb85c;"></i>&nbsp;Available&nbsp;&nbsp;<i class="fa fa-square" style="color:#3a87ad;"></i>&nbsp;Booked
                                                <div id="pg-calendar-month-view" class="fc fc-ltr fc-unthemed"><div class="fc-toolbar"><div class="fc-left"><button type="button" class="fc-today-button fc-button fc-state-default fc-corner-left fc-corner-right fc-state-disabled" disabled="disabled">today</button><div class="fc-button-group"><button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left"><span class="fc-icon fc-icon-left-single-arrow"></span></button><button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right"><span class="fc-icon fc-icon-right-single-arrow"></span></button></div><h2>May 2021</h2></div><div class="fc-right"></div><div class="fc-center"></div><div class="fc-clear"></div></div><div class="fc-view-container" style=""><div class="fc-view fc-month-view fc-basic-view" style=""><table><thead><tr><td class="fc-widget-header"><div class="fc-row fc-widget-header"><table><thead><tr><th class="fc-day-header fc-widget-header fc-sun">Sun</th><th class="fc-day-header fc-widget-header fc-mon">Mon</th><th class="fc-day-header fc-widget-header fc-tue">Tue</th><th class="fc-day-header fc-widget-header fc-wed">Wed</th><th class="fc-day-header fc-widget-header fc-thu">Thu</th><th class="fc-day-header fc-widget-header fc-fri">Fri</th><th class="fc-day-header fc-widget-header fc-sat">Sat</th></tr></thead></table></div></td></tr></thead><tbody><tr><td class="fc-widget-content"><div class="fc-day-grid-container" style=""><div class="fc-day-grid"><div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 61px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-other-month fc-past" data-date="2021-04-25"></td><td class="fc-day fc-widget-content fc-mon fc-other-month fc-past" data-date="2021-04-26"></td><td class="fc-day fc-widget-content fc-tue fc-other-month fc-past" data-date="2021-04-27"></td><td class="fc-day fc-widget-content fc-wed fc-other-month fc-past" data-date="2021-04-28"></td><td class="fc-day fc-widget-content fc-thu fc-other-month fc-past" data-date="2021-04-29"></td><td class="fc-day fc-widget-content fc-fri fc-other-month fc-past" data-date="2021-04-30"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2021-05-01"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-other-month fc-past" data-date="2021-04-25">25</td><td class="fc-day-number fc-mon fc-other-month fc-past" data-date="2021-04-26">26</td><td class="fc-day-number fc-tue fc-other-month fc-past" data-date="2021-04-27">27</td><td class="fc-day-number fc-wed fc-other-month fc-past" data-date="2021-04-28">28</td><td class="fc-day-number fc-thu fc-other-month fc-past" data-date="2021-04-29">29</td><td class="fc-day-number fc-fri fc-other-month fc-past" data-date="2021-04-30">30</td><td class="fc-day-number fc-sat fc-past" data-date="2021-05-01">1</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 61px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2021-05-02"></td><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2021-05-03"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2021-05-04"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2021-05-05"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2021-05-06"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2021-05-07"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2021-05-08"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-past" data-date="2021-05-02">2</td><td class="fc-day-number fc-mon fc-past" data-date="2021-05-03">3</td><td class="fc-day-number fc-tue fc-past" data-date="2021-05-04">4</td><td class="fc-day-number fc-wed fc-past" data-date="2021-05-05">5</td><td class="fc-day-number fc-thu fc-past" data-date="2021-05-06">6</td><td class="fc-day-number fc-fri fc-past" data-date="2021-05-07">7</td><td class="fc-day-number fc-sat fc-past" data-date="2021-05-08">8</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 61px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2021-05-09"></td><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2021-05-10"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2021-05-11"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2021-05-12"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2021-05-13"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2021-05-14"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2021-05-15"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-past" data-date="2021-05-09">9</td><td class="fc-day-number fc-mon fc-past" data-date="2021-05-10">10</td><td class="fc-day-number fc-tue fc-past" data-date="2021-05-11">11</td><td class="fc-day-number fc-wed fc-past" data-date="2021-05-12">12</td><td class="fc-day-number fc-thu fc-past" data-date="2021-05-13">13</td><td class="fc-day-number fc-fri fc-past" data-date="2021-05-14">14</td><td class="fc-day-number fc-sat fc-past" data-date="2021-05-15">15</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 61px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2021-05-16"></td><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2021-05-17"></td><td class="fc-day fc-widget-content fc-tue fc-past" data-date="2021-05-18"></td><td class="fc-day fc-widget-content fc-wed fc-past" data-date="2021-05-19"></td><td class="fc-day fc-widget-content fc-thu fc-past" data-date="2021-05-20"></td><td class="fc-day fc-widget-content fc-fri fc-past" data-date="2021-05-21"></td><td class="fc-day fc-widget-content fc-sat fc-past" data-date="2021-05-22"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-past" data-date="2021-05-16">16</td><td class="fc-day-number fc-mon fc-past" data-date="2021-05-17">17</td><td class="fc-day-number fc-tue fc-past" data-date="2021-05-18">18</td><td class="fc-day-number fc-wed fc-past" data-date="2021-05-19">19</td><td class="fc-day-number fc-thu fc-past" data-date="2021-05-20">20</td><td class="fc-day-number fc-fri fc-past" data-date="2021-05-21">21</td><td class="fc-day-number fc-sat fc-past" data-date="2021-05-22">22</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 61px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-past" data-date="2021-05-23"></td><td class="fc-day fc-widget-content fc-mon fc-past" data-date="2021-05-24"></td><td class="fc-day fc-widget-content fc-tue fc-today fc-state-highlight" data-date="2021-05-25"></td><td class="fc-day fc-widget-content fc-wed fc-future" data-date="2021-05-26"></td><td class="fc-day fc-widget-content fc-thu fc-future" data-date="2021-05-27"></td><td class="fc-day fc-widget-content fc-fri fc-future" data-date="2021-05-28"></td><td class="fc-day fc-widget-content fc-sat fc-future" data-date="2021-05-29"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-past" data-date="2021-05-23">23</td><td class="fc-day-number fc-mon fc-past" data-date="2021-05-24">24</td><td class="fc-day-number fc-tue fc-today fc-state-highlight" data-date="2021-05-25">25</td><td class="fc-day-number fc-wed fc-future" data-date="2021-05-26">26</td><td class="fc-day-number fc-thu fc-future" data-date="2021-05-27">27</td><td class="fc-day-number fc-fri fc-future" data-date="2021-05-28">28</td><td class="fc-day-number fc-sat fc-future" data-date="2021-05-29">29</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div><div class="fc-row fc-week fc-widget-content fc-rigid" style="height: 65px;"><div class="fc-bg"><table><tbody><tr><td class="fc-day fc-widget-content fc-sun fc-future" data-date="2021-05-30"></td><td class="fc-day fc-widget-content fc-mon fc-future" data-date="2021-05-31"></td><td class="fc-day fc-widget-content fc-tue fc-other-month fc-future" data-date="2021-06-01"></td><td class="fc-day fc-widget-content fc-wed fc-other-month fc-future" data-date="2021-06-02"></td><td class="fc-day fc-widget-content fc-thu fc-other-month fc-future" data-date="2021-06-03"></td><td class="fc-day fc-widget-content fc-fri fc-other-month fc-future" data-date="2021-06-04"></td><td class="fc-day fc-widget-content fc-sat fc-other-month fc-future" data-date="2021-06-05"></td></tr></tbody></table></div><div class="fc-content-skeleton"><table><thead><tr><td class="fc-day-number fc-sun fc-future" data-date="2021-05-30">30</td><td class="fc-day-number fc-mon fc-future" data-date="2021-05-31">31</td><td class="fc-day-number fc-tue fc-other-month fc-future" data-date="2021-06-01">1</td><td class="fc-day-number fc-wed fc-other-month fc-future" data-date="2021-06-02">2</td><td class="fc-day-number fc-thu fc-other-month fc-future" data-date="2021-06-03">3</td><td class="fc-day-number fc-fri fc-other-month fc-future" data-date="2021-06-04">4</td><td class="fc-day-number fc-sat fc-other-month fc-future" data-date="2021-06-05">5</td></tr></thead><tbody><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr></tbody></table></div></div></div></div></td></tr></tbody></table></div></div></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="pg-modal-container"></div>
        
            <div class="modal fade" id="pg-invitehelp-modal" tabindex="-1" role="dialog" aria-labelledby="pg-ok-modal-label" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Invitation Options</h4>
                        </div>
                        <div class="modal-body">
                            <h4>Here is the unique link for this Meal Train</h4>
                            <p><input id="InviteHelpUrl" name="InviteHelpUrl" type="text" class="form-control" value="https://mealtrain.com/ve6v38" onfocus="this.select();"></p>
                            <br>
                            <p><strong>Personal Email</strong><br>Copy and paste the link above into your favorite email program (gmail, yahoo mail, outlook, etc).</p>
                            <br>
                            <p><strong>Via MealTrain.com</strong>&nbsp;(PC or Tablet)<br>Click the orange Invite button on the Calendar tab.</p>
                            <br>
                            <p><strong>Mobile Device</strong><br>Option 1: Click the orange Invite button to access your phone's address book.<br>Option 2: Click "View all participants" then the "Invite Others" button to invite via MealTrain.com.</p>
                            <br>
                            <p><strong>Text Message</strong><br>While viewing the Meal Train, click your phone's Share button (usually <i class="fa fa-share-alt"></i> or <i class="fa fa-share-square-o"></i>).</p>
                            <br>
                            <p><strong>Facebook</strong><br>Click the blue "f Share" button.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Adding Call to Action + Footer -->
<?php include 'footer.php';?>

<!-- Load JS here for greater good =============================-->
<script src="js/jquery-.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/anim.js"></script>

<script type="text/javascript">
    $(document).ready(function()
    {
        viewAllEvents();

        $('#swal_btn').click(function(){
            swal({
                title: "SUCCESS!",
                text: "Account created successfully.",
                type: "success",
                confirmButtonColor: "#566b8a",
                closeOnConfirm: false
            }, function () {
                window.location.assign("dashboard.php");
            });
        });

        $('#swal_btn2').click(function()
        {
            swal({
                title: "Unsuccessfull!",
                text: "Operation Unsuccessfull",
                type: "warning",
                confirmButtonColor: "#DD6B55",
            });
        });
    });

    function viewAllEvents()
    {
        var organizer_id = localStorage.organizer_id;
        var organizer_name = localStorage.organizer_name;
        var packageID    = localStorage.packageID;

        $.ajax
        ({
            type:'GET',
            url:"https://yourinnercircles.com/MT_apis/public/api/ViewPackagesUser",
            data: { "organizer" : organizer_id },
            success: function(data)
            {
                if( data.status == "success")
                {
                    $.each(data.data, function(index,obj) 
                    {   
                        if( obj.mt_pack_id == packageID )
                        {
                            localStorage.recipient_name = obj.recipient_name;
                            
                            $('#recepient_name').html(obj.recipient_name);
                            $('#organizer_name').html('<i class="fa fa-envelope"></i>&nbsp;'+organizer_name);
                            $('#recepient_name2').html('<i class="fa fa-envelope"></i>&nbsp;'+obj.recipient_name);
                            $('#recipient_address').html(obj.recipient_address);
                            $('#recipient_city').html(obj.recipient_city);
                            $('#recipient_state').html(obj.recipient_state);
                            $('#recipient_postalcode').html(obj.recipient_postalcode);
                            $('#recipient_phone').html(obj.recipient_phone);
                            $('#adults_cook_for').html(obj.adults_cook_for);
                            $('#kids_cook_for').html(obj.kids_cook_for);
                            $('#delivery_time').html(obj.delivery_time);
                            $('#special_instructions').html(obj.special_instructions);
                        }
                        //$list.append('');
                    });
                }
                else if( data.status == "failed" )
                {
                    alert("Error");
                }
            },
            error: function(data) 
            {
                l.ladda('stop');
                alert("error");
            }  
        });
    }

    function openPlan(id)
    {
        localStorage.packageID = id ;
        window.location.href = "eventpage.php";
    }
</script>

</body>
</html>