<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="">
<title>GESTR - Dashboard Page</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">
</head>
<body>

<!-- HEADER =============================-->
<header class="margin-top-0">
	<div class="wrapper">
		<!-- Adding Nav Bar -->
		<?php include 'navbar.php';?>
		
		<div class="row" style="margin-top: 100px; padding: 20px">
	        <div class="col-sm-12">               
	            <h1>My Dashboard
	            </h1>
	        </div>
	    </div>
	</div>
</header>

<div class="row noEvents" style="display: none">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="alert alert-info">Below is a list of all of the events you have created or have been invited to participate in.</div>
                <div>
                    <h3 style="color: #00BCE4">You are not currently a participant in any Event pages.</h3><br>
                    <p>To organize a new Event page, click&nbsp;&nbsp;<a class="btn btn-primary btn-sm" href="member.php">Get Started</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row Events" style="display: none">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div id="events_list" class="panel-body">
                <!-- Appending Data Dynamically -->
            </div>
        </div>
    </div>
</div>


<!-- Adding Call to Action + Footer -->
<?php include 'footer.php';?>

<!-- Load JS here for greater good =============================-->
<script src="js/jquery-.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/anim.js"></script>

<script type="text/javascript">
    $(document).ready(function()
    {
        viewAllEvents();

        $('#swal_btn').click(function(){
            swal({
                title: "SUCCESS!",
                text: "Account created successfully.",
                type: "success",
                confirmButtonColor: "#566b8a",
                closeOnConfirm: false
            }, function () {
                window.location.assign("dashboard.php");
            });
        });

        $('#swal_btn2').click(function()
        {
            swal({
                title: "Unsuccessfull!",
                text: "Operation Unsuccessfull",
                type: "warning",
                confirmButtonColor: "#DD6B55",
            });
        });
    });

    function viewAllEvents()
    {
        var organizer_id = localStorage.organizer_id;
        var $list = $("#events_list");
        $list.empty();

        $.ajax
        ({
            type:'GET',
            url:"https://yourinnercircles.com/MT_apis/public/api/ViewPackagesUser",
            data: { "organizer" : organizer_id },
            success: function(data)
            {
                if( data.status == "success")
                {
                    $('.noEvents').css('display','none');
                    $('.Events').css('display','block');

                    $.each(data.data, function(index,obj) 
                    {          
                        if( index == 0 )
                        {
                            $list.append('<div class="alert alert-info">Below is a list of all of the events you have created or have been invited to participate in.</div>');
                        }

                        $list.append('<div class="row pg-entitylist-row">'+
                                        '<div class="col-sm-2 col-xs-3 text-center">'+
                                            '<img src="/content/img/app/tokens/appointments-sm.png" alt="" title="">'+
                                        '</div>'+
                                        '<div class="col-sm-5 col-xs-9 pg-col-name">'+
                                            '<strong>Meal Train for:</strong>'+
                                            '<a href="/trains/72g4qm"><h3 class="h-tight">'+obj.recipient_name+'</h3></a>'+
                                        '</div>'+
                                        '<div class="col-sm-5 col-sm-offset-0 col-xs-offset-3 col-xs-9">'+
                                            '<a id="'+obj.mt_pack_id+'" title="Open" onclick="openPlan(this.id)" class="btn btn-primary" ><i class="fa fa-search"></i>&nbsp;Open</a>&nbsp;'+
                                            '<a title="Edit" class="btn btn-default" ><i class="fa fa-pencil"></i>&nbsp;<span class="visible-xs-inline">Edit</span><span class="hidden-xs">Make Changes</span></a>&nbsp;'+
                                        '</div>'+
                                    '</div><hr>');
                    });
                }
                else if( data.status == "failed" )
                {
                    $('.noEvents').css('display','block');
                    $('.Events').css('display','none');
                }
            },
            error: function(data) 
            {
                l.ladda('stop');
                alert("error");
            }  
        });
    }

    function openPlan(id)
    {
        localStorage.packageID = id ;
        window.location.href = "eventpage.php";
    }
</script>

</body>
</html>