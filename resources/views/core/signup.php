<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="generator" content="">
	<title>GESTR - Signup Page</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">

	<!-- Sweet Alert -->
    <link href="css/sweetalert.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="css/ladda-themeless.min.css" rel="stylesheet">
    
	<style>
		form 
		{
			border: 3px solid #f1f1f1;
		}

		fieldset
		{
			display: none;
		}

		input[type=text], input[type=email], input[type=password] 
		{
		  width: 100%;
		  padding: 12px 20px;
		  margin: 8px 0;
		  display: inline-block;
		  border: 1px solid #ccc;
		  box-sizing: border-box;
		  height: 55px;
		}

		#register_btn {
		  background-color: #04AA6D;
		  color: white;
		  padding: 14px 20px;
		  margin: 8px 0;
		  border: none;
		  cursor: pointer;
		  width: 100%;
		}

		#register_btn:hover 
		{
		  opacity: 0.8;
		}
	</style>
</head>
<body>

<!-- HEADER =============================-->
<header class="item header margin-top-0">
	<div class="wrapper">
		<!-- Adding Nav Bar -->
		<?php include 'navbar.php';?>
	</div>
</header>
<!-- CONTENT =============================-->
<section class="item content" style="margin-top: 200px;">
	<div class="container toparea toparea-contact">
		<div class="underlined-title">
			<div class="editContent">
				<h1 class="text-center latestitems">Sign Up</h1>
			</div>
			<div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<div style="background-color:#f1f1f1; padding: 10px">
					<span>Already have an Account? <a href="login.php"><b>Sign In</b></a></span>
				</div>
				<form id="signup_form" autocomplete="false" style="padding: 10px">
					<div class="form-group">
						<label for="fname">First Name:</label>
						<input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname">
					</div>
					<div class="form-group">
						<label for="lname">Last Name:</label>
						<input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname">
					</div>
					<div class="form-group">
						<label for="email">Email:</label>
						<input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
					</div>
					<div class="form-group">
						<label for="password">Password:</label>
						<input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
					</div>
					<div class="form-group">
                        <button id="register_btn" class="ladda-button ladda-button-demo" type="submit" data-style="zoom-in">
                        	<span class="ladda-label">REGISTER</span><span class="ladda-spinner"></span>
                        </button>
                    </div>				
				</form>				
			</div>
		</div>
	</div>
</section>

<!-- Adding Call to Action + Footer -->
<?php include 'footer.php';?>

<!-- Successfully Registered Employee -->
<button id="swal_btn" type="button" style="display:none"></button>
<!-- Employee not  Register -->
<button id="swal_btn2" type="button" style="display:none"></button>

<!-- SCRIPTS =============================-->
<script src="js/jquery-.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/anim.js"></script>
<script src="js/validate.js"></script>

<!-- Sweet alert -->
<script src="js/sweetalert.min.js"></script>

<!-- Jquery Validate -->
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>

<!-- Ladda -->
<script src="js/spin.min.js"></script>
<script src="js/ladda.min.js"></script>
<script src="js/ladda.jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function()
	{
		var tempFormData = localStorage.FullFormData;

	    $("#signup_form").validate({
	        rules: 
	        {
	        	fname: {
	                required: true
	            },               
	            lname: {
	                required: true,
	            },   
	            email: {
	                required: true
	            },               
	            password: {
	                required: true,
	            },               
	        },
	        messages: 
	        {
	        },
	        submitHandler: function(form) 
	        {
	            var l = $( '.ladda-button-demo' ).ladda();
	            l.ladda( 'start' );

	            var fname = $('#fname').val();
	            var lname = $('#lname').val();
	            var email = $('#email').val();
	            var password = $('#password').val();

                var formData = new FormData();
                formData.append("first_name", fname);
                formData.append("last_name", lname);
                formData.append("email", email);
                formData.append("password", password);
                    
                $.ajax
                ({
                    type:'POST',
                    url:"https://yourinnercircles.com/MT_apis/public/api/CreateGkUser",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        if( data.status == "success")
                        {
                        	localStorage.UserSession    = "Session";
                        	localStorage.organizer_id   = data.id;
                        	localStorage.organizer_name = data.fullname;

                        	if( tempFormData )
							{
								formData = JSON.parse(localStorage.FullFormData);

	                        	var formData2 = new FormData();

	                        	for (let i=0; i<formData.length; ++i) 
	                        	{
								    const item = formData[i];
								    if(item[0] == "organizer")
								    {
								    	formData2.append(item[0], data.id);
								    }
									else
									{
								    	formData2.append(item[0], item[1]);
								    }
								}

	                        	$.ajax
				                ({
				                    type:'POST',
				                    url:"https://yourinnercircles.com/MT_apis/public/api/BuyPackage",
				                    data: formData2,
				                    contentType: false,
				                    cache: false,
				                    processData:false,
				                    success: function(data)
				                    {
				                        if( data.status == "success")
				                        {
				                        	localStorage.FullFormData = "";
											localStorage.removeItem("FullFormData");

				                            l.ladda('stop');
				                            swal_btn.click();
				                        }
				                        else if( data.status == "failed" )
				                        {
				                            l.ladda('stop');
				                            //errorMessage = data.error;
				                            swal_btn2.click();
				                        }
				                    },
				                    error: function(data) 
				                    {
				                        l.ladda('stop');
				                        alert("error");
				                    }  
				                });
							}
							else
							{
								localStorage.FullFormData = "";
								localStorage.removeItem("FullFormData");
								
	                            l.ladda('stop');
	                            swal_btn.click();
							}
                        }
                        else if( data.status == "failed" )
                        {
                            l.ladda('stop');
                            //errorMessage = data.error;
                            swal_btn2.click();
                        }
                    },
                    error: function(data) 
                    {
                    	l.ladda('stop');
                        alert("error");
                    }  
                });       
	            return false; // extra insurance preventing the default form action
	        }
	    });

	    $('#swal_btn').click(function(){
	        swal({
	            title: "SUCCESS!",
	            text: "Account created successfully.",
	            type: "success",
	            confirmButtonColor: "#566b8a",
	            closeOnConfirm: false
	        }, function () {
	            window.location.assign("dashboard.php");
	        });
	    });


	    $('#swal_btn2').click(function()
	    {
	        swal({
	            title: "Unsuccessfull!",
	            text: "Operation Unsuccessfull",
	            type: "warning",
	            confirmButtonColor: "#DD6B55",
	        });
	    });
	});
</script>
</body>
</html>