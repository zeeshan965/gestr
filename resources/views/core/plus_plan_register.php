<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="generator" content="">
    <title>GESTR - Plus Plan Page</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:200,300,400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/plus_calendar.css"/>
    <link rel="stylesheet" href="css/plus_calendar_demo.css"/>
    
    <!-- Sweet Alert -->
    <link href="css/sweetalert.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="css/ladda-themeless.min.css" rel="stylesheet">

    <style type="text/css">
        .required_star
        {
            color: red;
        }
        .disabled
        {
            pointer-events: none;
        }
        input
        {
            height: 50px !important;
        }

        .activeSM
        {
            background-color: white !important; /* #38283c !important;*/
        }
        .container 
        {
          margin-top: 15px;
        }
        .tab-group
        {
           position: relative;
           vertical-align: middle;
           zoom: 1; /* Fix for IE7 */
           *display: inline; /* Fix for IE7 */
        }
        
        .tab-group > li
        {
            background-color: #eeeeee;
            border-radius: 4px;
            position: relative;
            float: left;
        }
        /*.nav > li.active 
        {
            border-left: 4px solid #337ab7;
            background: #337ab7;
        }
        .tab-group > li.active > a, 
        .tab-group > li.active > a:hover, 
        .tab-group > li.active > a:focus
        {
            background-color: #337ab7;
            color: #fff;
        }*/
        .tab-group > li > a 
        {
            color: #337ab7;
            border-radius: 0;
            text-align: center;
        }
        .tab-group > li > a:hover 
        {
            border-radius: 4px;
        }
        
        .tab-group li + li 
        {
            margin-left: -1px;
        }
        
        .tab-group > li:not(:first-child):not(:last-child),
        .tab-group > li:not(:first-child):not(:last-child) > a:hover 
        {
          border-radius: 0;
        }
        .tab-group > li:first-child,
        .tab-group > li:first-child > a:hover 
        {
            margin-left: 0;
        }
        .tab-group > li:first-child:not(:last-child),
        .tab-group > li:first-child:not(:last-child) > a:hover 
        {
          border-top-right-radius: 0;
          border-bottom-right-radius: 0;
        }
        .tab-group > li:last-child:not(:first-child),
        .tab-group > li:last-child:not(:first-child) > a:hover 
        {
          border-top-left-radius: 0;
          border-bottom-left-radius: 0;
        }
    </style>
    
</head>
<body>

<!-- HEADER =============================-->
<header class="item header margin-top-0">
    <div class="wrapper">
        <!-- Adding Nav Bar -->
        <?php include 'navbar.php';?>
    </div>
</header>
<!-- CONTENT =============================-->
<!-- MultiStep Form -->
<section class="item content" style="margin-top: 100px;">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-content">
                                <div class="tabbable">
                                    <ul class="nav tab-group"><!--nav-tabs nav-justified-->
                                        <li class="TAB col-sm-4 col-xs-12 active" onclick="clickTab(this.id)" id="Recepient">
                                            <a data-toggle="tab"><strong>1</strong> Enter Recepient</a>
                                        </li>
                                        <li class="TAB col-sm-4 col-xs-12" onclick="clickTab(this.id)" id="Dates">
                                            <a data-toggle="tab"><strong>2</strong> Create Calendar</a>
                                        </li>
                                        <li class="TAB col-sm-4 col-xs-12" onclick="clickTab(this.id)" id="Preferences">
                                            <a data-toggle="tab"><strong>3</strong> Add Preferences</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     

                <!-- Recepient details Form -->
                <form id="recepient_form" style="display: block" class="form-horizontal" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-offset-1 col-sm-9">
                            <h3>This Gestr page is for:</h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventName" class="col-sm-2 control-label">Name<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="EventName" name="EventName" placeholder="Recipient name" required="">
                            <small>Example: The Smith Family</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventEmail" class="col-sm-2 control-label">Email<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="EventEmail" name="EventEmail" placeholder="Recipient email address" required="" >
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <small>
                                <a id="btnWhy" style="text-decoration:underline;" href="#" tabindex="9999" data-toggle="modal" data-target="#myModal">
                                Why do we ask for this information?</a>
                            </small>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <hr>
                            <div class="alert alert-info"><small><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Enter the address where meals are to be dropped off.</small></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="EventAddr1" class="col-sm-2 control-label">Address<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="EventAddr1" name="EventAddr1" placeholder="Omit for more privacy" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="text" class="form-control" id="EventAddr2" name="EventAddr2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventCity" class="col-sm-2 control-label">City<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="EventCity" name="EventCity" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventState" class="col-sm-2 control-label">State/Prov.<span class="required_star">*</span></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="EventState" name="EventState" required="">
                        </div>
                        <label for="EventZip" class="col-sm-2 control-label">Postal Code<span class="required_star">*</span></label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="EventZip" name="EventZip" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventPhone" class="col-sm-2 control-label">Phone<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="EventPhone" name="EventPhone" required="">
                        </div>
                    </div>
                </form>              
                
                <!-- Calendar Dates Form -->
                <form id="calendar_form" style="display: none" class="form-horizontal" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-info">
                                <i class="fa fa-calendar"></i>&nbsp;&nbsp;Click all the days when meals can be delivered
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Wrapper -->
                            <div id="wrapper">
                                <!-- Calendar element -->
                                <div id="events-calendar"></div>
                                <!-- Events -->
                                <div id="events"></div>
                                <!-- Clear -->
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </form>   

                <!-- Preferences Form -->
                <form id="preferences_form" style="display: none ; margin-top: 20px" class="form-horizontal" autocomplete="off">
                    <div class="form-group">
                        <label for="EventName" class="col-sm-2 control-label"># Adults to cook for<span class="required_star">*</span></label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" id="EventNumAdults" name="EventNumAdults">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventEmail" class="col-sm-2 control-label"># Kids to cook for<span class="required_star">*</span></label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" id="EventNumKids" name="EventNumKids">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventAddr1" class="col-sm-2 control-label">Preferred delivery time<span class="required_star">*</span></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="EventTime" name="EventTime" maxlength="100">
                            <small>Example: from 5PM - 6PM</small>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="alert alert-info"><small><i class="fa fa-comment-o"></i>&nbsp;&nbsp;Add special instructions, dietary preferences, or anything else you think the event participants may need to know.</small></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="EventCity" class="col-sm-2 control-label">Special instructions<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="EventInstructions" name="EventInstructions" rows="5"></textarea>
                            <small>List any dropoff, delivery, or other instructions</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventLikes" class="col-sm-2 control-label">Favorite meals/<span class="hidden-xs"> </span>Restaurants<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="EventLikes" name="EventLikes" rows="5"></textarea>
                            <small>Examples: lasagna, chili, etc</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventDislikes" class="col-sm-2 control-label">Least Favorite meals<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="EventDislikes" name="EventDislikes" rows="5"></textarea>
                            <small>Example: anchovies, broccoli, etc</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="EventRestrictions" class="col-sm-2 control-label">Allergies or dietary restrictions<span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="EventRestrictions" name="EventRestrictions" rows="5"></textarea>
                            <small>Example: allergic to shellfish, vegan, gluten-free, etc</small>
                        </div>
                    </div>
                </form>

                <hr>
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <div>
                            <button onclick="buttonClicks('Submit')" type="button" id="btnSubmit" style="display: none" class="ladda-button ladda-button-demo btn btn-lg btn-primary btn-responsive-block pull-right" data-loading-text="Saving..." data-style="zoom-in">
                                <span class="ladda-label">Submit&nbsp;
                                <i class="fa fa-caret-right"></i></span>
                                <span class="ladda-spinner"></span>                                
                            </button>
                            <button onclick="buttonClicks('NextStep')" type="button" id="btnNext" style="display: block" class="btn btn-lg btn-primary btn-responsive-block pull-right">Next Step&nbsp;
                                <i class="fa fa-caret-right"></i>
                            </button>
                            <button onclick="buttonClicks('Back')" type="button" id="btnBack" style="display: none;margin-right: 10px;" class="btn btn-lg btn-default btn-responsive-block pull-right">
                                <i class="fa fa-caret-left"></i>&nbsp;Back
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Adding Call to Action + Footer -->
<?php include 'footer.php';?>

<!-- Information Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recipient Information</h4>
            </div>
            <div class="modal-body">
                <h2>Why we ask for this information:</h2>
                <hr>
                <div class="row pad-bottom">
                    <div class="col-sm-4"><strong>Recipient Email Address:</strong></div>
                    <div class="col-sm-6"><p>Used to invite the recipient to view/edit the Meal Train page, and used for email notifications of date booking or cancellation. Recipient email is not displayed on the Meal Train page.</p></div>
                </div>
                <div class="row pad-bottom">
                    <div class="col-sm-4"><strong>Recipient Physical Address:</strong></div>
                    <div class="col-sm-6"><p>Optional: Used to inform participants where to drop off meals.</p></div>
                </div>
                <div class="row pad-bottom">
                    <div class="col-sm-4"><strong>Recipient Phone Number:</strong></div>
                    <div class="col-sm-6"><p>Optional: Helpful for participants who may have last minute questions.</p></div>
                </div>
                <p class="pad-bottom">We take privacy very seriously and do not share or sell personal information.  You can find our privacy policy at the bottom of every page.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Calendar Modal -->
<div id="calendarModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add to Calendar</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-info"><small><i class="fa fa-info-circle"></i>&nbsp;&nbsp;Describe what is needed</small></div>
                    </div>
                </div>
                <form id="add_to_calendar_form" class="form-horizontal" autocomplete="off">
                    <div class="form-group">
                        <label for="Category" class="col-sm-12">Category<span class="required_star">*</span></label>
                        <div class="col-sm-12">
                            <select class="form-control" id="category" name="category">
                                <option value="Meal" selected="">Meal</option>
                                <option value="Groceries">Groceries</option>
                                <option value="Help">Help</option>
                                <option value="Visit">Visit</option>
                                <option value="Ride">Ride</option>
                                <option value="Child Care">Child Care</option>
                                <option value="Communication">Communication</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Needed" class="col-sm-12">Needed<span class="required_star">*</span></label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="needed" name="needed">
                            <small>Example: Dinner</small>
                        </div>
                    </div>    
                    <button type="submit" style="width:100%" class="btn btn-lg btn-primary">
                    SAVE      
                    </button>                
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Successfully Registered Employee -->
<button id="swal_btn" type="button" style="display:none"></button>
<!-- Employee not  Register -->
<button id="swal_btn2" type="button" style="display:none"></button>

<!-- SCRIPTS =============================-->
<script src="js/jquery-.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/plus_calendar.js"></script>
<script type="text/javascript" src="js/plus_calendar_demo.js"></script>

<!-- Sweet alert -->
<script src="js/sweetalert.min.js"></script>

<!-- Jquery Validate -->
<script src="js/jquery.validate.min.js"></script>
<script src="js/additional-methods.min.js"></script>

<!-- Ladda -->
<script src="js/spin.min.js"></script>
<script src="js/ladda.min.js"></script>
<script src="js/ladda.jquery.min.js"></script>

<script type="text/javascript">
    var formData = new FormData();
    var dateArr = [1,2,3];
    var dateslotsArray = [];
    var recepient_form_status = 0;
    var calendar_form_status = 0;
    var preferences_form_status = 0;
    var add_to_calendar_id;
    var signin;
    var organizer_id = localStorage.organizer_id;

    $(document).ready(function()
    {
        scrollToTop();

        signin = localStorage.UserSession;

        if( organizer_id )
        {
            //Do Nothing
        }
        else
        {
            organizer_id = "";
        }
        
        //$('#calendarModal').modal('show');
        changeColors('Recepient');

        $("#recepient_form").validate({
            rules: 
            {
                EventName: {
                    required: true,
                },    
                EventEmail: {
                    required: true,
                },               
                EventAddr1: {
                    required: true,
                },                 
                EventCity: {
                    required: true,
                },  
                EventState: {
                    required: true,
                },               
                EventZip: {
                    required: true,
                },               
                EventPhone: {
                    required: true,
                },       
            },
            messages: 
            {
            },
            submitHandler: function(form) 
            {
                var EventName = $('#EventName').val();
                var EventEmail = $('#EventEmail').val();
                var EventAddr1 = $('#EventAddr1').val();
                var EventAddr2 = $('#EventAddr2').val();
                var EventCity = $('#EventCity').val();
                var EventState = $('#EventState').val();
                var EventZip = $('#EventZip').val();
                var EventPhone = $('#EventPhone').val();

                formData.append("url", "yahoo.com");
                formData.append("organizer", organizer_id);
                formData.append("recipient_name", EventName);
                formData.append("recipient_email", EventEmail);
                formData.append("recipient_address", EventAddr1 + " " + EventAddr2);
                formData.append("recipient_city", EventCity);
                formData.append("recipient_state", EventState);
                formData.append("recipient_postalcode", EventZip);
                formData.append("recipient_phone", EventPhone);
                formData.append("story", "some story text");   

                recepient_form_status = 1;                 
                   
                return false; // extra insurance preventing the default form action
            }
        });

        $("#preferences_form").validate({
            rules: 
            {
                EventNumAdults: {
                    required: true,
                    number: true,
                },    
                EventNumKids: {
                    required: true,
                    number: true,
                },               
                EventTime: {
                    required: true,
                },                 
                EventInstructions: {
                    required: true,
                },  
                EventLikes: {
                    required: true,
                },               
                EventDislikes: {
                    required: true,
                },               
                EventRestrictions: {
                    required: true,
                },       
            },
            messages: 
            {
            },
            submitHandler: function(form) 
            {
                var l = $( '.ladda-button-demo' ).ladda();
                l.ladda( 'start' );

                var EventNumAdults = $('#EventNumAdults').val();
                var EventNumKids = $('#EventNumKids').val();
                var EventTime = $('#EventTime').val();
                var EventInstructions = $('#EventInstructions').val();
                var EventLikes = $('#EventLikes').val();
                var EventDislikes = $('#EventDislikes').val();
                var EventRestrictions = $('#EventRestrictions').val();

                formData.append("adults_cook_for", EventNumAdults);
                formData.append("kids_cook_for", EventNumKids);
                formData.append("delivery_time", EventTime);
                formData.append("special_instructions", EventInstructions);
                formData.append("fave_meals_rest", EventLikes);
                formData.append("least_fave_meals", EventDislikes);
                formData.append("restrictions", EventRestrictions); 
                formData.append("package_name", "Meal Train Plus"); 
                formData.append("package_payment", "$10");
                formData.append("photo", "");

                //Preparing object
                $.each(myArray, function(key,value) 
                {
                    $.each(value, function(index,value2) 
                    {
                        dateslotsArray.push(value2);
                    });
                });

                formData.append("dateslots", JSON.stringify(dateslotsArray));


                $.ajax
                ({
                    type:'POST',
                    url:"https://yourinnercircles.com/MT_apis/public/api/BuyPackage",
                    data: formData,
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        if( data.status == "success")
                        {
                            l.ladda('stop');
                            swal_btn.click();
                        }
                        else if( data.status == "failed" )
                        {
                            l.ladda('stop');
                            //errorMessage = data.error;
                            swal_btn2.click();
                        }
                    },
                    error: function(data) 
                    {
                        l.ladda('stop');
                        alert("error");
                    }  
                });  
                   
                return false; // extra insurance preventing the default form action
            }
        });

        $("#add_to_calendar_form").validate({
            rules: 
            {
                category: {
                    required: true,
                },    
                needed: {
                    required: true,
                },                 
            },
            messages: 
            {
            },
            submitHandler: function(form) 
            {
                $('#calendarModal').modal('hide');

                var category = $('#category').val();
                var needed = $('#needed').val(); 

                var name = category + ": " + needed;

                //Return on cancel
                  if (name === null || name === "") 
                  {
                    return;
                  }

                  // Date string
                  var id = jsCalendar.tools.dateToString(current, date_format, "en");
                  var dateslot = jsCalendar.tools.dateToString(current, date_format2, "en");


                  // If no events, create list
                  if (!events.hasOwnProperty(id)) 
                  {
                    // Create list
                    events[id]  = [];
                    myArray[id] = [];
                  }

                  // If where were no events
                  if (events[id].length === 0) 
                  {
                    // Select date
                    calendar.select(current);
                  }

                  // Add event
                  events[id].push({name : name});
                  myArray[id].push({"mt_pack_id" : 1, "category" : category, "needed" : needed, "dateslots" : dateslot });
                  
                  calendarCounter++;

                  //alert(JSON.stringify(events));
                  //alert(JSON.stringify(myArray));

                  // Refresh events
                  showEvents(current);
                   
                return false; // extra insurance preventing the default form action
            }
        });

        $('#swal_btn').click(function(){
            swal({
                title: "SUCCESS",
                text: "Basic Plan added successfully!",
                type: "success",
                confirmButtonColor: "#566b8a",
                closeOnConfirm: false
            }, function () {
                window.location.assign("dashboard.php");
            });
        });

        $('#swal_btn2').click(function()
        {
            swal({
                title: "Operation Unsuccessfull!",
                text: "Basic Plan not added.",
                type: "warning",
                confirmButtonColor: "#DD6B55",
            });
        });
    });

    function clickTab(id)
    {
        scrollToTop();  

        var form_name = id;

        if( form_name == "Recepient" )
        {
            changeColors('Recepient');

            //Forms
            $('#recepient_form').css('display','block');
            $('#calendar_form').css('display','none');
            $('#preferences_form').css('display','none');

            //Buttons
            $('#btnSubmit').css('display','none');
            $('#btnNext').css('display','block');
            $('#btnBack').css('display','none');
        }
        else if( form_name == "Dates" )
        {
            recepient_form_status = 0;
            $('#recepient_form').submit();

            if( recepient_form_status == 0 )
            {
                $('#recepient_form').submit();

                changeColors('Recepient');
                $('#recepient_form').css('display','block');
                $('#calendar_form').css('display','none');
                $('#preferences_form').css('display','none');

                //Buttons
                $('#btnSubmit').css('display','none');
                $('#btnNext').css('display','block');
                $('#btnBack').css('display','none');
            }
            else
            {
                changeColors('Dates');

                //Forms
                $('#recepient_form').css('display','none');
                $('#calendar_form').css('display','block');
                $('#preferences_form').css('display','none');

                //Buttons
                $('#btnSubmit').css('display','none');
                $('#btnNext').css('display','block');
                $('#btnBack').css('display','block');
            }
        }
        else
        {
            recepient_form_status = 0;
            $('#recepient_form').submit();

            //calendar_form_status = dateArr.length;

            calendar_form_status = calendarCounter;

            if( recepient_form_status == 0 )
            {
                $('#recepient_form').submit();

                changeColors('Recepient');

                //Forms
                $('#recepient_form').css('display','block');
                $('#calendar_form').css('display','none');
                $('#preferences_form').css('display','none');

                //Buttons
                $('#btnSubmit').css('display','none');
                $('#btnNext').css('display','block');
                $('#btnBack').css('display','none');
            }
            else if( calendar_form_status == 0 )
            {
                alert("Please choose Dates First.")
                changeColors('Dates');

                //Forms
                $('#recepient_form').css('display','none');
                $('#calendar_form').css('display','block');
                $('#preferences_form').css('display','none');

                //Buttons
                $('#btnSubmit').css('display','none');
                $('#btnNext').css('display','block');
                $('#btnBack').css('display','block');
            }
            else
            {
                changeColors('Preferences');

                //Forms
                $('#recepient_form').css('display','none');
                $('#calendar_form').css('display','none');
                $('#preferences_form').css('display','block');

                //Buttons
                $('#btnSubmit').css('display','block');
                $('#btnNext').css('display','none');
                $('#btnBack').css('display','block');
            }
        }
    }

    function scrollToTop()
    {
        $("html, body").animate({ scrollTop: 50 }, "slow");
    }

    function buttonClicks(id)
    {
        if( id == "NextStep")
        {
            var styledisplay = $('#btnBack').css('display');
            if( styledisplay == "none" )
            {
                clickTab('Dates');
            }
            else
            {
                clickTab('Preferences');
            }
        }
        else if( id == "Back" )
        {
            var styledisplay = $('#btnNext').css('display');
            if( styledisplay == "block" )
            {
                clickTab('Recepient');
            }
            else
            {
                clickTab('Dates');
            }   
        }
        else
        {
            scrollToTop();
            $('#preferences_form').submit();
        }
    }

    function changeColors(id)
    {
        $('.tab-group li').css('color' , '#337ab7');
        $('.tab-group li').css('background-color' , '#eeeeee');
        $('.tab-group li a').css('background-color' , '#eeeeee');
        $('.tab-group li a').css('color' , '#337ab7');
        $('.tab-group li').css('border-left' , '4px solid #eeeeee');

        $('#'+id).css({
            'background-color':'#337ab7',
            'color':'white',
            'border-left':'4px solid #337ab7',
        });

        $('#'+id+' a').css({
            'background-color':'#337ab7',
            'color':'white',
        });
    }

    function compareDates(dateObj)
    {
        dateObj += " 23:59:59";

        var g1 = new Date();
        var g2 = new Date(dateObj)
        var result;

        if( g1.getTime() < g2.getTime() )
        {
            result = 1;
        }
        else if( g1.getTime() > g2.getTime() )
        {
            result = 0 ;
        }

        return result;
    }

    function editEvent()
    {
        alert("afaksjf");
    }
</script>

</body>
</html>