@section('extra_css')
    <link href="{{ asset ( 'css/aboutstyle.css' ) }}" rel="stylesheet"/>
@endsection

@section( 'extra_js' )
@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('Product Page') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.0s">
                    SERVICES
                </div>
            </div>
        </div>
    </x-slot>

    <!-- CONTENT -->
    <section class="item content">
        <div class="container toparea">
            <div class="underlined-title">
                <div class="editContent">
                    <h1 class="text-center latestitems">Meal Preparation</h1>
                </div>
                <div class="wow-hr type_short">
                    <span class="wow-hr-h">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="productbox">
                        <img src="{{ asset ( 'images/service1.jpg' ) }}" alt="">
                        <div class="clearfix"></div>
                        <br/>
                        <div class="product-details text-left">
                            <p>
                                Your description here. Serenity is a highly-professional & modern website theme crafted
                                with you, the user, in mind. This light-weight theme is generous, built with custom
                                types and enough shortcodes to customize each page according to your project. You will
                                notice some examples of pages in demo, but this theme can do much more.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <a href="{{ route ( 'checkout' ) }}" class="btn btn-buynow">$49.00 - Purchase</a>
                    <div class="properties-box">
                        <ul class="unstyle">
                            <li><b class="propertyname">Version:</b> 1.0</li>
                            <li><b class="propertyname">Image Size:</b> 2340x1200</li>
                            <li><b class="propertyname">Files Included:</b> mp3, audio, jpeg, png</li>
                            <li><b class="propertyname">Documentation:</b> Well Documented</li>
                            <li><b class="propertyname">License:</b> GNU</li>
                            <li><b class="propertyname">Requires:</b> Easy Digital Downloads</li>
                            <li><b class="propertyname">Environment:</b> Laravel</li>
                            <li><b class="propertyname">Any Field Etc:</b> Any Detail</li>
                            <li><b class="propertyname">Number:</b> Up to 20 specifications in this box</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

</x-app-layout>