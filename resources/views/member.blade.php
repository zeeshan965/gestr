@section('extra_css')
    <link href="{{ asset ( 'css/aboutstyle.css' ) }}" rel="stylesheet"/>
@endsection

@section( 'extra_js' )
    <script src="{{ asset ( 'js/validate.js' ) }}"></script>
@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('Member') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    FIND THE MEMBERSHIP THAT SUITS YOU BEST
                </div>
            </div>
        </div>
    </x-slot>

    <!-- CONTENT -->
    <section class="item content">
        <div class="container toparea">
            <div class="underlined-title">
                <div class="editContent">
                    <h1 class="text-center latestitems">Membership Plans</h1>
                </div>
                <div class="wow-hr type_short">
                    <span class="wow-hr-h">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </span>
                </div>
            </div>

            <div id="generic_price_table">
                <section>
                    <div class="container" style="margin-left: -10px;">
                        <!--BLOCK ROW START-->
                        <div class="row" style="margin-right: 0px;background-color: whitesmoke">
                            <div class="col-md-4">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Gestr Club</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->

                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">
                                            <span class="price">
		                                        <span class="sign">FREE</span>
		                                        <span class="currency"></span>
		                                        <span class="cent"></span>
		                                        <span class="month"></span>
		                                    </span>
                                        </div>
                                        <!--//PRICE END-->
                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list">
                                        <ul>
                                            <li><span></span>Bio</li>
                                            <li><span></span>Picture</li>
                                            <li><span></span>Allows easy booking for Up to 1 organiser</li>
                                            <li><span></span>Calendar</li>
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <div class="generic_price_btn clearfix">
                                        <a class="" href="{{ route ( 'basic_plan_register' ) }}">Join Today</a>
                                    </div>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>

                            <div class="col-md-4">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Gestr Club Plus</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->

                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">
		                                <span class="price">
		                                    <span class="sign">$</span>
		                                    <span class="currency">5</span>
		                                    <span class="cent">.99</span>
		                                    <span class="month">/MON</span>
		                                </span>
                                        </div>
                                        <!--//PRICE END-->
                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list">
                                        <ul>
                                            <li><span></span>Bio</li>
                                            <li><span></span>Pictures</li>
                                            <li><span></span>Video Chat</li>
                                            <li><span></span>Messages</li>
                                            <li><span></span>Upto 5 Organizers</li>
                                            <li><span></span>Interactive Bookings</li>
                                            <li><span></span>Donation Button</li>
                                            <li><span></span>Social Media Link</li>
                                            <li><span></span>Email Link</li>
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <div class="generic_price_btn clearfix">
                                        <a class="" href="{{ route ( 'plus_plan_register' ) }}">Join Today</a>
                                    </div>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>
                            <div class="col-md-4">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Gestr Club Platinum</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->

                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">
		                                <span class="price">
		                                    <span class="sign">$</span>
		                                    <span class="currency">9</span>
		                                    <span class="cent">.99</span>
		                                    <span class="month">/MON</span>
		                                </span>
                                        </div>
                                        <!--//PRICE END-->

                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->
                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list">
                                        <ul>
                                            <li><span></span>Bio</li>
                                            <li><span></span>Pictures</li>
                                            <li><span></span>Video Chat</li>
                                            <li><span></span>Messages</li>
                                            <li><span></span>Upto 5 Organizers</li>
                                            <li><span></span>Interactive Bookings</li>
                                            <li><span></span>Donation Button</li>
                                            <li><span></span>Social Media Link</li>
                                            <li><span></span>Email Link</li>
                                            <li><span></span>Discounted off services and products</li>
                                            <li><span></span>Gestr account manager</li>
                                            <li><span></span>Send discounted services to friends and family</li>
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <div class="generic_price_btn clearfix">
                                        <a class="" href="{{ route ( 'login' ) }}">Join Today</a>
                                    </div>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>
                        </div>
                        <!--//BLOCK ROW END-->
                    </div>
                </section>
            </div>

            <div class="underlined-title">
                <div class="editContent">
                    <h1 class="text-center latestitems">The Acts of GESTR</h1>
                    <center><small>(Our Products & Services)</small></center>
                </div>
                <div class="wow-hr type_short">
                    <span class="wow-hr-h">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service1.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Meal Preparation</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$49.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>PRODUCT</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service3.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Personalised Gifts</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$69.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service2.png') }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Cleaning</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$79.00</span>
						</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service1.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Meal Preparation</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$49.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>PRODUCT</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service3.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Personalised Gifts</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$69.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service2.png' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Cleaning</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$79.00</span>
						</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service1.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Meal Preparation</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$49.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>PRODUCT</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service3.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Personalised Gifts</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$69.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a href="{{ route ( 'checkout' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-shopping-cart"></i> Purchase
                                    </a>
                                    <a href="{{ route ( 'product' ) }}" class="learn-more detailslearn">
                                        <i class="fa fa-link"></i>Details
                                    </a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset ( 'images/service2.png' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Cleaning</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$79.00</span>
						</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</x-app-layout>