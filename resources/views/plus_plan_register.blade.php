@section('extra_css')
    <link rel="stylesheet" href="{{ asset ( 'css/demo.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/theme2.css' ) }}"/>

    <link rel="stylesheet" href="{{ asset ( 'css/app-calendar.min.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/full-calendar.min.css' ) }}"/>

    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">

    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">

    <style type="text/css">
        #btnUpdateEvent {
            margin-left: 5px;
        }

        .span-wrap {
            width: 100%;
            height: 100px;
            float: left;
            display: none;
        }

        .cld-day {
            height: 150px;
            border-right: none;
        }

        .cld-main a {
            display: table;
            width: 100%;
            margin-bottom: 2px;
            font-size: 12px;
            word-break: break-word;
        }

        .cld-number.eventday {
            height: 100%;
        }

        a:focus {
            outline: none;
        }

        .required_star {
            color: red;
        }

        .disabled {
            pointer-events: none;
        }

        input {
            height: 50px !important;
        }

        .activeSM {
            background-color: white !important; /* #38283c !important;*/
        }

        .container {
            margin-top: 15px;
        }

        .tab-group {
            position: relative;
            vertical-align: middle;
            zoom: 1; /* Fix for IE7 */
            *display: inline; /* Fix for IE7 */
        }

        .tab-group > li {
            background-color: #eeeeee;
            border-radius: 4px;
            position: relative;
            float: left;
        }

        /*.nav > li.active
        {
            border-left: 4px solid #337ab7;
            background: #337ab7;
        }
        .tab-group > li.active > a,
        .tab-group > li.active > a:hover,
        .tab-group > li.active > a:focus
        {
            background-color: #337ab7;
            color: #fff;
        }*/
        .tab-group > li > a {
            color: #337ab7;
            border-radius: 0;
            text-align: center;
        }

        .tab-group > li > a:hover {
            border-radius: 4px;
        }

        .tab-group li + li {
            margin-left: -1px;
        }

        .tab-group > li:not(:first-child):not(:last-child),
        .tab-group > li:not(:first-child):not(:last-child) > a:hover {
            border-radius: 0;
        }

        .tab-group > li:first-child,
        .tab-group > li:first-child > a:hover {
            margin-left: 0;
        }

        .tab-group > li:first-child:not(:last-child),
        .tab-group > li:first-child:not(:last-child) > a:hover {
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }

        .tab-group > li:last-child:not(:first-child),
        .tab-group > li:last-child:not(:first-child) > a:hover {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
    </style>
@endsection

@section( 'extra_js' )
    <script type="text/javascript" src="{{ asset ( 'js/jquery.serialize-object.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/moment.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/plus-calendar.js' ) }}"></script>

    <script type="text/javascript" src="{{ asset ( 'js/fullcalendar.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/app.calendar.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/plus-plan.js' ) }}"></script>

    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script type="text/javascript">
        var formData;
        var dateArr = [];
        var dateslotsArray = [];
        var recepient_form_status = 0;
        var calendar_form_status = 0;
        var preferences_form_status = 0;
        var signin;
        var organizer_id = localStorage.organizer_id;
        var tain_id = '';

        $ ( document ).ready ( function () {
            scrollToTop ();

            signin = localStorage.UserSession;

            if ( organizer_id ) {
                //Do Nothing
            } else {
                organizer_id = "";
            }

            changeColors ( 'Recepient' );

            $ ( document ).on ( "click", ".cld-day, .cld-number, .span-wrap", function ( e ) {
                if ( e.target !== e.currentTarget ) return;
                var temp = $ ( this ).parent ().hasClass ( 'cld-number' ) ? $ ( this ).parent ().attr ( 'id' ) : this.id;
                let arr = temp.split ( '-' );
                if ( arr[ 2 ] < 10 ) arr[ 2 ] = '0' + arr[ 2 ];
                var result = compareDates ( arr.join ( '-' ) );
                if ( result ) {
                    var color = $ ( '#' + arr.join ( '-' ) ).css ( "background-color" );
                    _modal ( arr.join ( '-' ) );
                } else {
                    swal ( {
                        title : "Error!",
                        text : "Sorry! You can not select previous date.",
                        type : "error",
                        confirmButtonColor : "#DD6B55",
                    } );
                }
            } );

            $ ( "#recepient_form" ).validate ( {
                rules :
                    {
                        EventName : {
                            required : true,
                        },
                        EventEmail : {
                            required : true,
                        },
                        EventAddr1 : {
                            required : true,
                        },
                        EventCity : {
                            required : true,
                        },
                        EventState : {
                            required : true,
                        },
                        EventZip : {
                            required : true,
                        },
                        EventPhone : {
                            required : true,
                        },
                    },
                messages :
                    {},
                submitHandler : function ( form ) {
                    var EventName = $ ( '#EventName' ).val ();
                    var EventEmail = $ ( '#EventEmail' ).val ();
                    var EventAddr1 = $ ( '#EventAddr1' ).val ();
                    var EventAddr2 = $ ( '#EventAddr2' ).val ();
                    var EventCity = $ ( '#EventCity' ).val ();
                    var EventState = $ ( '#EventState' ).val ();
                    var EventZip = $ ( '#EventZip' ).val ();
                    var EventPhone = $ ( '#EventPhone' ).val ();

                    formData = new FormData ();
                    formData.append ( "url", "tecjaunt.com" );
                    formData.append ( "organizer", organizer_id );
                    formData.append ( "recipient_name", EventName );
                    formData.append ( "recipient_email", EventEmail );
                    formData.append ( "recipient_address", EventAddr1 + " " + EventAddr2 );
                    formData.append ( "recipient_city", EventCity );
                    formData.append ( "recipient_state", EventState );
                    formData.append ( "recipient_postalcode", EventZip );
                    formData.append ( "recipient_phone", EventPhone );
                    formData.append ( "story", "some story text" );

                    recepient_form_status = 1;

                    return false; // extra insurance preventing the default form action
                }
            } );

            var l = $ ( '.ladda-button-demo' ).ladda ();
            $ ( "#preferences_form" ).validate ( {
                rules : {
                    EventNumAdults : {
                        required : true,
                        number : true,
                    },
                    EventNumKids : {
                        required : true,
                        number : true,
                    },
                    EventTime : {
                        required : true,
                    },
                    EventInstructions : {
                        required : true,
                    },
                    EventLikes : {
                        required : true,
                    },
                    EventDislikes : {
                        required : true,
                    },
                    EventRestrictions : {
                        required : true,
                    },
                },
                messages : {},
                submitHandler : function ( form ) {
                    l.ladda ( 'start' );
                    var EventNumAdults = $ ( '#EventNumAdults' ).val ();
                    var EventNumKids = $ ( '#EventNumKids' ).val ();
                    var EventTime = $ ( '#EventTime' ).val ();
                    var EventInstructions = $ ( '#EventInstructions' ).val ();
                    var EventLikes = $ ( '#EventLikes' ).val ();
                    var EventDislikes = $ ( '#EventDislikes' ).val ();
                    var EventRestrictions = $ ( '#EventRestrictions' ).val ();

                    formData.append ( "adults_cook_for", EventNumAdults );
                    formData.append ( "kids_cook_for", EventNumKids );
                    formData.append ( "delivery_time", EventTime );
                    formData.append ( "special_instructions", EventInstructions );
                    formData.append ( "fave_meals_rest", EventLikes );
                    formData.append ( "least_fave_meals", EventDislikes );
                    formData.append ( "restrictions", EventRestrictions );
                    formData.append ( "package_name", "GESTR CLUB PLUS" );
                    formData.append ( "package_payment", "Plus" );
                    formData.append ( "photo", "" );
                    formData.append ( "_token", $ ( 'meta[name="csrf-token"]' ).attr ( 'content' ) );

                    //Preparing object
                    for ( let j in events ) {
                        let value = events[ j ];
                        let obj = {};
                        obj[ "dateslots" ] = j;
                        obj[ "mt_pack_id" ] = 2;
                        obj[ 'events' ] = [];
                        for ( let l in value[ 'event_data' ] ) {
                            let event_data = value[ 'event_data' ][ l ];
                            obj[ 'events' ].push ( {
                                'category' : event_data.category,
                                'needed' : event_data.needed,
                            } );
                        }
                        dateslotsArray.push ( obj );
                    }

                    formData.append ( "dateslots", JSON.stringify ( dateslotsArray ) );

                    if ( signin == "Session" ) {
                        $ ( '.allForms' ).css ( 'display', 'block' );
                        $ ( '.notUser' ).css ( 'display', 'none' );

                        $.ajax ( {
                            type : 'POST',
                            url : "checkout",
                            data : formData,
                            contentType : false,
                            cache : false,
                            processData : false,
                            success : function ( data ) {
                                if ( data.status == "success" ) {
                                    l.ladda ( 'stop' );
                                    tain_id = data.data.unique_id;
                                    swal_btn.click ();
                                } else if ( data.status == "failed" ) {
                                    l.ladda ( 'stop' );
                                    swal_btn2.click ();
                                }
                            },
                            error : function ( data ) {
                                l.ladda ( 'stop' );
                            }
                        } );
                    } else {
                        const arr = Array.from ( formData );

                        localStorage.FullFormData = JSON.stringify ( arr );

                        $ ( '.allForms' ).css ( 'display', 'none' );
                        $ ( '.notUser' ).css ( 'display', 'block' );
                    }

                    return false; // extra insurance preventing the default form action
                }
            } );

            $ ( '#swal_btn' ).click ( function () {
                swal ( {
                    title : "SUCCESS",
                    text : "Basic Plan added successfully!",
                    type : "success",
                    confirmButtonColor : "#566b8a",
                    closeOnConfirm : false
                }, function () {
                    window.location.href = window.location.origin + '/trains/' + tain_id + '/activation';
                    // window.location.assign ( "dashboard" );
                } );
            } );

            $ ( '#swal_btn2' ).click ( function () {
                swal ( {
                    title : "Operation Unsuccessfull!",
                    text : "Basic Plan not added.",
                    type : "warning",
                    confirmButtonColor : "#DD6B55",
                } );
            } );
        } );

        function clickTab ( id ) {
            scrollToTop ();
            var form_name = id;

            if ( form_name == "Recepient" ) {
                changeColors ( 'Recepient' );

                //Forms
                $ ( '#recepient_form' ).css ( 'display', 'block' );
                $ ( '#calendar_form' ).css ( 'display', 'none' );
                $ ( '#preferences_form' ).css ( 'display', 'none' );

                //Buttons
                $ ( '#btnSubmit' ).css ( 'display', 'none' );
                $ ( '#btnNext' ).css ( 'display', 'block' );
                $ ( '#btnBack' ).css ( 'display', 'none' );
            } else if ( form_name == "Dates" ) {
                recepient_form_status = 0;
                $ ( '#recepient_form' ).submit ();

                if ( recepient_form_status == 0 ) {
                    $ ( '#recepient_form' ).submit ();

                    changeColors ( 'Recepient' );
                    $ ( '#recepient_form' ).css ( 'display', 'block' );
                    $ ( '#calendar_form' ).css ( 'display', 'none' );
                    $ ( '#preferences_form' ).css ( 'display', 'none' );

                    //Buttons
                    $ ( '#btnSubmit' ).css ( 'display', 'none' );
                    $ ( '#btnNext' ).css ( 'display', 'block' );
                    $ ( '#btnBack' ).css ( 'display', 'none' );
                } else {
                    changeColors ( 'Dates' );

                    //Forms
                    $ ( '#recepient_form' ).css ( 'display', 'none' );
                    $ ( '#calendar_form' ).css ( 'display', 'block' );
                    $ ( '#preferences_form' ).css ( 'display', 'none' );

                    //Buttons
                    $ ( '#btnSubmit' ).css ( 'display', 'none' );
                    $ ( '#btnNext' ).css ( 'display', 'block' );
                    $ ( '#btnBack' ).css ( 'display', 'block' );
                }
            } else {
                recepient_form_status = 0;
                $ ( '#recepient_form' ).submit ();

                calendar_form_status = Object.keys ( events ).length;

                if ( recepient_form_status == 0 ) {
                    $ ( '#recepient_form' ).submit ();

                    changeColors ( 'Recepient' );

                    //Forms
                    $ ( '#recepient_form' ).css ( 'display', 'block' );
                    $ ( '#calendar_form' ).css ( 'display', 'none' );
                    $ ( '#preferences_form' ).css ( 'display', 'none' );

                    //Buttons
                    $ ( '#btnSubmit' ).css ( 'display', 'none' );
                    $ ( '#btnNext' ).css ( 'display', 'block' );
                    $ ( '#btnBack' ).css ( 'display', 'none' );
                } else if ( calendar_form_status == 0 ) {
                    swal ( {
                        title : "Error!",
                        text : "Please choose Dates First.",
                        type : "error",
                        confirmButtonColor : "#DD6B55",
                    } );
                    changeColors ( 'Dates' );

                    //Forms
                    $ ( '#recepient_form' ).css ( 'display', 'none' );
                    $ ( '#calendar_form' ).css ( 'display', 'block' );
                    $ ( '#preferences_form' ).css ( 'display', 'none' );

                    //Buttons
                    $ ( '#btnSubmit' ).css ( 'display', 'none' );
                    $ ( '#btnNext' ).css ( 'display', 'block' );
                    $ ( '#btnBack' ).css ( 'display', 'block' );
                } else {
                    changeColors ( 'Preferences' );

                    //Forms
                    $ ( '#recepient_form' ).css ( 'display', 'none' );
                    $ ( '#calendar_form' ).css ( 'display', 'none' );
                    $ ( '#preferences_form' ).css ( 'display', 'block' );

                    //Buttons
                    $ ( '#btnSubmit' ).css ( 'display', 'block' );
                    $ ( '#btnNext' ).css ( 'display', 'none' );
                    $ ( '#btnBack' ).css ( 'display', 'block' );
                }
            }
        }

        function scrollToTop () {
            $ ( "html, body" ).animate ( { scrollTop : 0 }, "slow" );
        }

        function buttonClicks ( id ) {
            if ( id == "NextStep" ) {
                var styledisplay = $ ( '#btnBack' ).css ( 'display' );
                if ( styledisplay == "none" ) {
                    clickTab ( 'Dates' );
                } else {
                    clickTab ( 'Preferences' );
                }
            } else if ( id == "Back" ) {
                var styledisplay = $ ( '#btnNext' ).css ( 'display' );
                if ( styledisplay == "block" ) {
                    clickTab ( 'Recepient' );
                } else {
                    clickTab ( 'Dates' );
                }
            } else {
                scrollToTop ();
                $ ( '#preferences_form' ).submit ();
            }
        }

        function changeColors ( id ) {
            $ ( '.tab-group li' ).css ( 'color', '#337ab7' );
            $ ( '.tab-group li' ).css ( 'background-color', '#eeeeee' );
            $ ( '.tab-group li a' ).css ( 'background-color', '#eeeeee' );
            $ ( '.tab-group li a' ).css ( 'color', '#337ab7' );
            $ ( '.tab-group li' ).css ( 'border-left', '4px solid #eeeeee' );

            $ ( '#' + id ).css ( {
                'background-color' : '#337ab7',
                'color' : 'white',
                'border-left' : '4px solid #337ab7',
            } );

            $ ( '#' + id + ' a' ).css ( {
                'background-color' : '#337ab7',
                'color' : 'white',
            } );
        }

        function compareDates ( dateObj ) {
            dateObj += " 23:59:59";

            var g1 = new Date ();
            var g2 = new Date(dateObj.replace(/-/g, "/"));
            var result;

            if ( g1.getTime () < g2.getTime () ) {
                result = 1;
            } else if ( g1.getTime () > g2.getTime () ) {
                result = 0;
            }

            return result;
        }

    </script>
@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('Plus Plan Page') }}</x-slot>
    <x-slot name="headerContent"></x-slot>

    <!-- CONTENT -->
    <!-- MultiStep Form -->
    <section class="item content" style="margin-top: 100px;">
        <div class="container">
            <div class="panel panel-default">
                <div class="row notUser" style="display:none;">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h1 class="text-center">Almost Done!</h1>
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <div class="alert alert-info text-left">As the organizer, you have the ability to make
                                                                        changes to this Event.<br>Please sign in to
                                                                        identify yourself as the organizer.
                                </div>
                                <div><a class="btn btn-lg btn-primary btn-block" href="{{ route( 'login' ) }}">
                                        Sign In</a>
                                </div>
                                <div class="horizontal-divider">OR</div>
                                <div class="pad-bottom">Don't have an account?<br><a
                                            class="btn btn-lg btn-primary btn-block" href="{{ route ( 'register' ) }}">
                                        Create a free account</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body allForms">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-content">
                                    <div class="tabbable">
                                        <ul class="nav tab-group"><!--nav-tabs nav-justified-->
                                            <li class="TAB col-sm-4 col-xs-12 active" onclick="clickTab(this.id)"
                                                id="Recepient">
                                                <a data-toggle="tab"><strong>1</strong> Enter Recepient</a>
                                            </li>
                                            <li class="TAB col-sm-4 col-xs-12" onclick="clickTab(this.id)" id="Dates">
                                                <a data-toggle="tab"><strong>2</strong> Select Dates</a>
                                            </li>
                                            <li class="TAB col-sm-4 col-xs-12" onclick="clickTab(this.id)"
                                                id="Preferences">
                                                <a data-toggle="tab"><strong>3</strong> Add Preferences</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Recepient details Form -->
                    <form id="recepient_form" style="display: block" class="form-horizontal" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-9">
                                <h3>This Gestr page is for:</h3>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventName" class="col-sm-2 control-label">Name<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventName" name="EventName"
                                       placeholder="Recipient name" required="">
                                <small>Example: The Smith Family</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventEmail" class="col-sm-2 control-label">Email<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="EventEmail" name="EventEmail"
                                       placeholder="Recipient email address" required="">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-8">
                                <small>
                                    <a id="btnWhy" style="text-decoration:underline;" href="#" tabindex="9999"
                                       data-toggle="modal" data-target="#myModal">
                                        Why do we ask for this information?</a>
                                </small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-8">
                                <hr>
                                <div class="alert alert-info"><small><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Enter
                                                                                                     the address where
                                                                                                     meals are to be
                                                                                                     dropped
                                                                                                     off.</small></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventAddr1" class="col-sm-2 control-label">Address<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventAddr1" name="EventAddr1"
                                       placeholder="Omit for more privacy" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-8">
                                <input type="text" class="form-control" id="EventAddr2" name="EventAddr2">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventCity" class="col-sm-2 control-label">City<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventCity" name="EventCity" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventState" class="col-sm-2 control-label">State/Prov.<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="EventState" name="EventState" required="">
                            </div>
                            <label for="EventZip" class="col-sm-2 control-label">Postal Code<span class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="EventZip" name="EventZip" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventPhone" class="col-sm-2 control-label">Phone<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventPhone" name="EventPhone" required="">
                            </div>
                        </div>
                    </form>

                    <!-- Calendar Dates Form -->
                    <form id="calendar_form" style="display: none" class="form-horizontal" autocomplete="off">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-calendar"></i>&nbsp;&nbsp;Click all the days when help is needed
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="halfpad-bottom">
                                    <button id="btnAddEvent" class="btn btn-primary" type="button">
                                        <i class="fa fa-plus"></i>&nbsp;Add to Calendar
                                    </button>
                                </div>
                                <div id="caleandar"></div>
                            </div>
                        </div>
                    </form>

                    <!-- Preferences Form -->
                    <form id="preferences_form" style="display: none ; margin-top: 20px" class="form-horizontal"
                          autocomplete="off">
                        <div class="form-group">
                            <label for="EventName" class="col-sm-2 control-label"># Adults to cook for<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="EventNumAdults" name="EventNumAdults">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventEmail" class="col-sm-2 control-label"># Kids to cook for<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="EventNumKids" name="EventNumKids">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventAddr1" class="col-sm-2 control-label">Preferred delivery time<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="EventTime" name="EventTime" maxlength="100">
                                <small>Example: from 5PM - 6PM</small>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-8">
                                <div class="alert alert-info">
                                    <small>
                                        <i class="fa fa-comment-o"></i>
                                        &nbsp;&nbsp;
                                        Add special instructions, dietary preferences, or anything else you think the
                                        event participants may need to know.
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventCity" class="col-sm-2 control-label">Special instructions<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventInstructions" name="EventInstructions"
                                          rows="5"></textarea>
                                <small>List any dropoff, delivery, or other instructions</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventLikes" class="col-sm-2 control-label">Favorite meals/<span
                                        class="hidden-xs"> </span>Restaurants<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventLikes" name="EventLikes" rows="5"></textarea>
                                <small>Examples: lasagna, chili, etc</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventDislikes" class="col-sm-2 control-label">Least Favorite meals<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventDislikes" name="EventDislikes"
                                          rows="5"></textarea>
                                <small>Example: anchovies, broccoli, etc</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventRestrictions" class="col-sm-2 control-label">Allergies or dietary
                                                                                          restrictions<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventRestrictions" name="EventRestrictions"
                                          rows="5"></textarea>
                                <small>Example: allergic to shellfish, vegan, gluten-free, etc</small>
                            </div>
                        </div>
                    </form>

                    <hr>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <div>
                                <button onclick="buttonClicks('Submit')" type="button" id="btnSubmit"
                                        style="display: none"
                                        class="ladda-button ladda-button-demo btn btn-lg btn-primary btn-responsive-block pull-right"
                                        data-loading-text="Saving..." data-style="zoom-in">
                                <span class="ladda-label">Submit&nbsp;
                                <i class="fa fa-caret-right"></i></span>
                                    <span class="ladda-spinner"></span>
                                </button>
                                <button onclick="buttonClicks('NextStep')" type="button" id="btnNext"
                                        style="display: block"
                                        class="btn btn-lg btn-primary btn-responsive-block pull-right">Next Step&nbsp;
                                    <i class="fa fa-caret-right"></i>
                                </button>
                                <button onclick="buttonClicks('Back')" type="button" id="btnBack"
                                        style="display: none;margin-right: 10px;"
                                        class="btn btn-lg btn-default btn-responsive-block pull-right">
                                    <i class="fa fa-caret-left"></i>&nbsp;Back
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Information Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Recipient Information</h4>
                </div>
                <div class="modal-body">
                    <h2>Why we ask for this information:</h2>
                    <hr>
                    <div class="row pad-bottom">
                        <div class="col-sm-4"><strong>Recipient Email Address:</strong></div>
                        <div class="col-sm-6"><p>Used to invite the recipient to view/edit the Gestr page, and used
                                                 for email notifications of date booking or cancellation. Recipient
                                                 email is not displayed on the Gestr page.</p></div>
                    </div>
                    <div class="row pad-bottom">
                        <div class="col-sm-4"><strong>Recipient Physical Address:</strong></div>
                        <div class="col-sm-6"><p>Optional: Used to inform participants where to drop off meals.</p>
                        </div>
                    </div>
                    <div class="row pad-bottom">
                        <div class="col-sm-4"><strong>Recipient Phone Number:</strong></div>
                        <div class="col-sm-6"><p>Optional: Helpful for participants who may have last minute
                                                 questions.</p></div>
                    </div>
                    <p class="pad-bottom">We take privacy very seriously and do not share or sell personal information.
                                          You can find our privacy policy at the bottom of every page.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pg-create-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="createForm" class="form-horizontal">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Add to Calendar</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info">
                            <strong><i class="fa fa-info-circle"></i>&nbsp;&nbsp;</strong>
                            Describe what is needed
                        </div>
                        <div class="form-group">
                            <label for="Category" class="col-sm-12">Category<span class="required_star">*</span></label>
                            <div class="col-sm-12">
                                <select class="form-control category-dropdown" id="category" name="category" required>
                                    <option value="" selected>Select Category</option>
                                    <option value="1">Meal</option>
                                    <option value="2">Groceries</option>
                                    <option value="3">Help</option>
                                    <option value="4">Visit</option>
                                    <option value="5">Ride</option>
                                    <option value="6">Child Care</option>
                                    <option value="7">Communication</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Needed" class="col-sm-12">Needed<span class="required_star">*</span></label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="needed" name="needed" required/>
                                <small><span class="category-example">Example: Dinner</span></small>
                            </div>
                        </div>

                        <div class="alert alert-info">
                            <strong><i class="fa fa-info-circle"></i>&nbsp;&nbsp;</strong>
                            Select the date(s) when this is needed
                        </div>
                        <div class="row" style="padding-bottom:20px;">
                            <div class="col-sm-12">
                                <div class="pull-left">
                                    <a id="btnCalPrev" class="btn btn-default btn-sm" href="#">
                                        <i class="fa fa-caret-left"></i>&nbsp;Prev Month
                                    </a>
                                </div>
                                <div class="pull-right">
                                    <a id="btnCalNext" class="btn btn-default btn-sm" href="#">Next Month&nbsp;
                                        <i class="fa fa-caret-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div id="calendarCurrent" class="mt-calendar"></div>
                            </div>
                            <div class="col-sm-6">
                                <div id="calendarNext" class="mt-calendar"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnCreateEvent" type="submit" data-loading-text="Saving..." class="btn btn-primary">
                            <i class="fa fa-check"></i>&nbsp;Save Changes
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pg-update-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="updateForm" class="form-horizontal" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Update Calendar</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="editDate" class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-10">
                                <p id="editDate" class="form-control-static"></p>
                                <input type="hidden" name="editID" id="editId"/>
                                <input type="hidden" name="editIndex" id="editIndex"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editCategoryId" class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-10">
                                <select class="form-control category-dropdown" id="editCategory" name="editCategory"
                                        required>
                                    <option value="" selected>Select Category</option>
                                    <option value="1">Meal</option>
                                    <option value="2">Groceries</option>
                                    <option value="3">Help</option>
                                    <option value="4">Visit</option>
                                    <option value="5">Ride</option>
                                    <option value="6">Child Care</option>
                                    <option value="7">Communication</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editTitle" class="col-sm-2 control-label">Needed</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="editNeed" name="editNeed" value=""
                                       maxlength="255" required="">
                                <small><span class="category-example">Example: Dinner</span></small>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="btnUpdateEvent" type="submit"
                                class="btn btn-primary btn-responsive-block pull-right">
                            <i class="fa fa-check"></i>&nbsp;Save Changes
                        </button>

                        <button id="btnDeleteEvent" type="button"
                                class="btn  btn-default btn-responsive-block pull-right">
                            <i class="fa fa-trash"></i>&nbsp;Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Successfully Registered Employee -->
    <button id="swal_btn" type="button" style="display:none"></button>
    <!-- Employee not  Register -->
    <button id="swal_btn2" type="button" style="display:none"></button>

</x-app-layout>