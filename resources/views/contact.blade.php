@section('extra_css')
    <link href="{{ asset ( 'css/aboutstyle.css' ) }}" rel="stylesheet"/>
@endsection

@section( 'extra_js' )
    <script src="{{ asset ( 'js/validate.js' ) }}"></script>
@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('Contact') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.0s">
                    CONTACT US
                </div>
            </div>
        </div>
    </x-slot>

    <!-- CONTENT -->
    <section class="item content">
        <div class="container toparea toparea-contact">
            <div class="underlined-title">
                <div class="editContent">
                    <h1 class="text-center latestitems">Get in Touch</h1>
                </div>
                <div class="wow-hr type_short">
                    <span class="wow-hr-h">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="done">
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            Your message has been sent. Thank you!
                        </div>
                    </div>
                    <form method="post" action="{{ route ( 'contact.store' ) }}" id="contactform">
                        <div class="form">
                            <input type="text" name="name" placeholder="Your Name *">
                            <input type="text" name="email" placeholder="Your E-mail Address *">
                            <textarea name="comment" rows="7" placeholder="Type your Message *"></textarea>
                            <input type="submit" id="submit" class="clearfix btn" value="Send">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

</x-app-layout>