@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.fileupload.css' ) }}"/>
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <style>
        .mt {
            margin-top: 20px;
        }

        .h-tight {
            margin-bottom: 0;
            margin-top: 0;
            color: #00BCE4;
            font-weight: 400;
            line-height: 1.1;
        }

        .h-strong {
            color: #333;
            font-size: 14px;
        }

        .pg-photo-box {
            background-color: #C9C9C9;
            border: 0;
            border-radius: 5px;
            color: #fff;
            font-size: 20px;
            margin: 0 auto;
            overflow: hidden;
            padding: 20% 0;
            text-align: center;
            width: 80%;
        }

        .meal-image {
            border-radius: 50px;
            width: 55px;
            height: 55px;
        }
    </style>
@endsection
@section( 'extra_js' )
    <script type="text/javascript" src="{{ asset ( 'js/api.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.ui.widget.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.fileupload.js' ) }}"></script>

    <script>
        const base_image = '<img class="meal-image" src="{{ asset ( 'images/appointments-sm.png' ) }}">';
        $ ( '.btn-remove-train' ).on ( "click", function ( e ) {
            let url = "{{ url ("/trains") }}/" + $ ( this ).attr ( 'data-uniqueId' ) + "/remove";
            $ ( '.meal_name' ).text ( $ ( this ).attr ( 'data-name' ) );
            $ ( '.meal-volunteers' ).html ( $ ( '.slot-data' ).html () );
            $ ( '#train_id' ).val ( $ ( this ).attr ( 'data-id' ) );
            $ ( '#mt-form-remove' ).attr ( 'action', url );
            $ ( '#pg-modal' ).modal ( 'toggle' );
        } );

        $ ( ".add-edit-photo" ).on ( "click", function ( e ) {
            let src = $ ( this ).attr ( 'data-src' );
            if ( src != '' ) {
                $ ( "#file-upload-img" ).attr ( "src", src ).show ();
                $ ( '.pg-photo-box' ).hide ();
                $ ( ".btn-remove" ).show ();
            } else {
                $ ( "#file-upload-img" ).attr ( "src", "" ).hide ();
                $ ( '.pg-photo-box' ).show ();
                $ ( ".btn-remove" ).hide ();
            }
            $ ( '#add-edit-photo-modal' ).modal ( 'toggle' );
            let val = $ ( this ).attr ( 'data-id' );
            $ ( '#meal_id' ).val ( val );
        } );

        $ ( "#fileupload" ).fileupload ( {
            url : window.location.origin,
            dataType : "json",
            autoUpload : false,
            maxNumberOfFiles : 1,
            formData : { _token : $ ( 'meta[name="csrf-token"]' ).attr ( 'content' ) },
            done : function ( e, data ) {
                $ ( ".progress.fileupload-progress, .pg-photo-box" ).hide ();
                $ ( "#file-upload-img" ).attr ( "src", data.result.data ).show ();
                $ ( ".btn-remove" ).show ();
                fileUploadDone ( data.result.data );
                let val = $ ( "#meal_id" ).val ();
                $ ( "[data-id='" + val + "']" ).attr ( 'data-src', data.result.data );
                renderImageOnListing ( "<img class='meal-image' src='" + data.result.data + "'>" );
            },
            add : function ( e, data ) {
                data.url = window.location.origin + '/trains/' + $ ( '#meal_id' ).val ().trim () + '/upload';
                let elem = $ ( "#file-upload-status" );
                elem.hide ();
                if ( data.files && data.files[ 0 ] && data.files[ 0 ].size && data.files[ 0 ].size > 8388608 ) {
                    let message = "File size limit is 8 MB.Please reduce the size of the image and try again.";
                    elem.removeClass ( "hidden alert-success alert-danger" )
                        .html ( message || "The action did not complete. Please try again." ).addClass ( "alert-danger" ).fadeIn ( "fast" );
                } else {
                    console.log ( data );
                    data.submit ();
                }
            },
            fail : function ( e, data ) {
                console.log ( data )
                $ ( ".progress.fileupload-progress" ).hide ();
                let message = data.jqXHR.responseJSON.messages;
                $ ( "#file-upload-status" ).removeClass ( "hidden alert-success alert-danger" )
                    .html ( message || "The action did not complete. Please try again." ).addClass ( "alert-danger" ).fadeIn ( "fast" );
            },
            progressall : function ( e, data ) {
                var progress = parseInt ( data.loaded / data.total * 100, 10 );
                $ ( ".progress.fileupload-progress .progress-bar" ).css ( "width", progress + "%" );
            },
            start : function ( e, data ) {
                $ ( ".progress.fileupload-progress .progress-bar" ).css ( "width", "0%" );
                $ ( ".progress.fileupload-progress" ).show ();
            }
        } ).prop ( 'disabled', ! $.support.fileInput ).parent ().addClass ( $.support.fileInput ? undefined : 'disabled' );

        $ ( ".btn-remove" ).click ( function ( e ) {
            e.preventDefault ();
            let val = $ ( "#meal_id" ).val ();
            let url = window.location.origin + '/trains/' + val.trim ();
            api_call ( url + '/remove_photo', 'post', new FormData () ).then ( ( response ) => {
                $ ( "#file-upload-img" ).attr ( "src", "" ).hide ();
                $ ( ".pg-photo-box" ).show ();
                $ ( this ).hide ();
                renderImageOnListing ( base_image );
                fileUploadDone ( '' );
            } ).catch ( err => {
                console.log ( err )
            } );
        } );

        $ ( ".manage-participants" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( '#participantSelectedId' ).val ( $ ( this ).attr ( 'data-id' ) );
            let url = window.location.origin;
            // $ ( '#participants-modal' ).modal ( 'toggle' );
            participant_other_url = url + '/trains/' + $ ( "#participantSelectedId" ).val () + '/participant_message?type=others';
            participant_all_url = url + '/trains/' + $ ( "#participantSelectedId" ).val () + '/participant_message?type=all';
            $("#participant_invite_others").attr("href", participant_other_url);
            $("#participant_email_all").attr("href", participant_all_url);
            url = url + '/trains/' + $ ( "#participantSelectedId" ).val () + '/participants';

            fetchParticipants ( url, true );
        } );

        function fetchParticipants ( url, modal_toggle = false ) {
            api_call ( url, 'get' ).then ( ( response ) => {
                let elem = $ ( "#participants_listing" );
                elem.html ( "" );
                const data = response.data;
                // <a href="#" data-id="lmken16" class="btn btn-participants-remove btn-danger btn-sm"><i class="fa fa-trash" alt="Remove" title="Remove"></i></a>
                $.each ( data, function ( i, item ) {
                    let div =   '<div class="row">' + 
                                    '<div class="col-sm-4">' + item.name + '</div>' +
                                    '<div class="col-sm-4">' + item.email + '</div>' +
                                    '<div class="col-sm-2">' + item.actions + '</div>' +
                                '</div>' +
                                '<hr>';
                    elem.append ( div );
                } );
                setTimeout ( x => checkLimit (), 50 );
                if ( modal_toggle ) $ ( '#participants-modal' ).modal ( 'toggle' );
            } ).catch ( err => {
                console.log ( err )
            } );
        }

        $ ( ".manage-organizers" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( '#selectedId' ).val ( $ ( this ).attr ( 'data-id' ) );
            let url = window.location.origin;
            url = url + '/trains/' + $ ( "#selectedId" ).val () + '/organizers';
            fetchOrganizers ( url, true );
        } );

        function fetchOrganizers ( url, modal_toggle = false ) {
            api_call ( url, 'get' ).then ( ( response ) => {
                let elem = $ ( "#organizers-data tbody" );
                elem.html ( "" );
                const data = response.data;
                $.each ( data, function ( i, item ) {
                    let tr = "<tr>" +
                        "<td>" + item.name + "</td>" +
                        "<td>" + item.email + "</td>" +
                        "<td><strong>" + item.type + "</strong></td>" +
                        "<td>" + item.actions + "</td>" +
                        "</tr>";
                    elem.append ( tr );
                } );
                setTimeout ( x => checkLimit (), 50 );
                if ( modal_toggle ) $ ( '#organizers-modal' ).modal ( 'toggle' );
            } ).catch ( err => {
                console.log ( err )
            } );
        }

        function checkLimit () {
            if ( $ ( "#organizers-data tbody tr" ).length >= 5 ) {
                $ ( '#btnAdd' ).addClass ( 'disabled' ).prop ( 'disabled', true );
                $ ( "#organizer_email" ).prop ( 'disabled', true );
                $ ( '.og-info' ).text ( "Max limit to create organinzers reached for this plan." );
            } else {
                $ ( '#btnAdd' ).removeClass ( 'disabled' ).prop ( 'disabled', false );
                $ ( "#organizer_email" ).prop ( 'disabled', false );
                $ ( '.og-info' ).text ( "Adding organizers allows others to make edits, post updates, and receive notifications." );
            }
            setTimeout ( function () {
                $ ( '#organizer_email' ).removeClass ( 'error' );
                $ ( '#organizer_email-error' ).remove ();
            }, 50 );
        }

        function fileUploadDone ( path ) {
            $ ( "#pg-train-photo > img" ).attr ( "src", path );
            if ( path == null || path == "" ) {
                $ ( "#pg-train-photo" ).addClass ( 'hide' );
                $ ( ".btn-photo-box" ).removeClass ( 'hide' );
            } else {
                $ ( ".btn-photo-box" ).addClass ( 'hide' );
                $ ( "#pg-train-photo" ).removeClass ( 'hide' );
            }
        }

        $ ( "#organizers_form" ).validate ( {
            rules : {
                organizer_email : {
                    required : true,
                    email : true
                }
            },
            errorPlacement : function ( error, element ) {
                error.insertAfter ( $ ( element ).parent () );
            },
            submitHandler : function ( form ) {
                $ ( '#status' ).hide ();
                let l = $ ( "#btnAdd" ).ladda ();
                l.ladda ( 'start' );
                let url = window.location.origin;
                url = url + '/trains/' + $ ( "#selectedId" ).val () + '/organizers/create';
                const formData = new FormData ();
                formData.append ( 'organizer_email', $ ( "#organizer_email" ).val () );
                api_call ( url, 'post', formData ).then ( ( response ) => {
                    if ( response.status === 'success' ) {
                        let url = window.location.origin;
                        url = url + '/trains/' + $ ( "#selectedId" ).val () + '/organizers';
                        fetchOrganizers ( url );
                        form.reset ();
                    }
                    if ( response.status === 'error' ) $ ( '#status' ).show ();
                    l.ladda ( 'stop' );
                } ).catch ( err => {
                    l.ladda ( 'stop' );
                    console.log ( err )
                } );
                return false;
            }
        } );

        $ ( document ).on ( "click", ".btn-participants-remove", function ( e ) {
            e.preventDefault ();
            let l = $ ( this ).ladda ();
            l.ladda ( 'start' );
            let url = window.location.origin;
            url = url + '/trains/' + $ ( "#participantSelectedId" ).val () + '/participants/remove';
            const formData = new FormData ();
            formData.append ( 'participant_id', $ ( this ).attr ( 'data-id' ) );
            api_call ( url, 'post', formData ).then ( ( response ) => {
                if ( response.status === 'success' ) {
                    let url = window.location.origin;
                    url = url + '/trains/' + $ ( "#participantSelectedId" ).val () + '/participants';
                    fetchParticipants ( url );
                }
                // l.ladda ( 'stop' );
            } ).catch ( err => {
                // l.ladda ( 'stop' );
                console.log ( err )
            } );
        } );

        $ ( document ).on ( "click", ".remove-organizer", function ( e ) {
            e.preventDefault ();
            let l = $ ( this ).ladda ();
            l.ladda ( 'start' );
            let url = window.location.origin;
            url = url + '/trains/' + $ ( "#selectedId" ).val () + '/organizers/remove';
            const formData = new FormData ();
            formData.append ( 'organizer_id', $ ( this ).attr ( 'data-id' ) );
            api_call ( url, 'post', formData ).then ( ( response ) => {
                if ( response.status === 'success' ) {
                    let url = window.location.origin;
                    url = url + '/trains/' + $ ( "#selectedId" ).val () + '/organizers';
                    fetchOrganizers ( url );
                }
                // l.ladda ( 'stop' );
            } ).catch ( err => {
                // l.ladda ( 'stop' );
                console.log ( err )
            } );
        } );

        $ ( document ).on ( "click", ".make-primary", function ( e ) {
            e.preventDefault ();
            let l = $ ( this ).ladda ();
            l.ladda ( 'start' );
            let url = window.location.origin;
            url = url + '/trains/' + $ ( "#selectedId" ).val () + '/organizers/make_primary';
            const formData = new FormData ();
            formData.append ( 'organizer_id', $ ( this ).attr ( 'data-id' ) );
            api_call ( url, 'post', formData ).then ( ( response ) => {
                if ( response.status === 'success' ) {
                    let url = window.location.origin;
                    url = url + '/trains/' + $ ( "#selectedId" ).val () + '/organizers';
                    fetchOrganizers ( url );
                }
                // l.ladda ( 'stop' );
            } ).catch ( err => {
                // l.ladda ( 'stop' );
                console.log ( err )
            } );
        } );

        function renderImageOnListing ( src ) {
            let val = $ ( "#meal_id" ).val ();
            let child = $ ( "[data-id='" + val + "']" ).parent ().parent ().children ()[ 0 ];
            $ ( child ).find ( 'img' ).remove ();
            $ ( child ).append ( src );
        }

    </script>
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __('Dashboard') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    Dashboard
                </div>
            </div>
        </div>
    </x-slot>


    <div class="container-fluid">
        <div class="col-sm-offset-1 col-sm-10 mt">
            @if( $meals -> count () == 0 )
                <div class="row noEvents">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="alert alert-info">
                                    Below is a list of all of the events you have created or have been invited to
                                    participate in.
                                </div>
                                <div>
                                    <h3 class="h-tight" style="color: #00BCE4">
                                        You are not currently a participant in any Gestr pages.</h3>
                                    <br>
                                    <p>To organize a new Event page, click&nbsp;&nbsp;
                                        <a class="btn btn-primary btn-sm" href="{{ route ( 'member' ) }}">
                                            Get Started</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if( $meals -> count () > 0 )
                <div class="row Events">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="alert alert-info">
                                    Below is a list of all of the events you have created or have been invited to
                                    participate in.
                                </div>
                                @foreach( $meals as $meal )
                                    <div class="row pg-entitylist-row">
                                        <div class="col-sm-2 col-xs-3 text-center">
                                            @if( $meal -> attachment == null )
                                                <img class="meal-image"
                                                     src="{{ asset ( 'images/appointments-sm.png' ) }}" alt="" title="">
                                            @else
                                                <img class="meal-image"
                                                     src="{{ asset ( "uploads/{$meal -> attachment -> url}" ) }}">
                                            @endif
                                        </div>
                                        <div class="col-sm-5 col-xs-9 pg-col-name">
                                            <strong class="h-strong">Gestr for:</strong>
                                            <a href="{{ url ( "trains/{$meal -> unique_id}" ) }}">
                                                <h4 class="h-tight">{{ $meal -> recipient_name }}</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-5 col-sm-offset-0 col-xs-offset-3 col-xs-9"
                                             style="margin-top: 1rem;">
                                            <a title="Open" class="btn btn-primary"
                                               href="{{ url ( "trains/{$meal -> unique_id}" ) }}">
                                                <i class="fa fa-search"></i>
                                            </a>&nbsp;
                                            @if( $user -> id == $meal -> user_id || $meal->users->where('id', $user -> id) )
                                                <a title="Edit GESTR" class="btn btn-default"
                                                   href="{{ url ( "trains/{$meal -> unique_id}/edit" ) }}">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a title="Edit Calendar" class="btn btn-default"
                                                   href="{{ url ( "trains/{$meal -> unique_id}/edit_calendar" ) }}">
                                                    <i class="fa fa-calendar"></i>
                                                </a>
                                                <a title="Add/Edit Main Photo" class="btn btn-default add-edit-photo"
                                                   data-id="{{ $meal -> unique_id }}" href="javascript:;"
                                                   data-src="@if( $meal -> attachment != null ) {{ asset ( "uploads/{$meal -> attachment -> url}" ) }} @endif">
                                                    <i class="fa fa-photo"></i>
                                                </a>
                                                @if( $meal -> package_payment === "Plus" )
                                                    <a title="Manage Organizers"
                                                       class="btn btn-default manage-organizers"
                                                       data-id="{{ $meal -> unique_id }}"
                                                       href="javascript:;">
                                                        <i class="fa fa-user"></i>
                                                    </a>
                                                @endif
                                                <a title="Manage Participants"
                                                   class="btn btn-default manage-participants"
                                                   data-id="{{ $meal -> unique_id }}"
                                                   href="javascript:;">
                                                    <i class="fa fa-group"></i>
                                                </a>
                                                <a title="Delete" class="btn btn-default"
                                                   href="javascript:;">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            @else
                                                <a href="javascript:;" title="Remove Me"
                                                   data-id="{{ $meal -> id }}"
                                                   data-uniqueId="{{ $meal -> unique_id }}"
                                                   data-name="{{ $meal -> recipient_name }}"
                                                   class="btn btn-default btn-remove-train">
                                                    <i class="fa fa-trash"></i><span class="">&nbsp;Remove
                                                        <span class="hidden-xs">Me</span></span>
                                                </a>
                                            @endif&nbsp;
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="modal fade" id="add-edit-photo-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                    <h4 class="modal-title">Add a Photo</h4>
                </div>
                <div class="modal-body">
                    <form id="mt-form-photo" action="" method="post">
                        <input type="hidden" id="meal_id">
                        <div class="text-center">
                            <div class="pg-photo-box"><i class="fa fa-photo fa-2x"></i></div>
                            <img id="file-upload-img" style="display:none;"/>
                            <p>
                            <div class="btn btn-success btn-responsive-block fileinput-button">
                                <i class="fa fa-plus"></i>&nbsp;Select new photo...
                                <input id="fileupload" type="file" name="files" accept="image/*"/>
                            </div>
                            <a class="btn btn-default btn-responsive-block btn-remove" style="display:none;"
                               href="javascript:;" data-loading-text="Removing...">
                                <i class="fa fa-trash"></i>&nbsp;Remove photo</a>
                            </p>
                            <div class="progress fileupload-progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                     aria-valuemax="100" style="width:0%;"></div>
                            </div>
                            <div id="file-upload-status" class="alert hidden"></div>
                        </div>
                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="pg-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="mt-form-remove" action="" method="post">
                    @csrf
                    <input type="hidden" name="id" id="train_id"/>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Confirm Remove</h4>
                    </div>
                    <div class="modal-body">
                        <br>
                        <p>
                            <strong>You are removing yourself from the Gestr for <span class="meal_name"></span>
                            </strong></p>
                        <div class="meal-volunteers"></div>
                        <div class="form-group">
                            <textarea id="Message" class="form-control" name="Message" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Remove Me</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="participants-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <input type="hidden" id="participantSelectedId"/>
                <form id="mt-form-participants" class="form-horizontal" autocomplete="off">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Participants</h4>
                    </div>
                    <div class="modal-body" style="max-height:400px; overflow-y:scroll;">
                        <div class="alert alert-info">
                            The list contains those who were invited from within MealTrain.com, signed up to help, or
                            made a cash donation (if active). It does not include those who sent a gift card.
                            <br><br><strong>Privacy Note:</strong> As an Organizer/Recipient, you are able to see the
                            name, email and message/delete buttons. General participants will only see the name and
                            message button.
                        </div>

                        <div id="participants_listing">
                            <div class="row">
                                <div class="col-sm-4">Zeeshan Iqbal</div>
                                <div class="col-sm-4">zeeshanbutt223@gmail.com</div>
                                <div class="col-sm-2">
                                    <a href="/trains/ye9gvl/message/k94wz92/" class="btn btn-primary btn-sm"><i
                                                class="fa fa-envelope" alt="Send email" title="Send email"></i></a>
                                    <a href="#" data-id="lmken16" class="btn btn-participants-remove btn-danger btn-sm"><i
                                                class="fa fa-trash" alt="Remove" title="Remove"></i></a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>

                        <a class="btn btn-default" id="participant_invite_others" href="/trains/ye9gvl/participant_message?type=others">
                            <i class="fa fa-envelope"></i>&nbsp;Invite Others
                        </a>

                        <a class="btn btn-default" id="participant_email_all" title="Email all" href="/trains/ye9gvl/participant_message?type=all"><i
                                    class="fa fa-comments"></i>&nbsp;Email all</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="organizers-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Manage Organizers</h4>
                </div>
                <div class="modal-body" style="max-height:400px; overflow-y:scroll;">
                    <div class="alert alert-info og-info">
                        Adding organizers allows others to make edits, post updates, and receive notifications.
                    </div>
                    <div id="status" class="alert alert-danger" style="display: none;">
                        The email entered does not match an active Gestr user account. Please have the owner of the
                        email address create a user account before continuing.
                    </div>

                    <div class="row">
                        <div class="col-md-12" style="margin-bottom: 1.5rem">
                            <form id="organizers_form" method="POST" class="form-horizontal"
                                  autocomplete="off">
                                <div class="input-group">
                                    <input type="hidden" id="selectedId"/>
                                    <input id="organizer_email" name="organizer_email" type="email" class="form-control"
                                           placeholder="Enter an email..." required/>
                                    <span class="input-group-btn">
                                        <button id="btnAdd" type="submit" class="btn btn-primary">
                                            <i class="fa fa-plus"></i>&nbsp;Add Organizer
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-condensed" id="organizers-data">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
