@section( 'extra_css' )
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.tagit.css' ) }}"/>
    <link rel="stylesheet" href="{{ asset ( 'css/jquery.fileupload.css' ) }}"/>
    <style>
        .mt-buttonbar > .mt-buttonbar-buttons {
            padding: 10px 0;
        }

        .thumbnail-sm {
            max-height: 160px;
            max-width: 160px;
        }
    </style>
@endsection

@section( 'extra_js' )
    <script type="text/javascript" src="{{ asset ( 'js/jquery.ui.widget.js' ) }}"></script>
    <script type="text/javascript" src="{{ asset ( 'js/jquery.fileupload.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/jquery.serialize-object.min.js' ) }}"></script>
    <script type="text/javascript">
        let url = window.location.href;
        $ ( "#fileupload" ).on ( 'change', function () {
            let reader = new FileReader ();
            reader.onload = function ( e ) {
                $ ( "#file-upload-img" ).attr ( "src", e.target.result ).show ();
                $ ( ".btn-remove" ).show ();
                $ ( "#remove_image" ).val ( 0 );
            };
            reader.readAsDataURL ( this.files[ 0 ] );
        } );

        $ ( ".btn-remove" ).click ( function ( e ) {
            e.preventDefault ();
            $ ( this ).hide ();
            $ ( "#file-upload-img" ).attr ( "src", "" ).hide ();
            $ ( "#fileupload" ).val ( "" );
            $ ( "#remove_image" ).val ( 1 );
        } );

        $ ( ".form-post" ).validate ( {
            rules : {
                title : {
                    required : true,
                },
                text : {
                    required : true,
                }
            },
            submitHandler : function ( form ) {
                console.log ( form )
                form.submit ();
            }
        } );

        @if( isset( $post ) && $post -> attachment != null )
        $ ( "#file-upload-img" ).attr ( "src", "{{ url ( "uploads/{$post -> attachment -> url}" ) }}" ).show ();
        $ ( ".btn-remove" ).show ();
        @endif
    </script>
@endsection
<x-app-layout>
    <x-slot name="page_title">{{ __('Setup is Complete') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                </div>
            </div>
        </div>
    </x-slot>

    <div class="container-fluid">
        <h1>Update</h1>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <div class="alert alert-info"><p><strong>Your friends want to hear from you</strong></p>
                            <p>Use the Updates tab to share updates and photos as they occur.</p>
                            <p>Start your post by adding a catchy title below.</p></div>
                    </div>
                </div>
                <form class="form-post form-horizontal" action="{{ $url }}"
                      method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Date Posted</label>
                        <div class="col-sm-8">
                            <p class="form-control-static">{{ $date }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="PostTitle" class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="title" name="title" maxlength="255" required
                                   value="{{ $post -> title ?? '' }}">
                            <input type="hidden" id="id" name="id" value="{{ $post -> id ?? '' }}"/>
                            <input type="hidden" id="remove_image" name="remove_image" value="0"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="PostText" class="col-sm-2 control-label">Text</label>
                        <div class="col-sm-8">
                            <textarea id="PostText" class="form-control" name="text" rows="15"
                                      required>{{ $post -> body ?? '' }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="PostPhoto" class="col-sm-2 control-label">Photo</label>
                        <div class="col-sm-8">
                            <div>
                                <img id="file-upload-img" class="thumbnail thumbnail-sm" style="display:none;"
                                     @if( isset( $post ) && $post -> attachment !== null )
                                     src="{{ url ( "uploads/{$post -> attachment -> url}" ) }}" @endif
                                />
                                <div id="file-upload-status" class="alert hidden"></div>
                                <div class="btn btn-primary btn-responsive-block fileinput-button"><i
                                            class="fa fa-plus"></i>&nbsp;Select new photo...
                                    <input id="fileupload" type="file" name="files" accept="image/*">
                                </div>
                                <a class="btn btn-default btn-responsive-block btn-remove" style="display:none;"
                                   href="#" data-loading-text="Removing...">
                                    <i class="fa fa-trash"></i>&nbsp;Remove photo
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div style="margin-left: 17.8%;">
                            <div class="mt-buttonbar mt-buttonbar-default margin-bottom">
                                <div class="mt-buttonbar-buttons">
                                    <a class="btn btn-default btn-back-confirm"
                                       href="{{ url ( "trains/{$id}/updates" ) }}">
                                        <i class="fa fa-caret-left"></i>&nbsp;Back</a>
                                    <button class="btn btn-success btn-submit">
                                        <i class="fa fa-check"></i>
                                        @if( isset( $post_id ) ) Save Changes @else Post Update @endif&nbsp;

                                    </button>
                                    <div class="alert hidden alert-status"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-sm-offset-2 col-sm-8">
                        <div class="alert alert-info">
                            Note: An email will be sent to all participants notifying them of the update.
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
