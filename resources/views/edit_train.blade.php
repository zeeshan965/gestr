@section('extra_css')
    <!-- Sweet Alert -->
    <link href="{{ asset ( 'css/sweetalert.css' ) }}" rel="stylesheet">
    <!-- Ladda style -->
    <link href="{{ asset ( 'css/ladda-themeless.min.css' ) }}" rel="stylesheet">
    <style type="text/css">
        a:focus {
            outline: none;
        }

        .required_star {
            color: red;
        }

        .disabled {
            pointer-events: none;
        }

        input {
            height: 50px !important;
        }

        .activeSM {
            background-color: white !important; /* #38283c !important;*/
        }

        .container {
            margin-top: 15px;
        }
    </style>
@endsection

@section( 'extra_js' )
    <!-- Sweet alert -->
    <script src="{{ asset ( 'js/sweetalert.min.js' ) }}"></script>
    <!-- Jquery Validate -->
    <script src="{{ asset ( 'js/jquery.validate.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/additional-methods.min.js' ) }}"></script>
    <!-- Ladda -->
    <script src="{{ asset ( 'js/spin.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/ladda.jquery.min.js' ) }}"></script>
    <script src="{{ asset ( 'js/api.js' ) }}"></script>
    <script type="text/javascript">
        const formData = new FormData ();
        let train_url = "{{ $url }}";
        let l = $ ( '.ladda-button-demo' ).ladda ();
        $ ( document ).ready ( function () {
            $ ( "#recepient_form" ).validate ( {
                rules : {
                    EventName : {
                        required : true,
                    },
                    EventEmail : {
                        required : true,
                    },
                    EventAddr1 : {
                        required : true,
                    },
                    EventCity : {
                        required : true,
                    },
                    EventState : {
                        required : true,
                    },
                    EventZip : {
                        required : true,
                    },
                    EventPhone : {
                        required : true,
                    },
                    EventNumAdults : {
                        required : true,
                        number : true,
                    },
                    EventNumKids : {
                        required : true,
                        number : true,
                    },
                    EventTime : {
                        required : true,
                    },
                    EventInstructions : {
                        required : true,
                    },
                    EventLikes : {
                        required : true,
                    },
                    EventDislikes : {
                        required : true,
                    },
                    EventRestrictions : {
                        required : true,
                    }
                },
                submitHandler : function ( form, event ) {
                    console.log ( form, event )
                    event.preventDefault ();
                    l.ladda ( 'start' );
                    let EventName = $ ( '#EventName' ).val ();
                    let EventEmail = $ ( '#EventEmail' ).val ();
                    let EventAddr1 = $ ( '#EventAddr1' ).val ();
                    let EventCity = $ ( '#EventCity' ).val ();
                    let EventState = $ ( '#EventState' ).val ();
                    let EventZip = $ ( '#EventZip' ).val ();
                    let EventPhone = $ ( '#EventPhone' ).val ();
                    let EventNumAdults = $ ( '#EventNumAdults' ).val ();
                    let EventNumKids = $ ( '#EventNumKids' ).val ();
                    let EventTime = $ ( '#EventTime' ).val ();
                    let EventInstructions = $ ( '#EventInstructions' ).val ();
                    let EventLikes = $ ( '#EventLikes' ).val ();
                    let EventDislikes = $ ( '#EventDislikes' ).val ();
                    let EventRestrictions = $ ( '#EventRestrictions' ).val ();
                    let story = $ ( "[name='about']" ).val ().trim ();

                    formData.append ( "recipient_name", EventName );
                    formData.append ( "recipient_email", EventEmail );
                    formData.append ( "recipient_address", EventAddr1 );
                    formData.append ( "recipient_city", EventCity );
                    formData.append ( "recipient_state", EventState );
                    formData.append ( "recipient_postalcode", EventZip );
                    formData.append ( "recipient_phone", EventPhone );
                    formData.append ( "adults_cook_for", EventNumAdults );
                    formData.append ( "kids_cook_for", EventNumKids );
                    formData.append ( "delivery_time", EventTime );
                    formData.append ( "special_instructions", EventInstructions );
                    formData.append ( "fave_meals_rest", EventLikes );
                    formData.append ( "least_fave_meals", EventDislikes );
                    formData.append ( "restrictions", EventRestrictions );
                    formData.append ( "body", story );
                    formData.append ( "_token", $ ( 'meta[name="csrf-token"]' ).attr ( 'content' ) );
                    api_call ( 'update', 'post', formData ).then ( ( response ) => {
                        console.log ( response );
                        l.ladda ( 'stop' );
                        if ( typeof ( response.status ) !== 'undefined' && response.status == 'success' ) {
                            successMessage ( response.message, train_url )
                        } else {
                            if ( response.status == 'error' ) {
                                errorMessage ( response );
                            } else {
                                showErrors ( response );
                            }
                        }
                    } ).catch ( err => {
                        l.ladda ( 'stop' );
                        console.log ( err )
                    } );
                    return false;
                }
            } );
        } );

    </script>
@endsection

<x-app-layout>
    <x-slot name="page_title">{{ __('Edit Gestr') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-pageheader">
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.1s">
                    <strong>Gestr for</strong>
                    <p>{{ $meal -> recipient_name }}</p>
                </div>
            </div>
        </div>
    </x-slot>

    <!-- CONTENT -->
    <!-- MultiStep Form -->
    <section class="item content">
        <div class="container">
            <div class="panel panel-default">
                <div class="row notUser" style="display:none;">
                    <div class="col-sm-6 col-sm-offset-3">
                        <h1 class="text-center">Almost Done!</h1>
                        <div class="panel panel-default">
                            <div class="panel-body text-center">
                                <div class="alert alert-info text-left">
                                    As the organizer, you have the ability to make
                                    changes to this Event.<br>Please sign in to
                                    identify yourself as the organizer.
                                </div>
                                <div>
                                    <a class="btn btn-lg btn-primary btn-block" href="{{ route( 'login' ) }}">
                                        Sign In
                                    </a>
                                </div>
                                <div class="horizontal-divider">OR</div>
                                <div class="pad-bottom">Don't have an account?<br>
                                    <a class="btn btn-lg btn-primary btn-block" href="{{ route ( 'register' ) }}">
                                        Create a free account</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body allForms">
                    <!-- Recepient details Form -->
                    <form id="recepient_form" class="form-horizontal" method="POST"
                          action="{{ route( 'trains.update', [ $meal -> unique_id ] ) }}"
                          autocomplete="off">
                        <div class="row">
                            <div class="col-sm-offset-2 col-sm-9">
                                <h3>This Gestr page is for:</h3>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventName" class="col-sm-2 control-label">Name
                                <span class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventName" name="EventName"
                                       placeholder="Recipient name" value="{{ $meal -> recipient_name }}" required>
                                <small>Example: The Smith Family</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventEmail" class="col-sm-2 control-label">Email
                                <span class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="EventEmail" name="EventEmail"
                                       placeholder="Recipient email address" readonly
                                       value="{{ $meal -> meal_recipient == null ? $meal -> recipient_email : $meal -> meal_recipient -> email }}"
                                >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventAddr1" class="col-sm-2 control-label">Address
                                <span class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventAddr1" name="EventAddr1"
                                       placeholder="Omit for more privacy" value="{{ $meal -> recipient_address }}"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventCity" class="col-sm-2 control-label">City
                                <span class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventCity" name="EventCity"
                                       value="{{ $meal -> recipient_city }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventState" class="col-sm-2 control-label">State/Prov.
                                <span class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="EventState" name="EventState"
                                       value="{{ $meal -> recipient_state }}" required>
                            </div>
                            <label for="EventZip" class="col-sm-2 control-label">Postal Code
                                <span class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="EventZip" name="EventZip"
                                       value="{{ $meal -> recipient_postal_code }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventPhone" class="col-sm-2 control-label">Phone
                                <span class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="EventPhone" name="EventPhone"
                                       value="{{ $meal -> recipient_phone }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventPhone" class="col-sm-2 control-label">About this Gestr Page</label>
                            <div class="col-sm-8">
                                <textarea id="about" class="form-control" name="about"
                                          rows="5">{{ $meal -> body }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventName" class="col-sm-2 control-label"># Adults to cook for<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="EventNumAdults" name="EventNumAdults"
                                       value="{{ $meal -> adults_cook_for }}" required>
                            </div>

                            <label for="EventEmail" class="col-sm-2 control-label"># Kids to cook for<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-3">
                                <input type="number" class="form-control" id="EventNumKids" name="EventNumKids"
                                       value="{{ $meal -> kids_cook_for }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventAddr1" class="col-sm-2 control-label">Preferred delivery time<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="EventTime" name="EventTime" maxlength="100"
                                       value="{{ $meal -> delivery_time }}" required>
                                <small>Example: from 5PM - 6PM</small>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group">
                            <label for="EventCity" class="col-sm-2 control-label">Special instructions<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventInstructions" name="EventInstructions"
                                          rows="5">{{ $meal -> special_instructions }}</textarea>
                                <small>List any dropoff, delivery, or other instructions</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventLikes" class="col-sm-2 control-label">Favorite meals/<span
                                        class="hidden-xs"> </span>Restaurants<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventLikes" name="EventLikes"
                                          rows="5">{{ $meal -> fave_meals_rest }}</textarea>
                                <small>Examples: lasagna, chili, etc</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventDislikes" class="col-sm-2 control-label">Least Favorite meals<span
                                        class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventDislikes" name="EventDislikes"
                                          rows="5">{{ $meal -> least_fave_meals }}</textarea>
                                <small>Example: anchovies, broccoli, etc</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="EventRestrictions" class="col-sm-2 control-label">
                                Allergies or dietary restrictions
                                <span class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea class="form-control" id="EventRestrictions" name="EventRestrictions"
                                          rows="5">{{ $meal -> restrictions }}</textarea>
                                <small>Example: allergic to shellfish, vegan, gluten-free, etc</small>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <div>
                                    <button class="btn btn-lg btn-success btn-responsive-block pull-right ladda-button ladda-button-demo"
                                            data-loading-text="Saving..." data-style="zoom-in" type="submit">
                                        <span class="ladda-label">Save Changes&nbsp;
                                            <i class="fa fa-caret-right"></i>
                                        </span>
                                        <span class="ladda-spinner"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Successfully Registered Employee -->
    <button id="swal_btn" type="button" style="display:none"></button>
    <!-- Employee not  Register -->
    <button id="swal_btn2" type="button" style="display:none"></button>

</x-app-layout>