<x-app-layout>
    <x-slot name="page_title">{{ __('Home') }}</x-slot>
    <x-slot name="headerContent">
        <div class="col-md-12 text-center">
            <div class="text-homeimage">
                <div class="maintext-image" data-scrollreveal="enter top over 1.5s after 0.1s">
                    BE KIND MAKE A GESTR
                </div>
                <div class="subtext-image" data-scrollreveal="enter bottom over 1.7s after 0.3s">
                    Join the Gestr Family
                </div>
            </div>
        </div>
    </x-slot>

    <!-- STEPS -->
    <div class="item content">
        <div class="container toparea">
            <div class="row text-center">
                <div class="col-md-4">
                    <div class="col editContent servicecontainer">
                        <span class="numberstep" style="background-color:violet;">
                            <i class="fa fa-shopping-cart"></i>
                        </span>
                        <h3 class="numbertext">Gestr Club</h3>
                        <p>The perfect way to organize meal giving, for free!</p>
                        <button class="btn btn-info">
                            <a href="{{ route ( 'member' ) }}" style="color: white">Join Today</a>
                        </button>
                    </div>
                    <!-- /.col-md-4 -->
                </div>
                <!-- /.col-md-4 col -->
                <div class="col-md-4 editContent servicecontainer">
                    <div class="col">
                        <span class="numberstep" style="background-color:orange;"><i class="fa fa-gift"></i></span>
                        <h3 class="numbertext">Gestr Club Plus</h3>
                        <p>Multiple meals per day, groceries, tasks and more.</p>
                        <button class="btn btn-info">
                            <a href="{{ route ( 'member' ) }}" style="color: white">Join Today</a>
                        </button>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.col-md-4 col -->
                <div class="col-md-4 editContent servicecontainer">
                    <div class="col">
                        <span class="numberstep" style="background-color:indigo;"><i class="fa fa-download"></i></span>
                        <h3 class="numbertext">Gestr Club Platinum</h3>
                        <p>Great for a group event.</p>
                        <button class="btn btn-info">
                            <a href="{{ route ( 'member' ) }}" style="color: white">Join Today</a>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- LATEST ITEMS -->
    <section class="item content">
        <div class="container">
            <div class="underlined-title">
                <div class="editContent">
                    <h1 class="text-center latestitems">The Acts of GESTR</h1>
                    <center><small>(Our Products & Services)</small></center>
                </div>
                <div class="wow-hr type_short">
				<span class="wow-hr-h">
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>This is a short excerpt to generally describe what the item is about.</p>
                                <p>
                                    <a class="learn-more detailslearn"><i class="fa fa-shopping-cart"></i> Purchase</a>
                                    <a class="learn-more detailslearn"><i class="fa fa-link"></i> Details</a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset( 'images/service1.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Meal Preparation</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$49.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>PRODUCT</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a class="learn-more detailslearn"><i class="fa fa-shopping-cart"></i> Purchase</a>
                                    <a class="learn-more detailslearn"><i class="fa fa-link"></i> Details</a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset( 'images/service3.jpg' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Personalised Gifts</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$69.00</span>
						</span>
                        </div>
                    </div>
                </div>
                <!-- /.productbox -->
                <div class="col-md-4">
                    <div class="productbox">
                        <div class="fadeshop">
                            <div class="captionshop text-center" style="display: none;">
                                <h3>SERVICE</h3>
                                <p>
                                    This is a short excerpt to generally describe what the item is about.
                                </p>
                                <p>
                                    <a class="learn-more detailslearn"><i class="fa fa-shopping-cart"></i> Purchase</a>
                                    <a class="learn-more detailslearn"><i class="fa fa-link"></i> Details</a>
                                </p>
                            </div>
                            <span class="maxproduct"><img src="{{ asset( 'images/service2.png' ) }}" alt=""></span>
                        </div>
                        <div class="product-details">
                            <a href="#">
                                <h1>Cleaning</h1>
                            </a>
                            <span class="price">
						<span class="edd_price">$79.00</span>
						</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- BUTTONS -->
    <div class="item content">
        <div class="container text-center">
            <a href="{{ route ( 'shop' ) }}" class="homebrowseitems">Browse All Products
                <div class="homebrowseitemsicon">
                    <i class="fa fa-star fa-spin"></i>
                </div>
            </a>
        </div>
    </div>
    <br/>

    <!-- AREA -->
    <div class="item content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-microphone infoareaicon"></i>
                    <div class="infoareawrap">
                        <h1 class="text-center subtitle">Who are we?</h1>
                        <p>
                            Gestr Is A Platform By Which You Can Create An Online Community To Organise, Fund And
                            Purchase Support Services And As A Means To Offer Help Support Or As An Much Needed Gift.
                            Our Club Packages Provide A Platform By Which You Can Create An Online Community To
                            Organise, Fund And Purchase Support, Gift Packages And Or Services From Our Carefully
                            Selected Range.
                        </p>
                        <p class="text-center">
                            <a href="{{ route ( 'contact.index' ) }}">- Get in Touch -</a>
                        </p>
                    </div>
                </div>
                <!-- /.col-md-4 col -->
                <div class="col-md-6">
                    <i class="fa fa-comments infoareaicon"></i>
                    <div class="infoareawrap">
                        <h1 class="text-center subtitle">WHAT DO WE DO?</h1>
                        <p>
                            Gestr is designed to assist communities, whether these be friends, family groups or
                            colleagues to transform people lives by providing a platform by which they can create an
                            online community to organise, fund and purchase support services and as a means to offer
                            help support or as an much needed gift. Gestr is built to bring happiness through
                            collaboration, a truly individualised service designed around the community model We bring
                            local business together to provide an individual or package of services that will provide
                            meaningful benefits to the recipient and help them feel that sense of communitydisabled.
                        </p>
                        <p class="text-center">
                            <a href="{{ route ( 'contact.index' ) }}">- Get in Touch -</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- TESTIMONIAL -->
    <div class="item content">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="slide-text">
                    <div>
                        <h2><span class="uppercase">A Word From Our Founder</span></h2>
                        <img src="http://wowthemes.net/demo/salique/salique-boxed/images/temp/avatar2.png"
                             alt="Awesome Support">
                        <p>
                            The support... I can only say it's awesome. You make a product and you help people out any
                            way you can even if it means that you have to log in on their dashboard to sort out any
                            problems that customer might have. Simply Outstanding!
                        </p>
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                                class="fa fa-star"></i><i class="fa fa-star"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
