<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route ::get ( 'TestingQueBuild', 'UserController@TestingQueBuild' );
Route ::get ( 'GetUser/{id}', 'UserController@GetUser' );
Route ::get ( 'GetAllUsers', 'UserController@GetAllUsers' );
Route ::post ( 'CreateGkUser', 'UserController@CreateGkUser' );
Route ::post ( 'UserSignin', 'UserController@UserSignin' );

Route ::post ( 'BuyPackage', 'UserController@BuyPackage' );
Route ::post ( 'DateSlots', 'UserController@DateSlots' );
Route ::post ( 'DateSlotsCode2', 'UserController@DateSlotsCode2' );
Route ::post ( 'AddPost', 'UserController@AddPost' );
Route ::get ( 'ViewPosts', 'UserController@ViewPosts' );
Route ::get ( 'ViewPackagesUser', 'UserController@ViewPackagesUser' );

Route ::middleware ( 'auth:api' ) -> get ( '/user', function ( Request $request ) {
    return $request -> user ();
} );
