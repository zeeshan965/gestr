<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\TrainsController;
use App\Http\Controllers\MealPostsController;
use App\Http\Controllers\VolunteerController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route ::get ( '/', HomeController::class ) -> name ( 'home' );
Route ::get ( '/member', MemberController::class ) -> name ( 'member' );
Route ::get ( '/basic_plan_register', [ CheckoutController::class, 'basic_plan_register' ] ) -> name ( 'basic_plan_register' );
Route ::get ( '/plus_plan_register', [ CheckoutController::class, 'plus_plan_register' ] ) -> name ( 'plus_plan_register' );
Route ::get ( '/shop', ShopController::class ) -> name ( 'shop' );
Route ::get ( '/about', AboutController::class ) -> name ( 'about' );
Route ::get ( '/checkout', CheckoutController::class ) -> name ( 'checkout' );
Route ::get ( '/product', ProductController::class ) -> name ( 'product' );
Route ::resource ( '/contact', ContactController::class );

Route ::group ( [ 'middleware' => [ 'guest' ] ], function () {

} );

//Fetch details of meal | These routes needs to be public, Guest & logged in user both can access it
Route ::get ( 'trains/{id}', [ TrainsController::class, 'trains' ] ) -> name ( 'trains' );
Route ::get ( 'trains/{id}/disable_tips', [ TrainsController::class, 'disable_tips' ] ) -> name ( 'disable_tips' );
Route ::get ( 'trains/{id}/fetch_events', [ TrainsController::class, 'fetch_events' ] ) -> name ( 'trains.fetch_events' );
Route ::get ( 'trains/{id}/all_events', [ TrainsController::class, 'all_events' ] ) -> name ( 'trains.all_events' );
Route ::get ( 'trains/{id}/updates', [ MealPostsController::class, 'updates' ] ) -> name ( 'trains.updates' );
Route ::get ( 'trains/{id}/print', [ TrainsController::class, 'print' ] ) -> name ( 'trains.print' );

Route ::group ( [ 'middleware' => [ 'auth' ] ], function () {
    Route ::prefix ( 'trains/{id}' ) -> group ( function () {
        //Activation of Meal
        Route ::get ( 'activation', [ TrainsController::class, 'activate_train' ] ) -> name ( 'trains.activation' );

        //Upload meal photo
        Route ::post ( 'upload', [ TrainsController::class, 'upload' ] ) -> name ( 'trains.upload' );
        Route ::post ( 'remove_photo', [ TrainsController::class, 'remove_photo' ] ) -> name ( 'trains.remove_photo' );
        Route ::post ( 'remove', [ TrainsController::class, 'remove' ] ) -> name ( 'trains.remove' );

        //Edit existing meal
        Route ::get ( 'edit', [ TrainsController::class, 'edit' ] ) -> name ( 'trains.edit' );
        Route ::post ( 'update', [ TrainsController::class, 'update' ] ) -> name ( 'trains.update' );
        Route ::post ( 'edit/story', [ TrainsController::class, 'edit_story' ] ) -> name ( 'trains.edit_story' );
        Route ::get ( 'edit_calendar', [ TrainsController::class, 'edit_calendar' ] ) -> name ( 'trains.edit_calendar' );
        Route ::post ( 'update_calendar', [ CheckoutController::class, 'update_calendar' ] ) -> name ( 'trains.update_calendar' );

        //Manage Organizers
        Route ::prefix ( 'organizers' ) -> group ( function () {
            Route ::get ( '/', [ TrainsController::class, 'organizers' ] ) -> name ( 'trains.organizers' );
            Route ::post ( '/create', [ TrainsController::class, 'create_organizer' ] ) -> name ( 'trains.organizers.create' );
            Route ::post ( '/make_primary', [ TrainsController::class, 'make_primary' ] ) -> name ( 'trains.organizers.make_primary' );
            Route ::post ( '/remove', [ TrainsController::class, 'remove_organizer' ] ) -> name ( 'trains.organizers.remove' );
        } );

        //Manage Participants
        Route ::prefix ( 'participants' ) -> group ( function () {
            Route ::get ( '/', [ TrainsController::class, 'participants' ] ) -> name ( 'trains.participants' );
            Route ::post ( '/remove', [ TrainsController::class, 'remove_participants' ] ) -> name ( 'trains.participants.remove' );
        } );

        //Message users
        Route ::prefix ( 'message' ) -> group ( function () {
            Route ::get ( '/', [ TrainsController::class, 'message' ] ) -> name ( 'trains.message' );
            Route ::post ( '/', [ TrainsController::class, 'send_message' ] ) -> name ( 'trains.send_message' );
            Route ::get ( '/confirm', [ TrainsController::class, 'message_confirm' ] ) -> name ( 'trains.message_confirm' );
        } );

        //Message participant users
        Route ::prefix ( 'participant_message' ) -> group ( function () {
            Route ::get ( '/', [ TrainsController::class, 'participant_message' ] ) -> name ( 'trains.participant_message' );
            Route ::post ( '/', [ TrainsController::class, 'send_participant_message' ] ) -> name ( 'trains.participant_send_message' );
        } );

        //Invite users to meal
        Route ::prefix ( 'invite' ) -> group ( function () {
            Route ::get ( '/', [ TrainsController::class, 'invite' ] ) -> name ( 'trains.invite' );
            Route ::post ( '/', [ TrainsController::class, 'send_invite' ] ) -> name ( 'trains.send_invite' );
            Route ::get ( '/confirm', [ TrainsController::class, 'invite_confirm' ] ) -> name ( 'trains.invite_confirm' );
        } );

        //Volunteer for meal
        Route ::prefix ( 'volunteer/{slot}' ) -> group ( function () {
            Route ::get ( '/', [ VolunteerController::class, 'volunteer' ] ) -> name ( 'trains.volunteer' );
            Route ::post ( '/', [ VolunteerController::class, 'store' ] ) -> name ( 'trains.store' );
            Route ::get ( '/confirm/{v_id}', [ VolunteerController::class, 'confirm_volunteer' ] ) -> name ( 'trains.volunteer.confirm' );
            Route ::get ( '/calendar_invite/{v_id}', [ VolunteerController::class, 'calendar_invite' ] ) -> name ( 'trains.volunteer.calendar_invite' );
            Route ::get ( '/edit/{v_id}', [ VolunteerController::class, 'edit_volunteer' ] ) -> name ( 'trains.volunteer.edit' );
            Route ::post ( '/update/{v_id}', [ VolunteerController::class, 'update_volunteer' ] ) -> name ( 'trains.volunteer.update' );
            Route ::post ( '/delete/{v_id}', [ VolunteerController::class, 'delete_volunteer' ] ) -> name ( 'trains.volunteer.delete' );
            Route ::post ( '/cancel/{v_id}', [ VolunteerController::class, 'cancel_booking' ] ) -> name ( 'trains.volunteer.cancel' );
        } );

        //Meal posts
        Route ::prefix ( 'updates' ) -> group ( function () {
            Route ::get ( '/new', [ MealPostsController::class, 'new_updates' ] ) -> name ( 'trains.updates.new' );
            Route ::post ( '/store', [ MealPostsController::class, 'store_updates' ] ) -> name ( 'trains.updates.store' );

            //Post Update
            Route ::prefix ( '/{update_id}' ) -> group ( function () {
                Route ::get ( '/confirm', [ MealPostsController::class, 'update_confirm' ] ) -> name ( 'trains.updates.confirm' );
                Route ::get ( '/edit', [ MealPostsController::class, 'edit_post' ] ) -> name ( 'trains.updates.edit' );
                Route ::post ( '/update', [ MealPostsController::class, 'post_update' ] ) -> name ( 'trains.updates.update' );
                Route ::post ( '/delete', [ MealPostsController::class, 'delete_update' ] ) -> name ( 'trains.updates.delete' );
            } );
        } );
    } );
    Route ::get ( '/dashboard', DashboardController::class ) -> name ( 'dashboard' );
    Route ::post ( 'checkout', [ CheckoutController::class, 'store' ] ) -> name ( 'checkout.store' );
    Route ::get ( 'profile', [ UserController::class, 'profile' ] ) -> name ( 'profile' );
    Route ::post ( 'profile', [ UserController::class, 'update' ] ) -> name ( 'profile.update' );
    Route ::post ( 'update_password', [ UserController::class, 'update_password' ] ) -> name ( 'profile.update_password' );
} );

require __DIR__ . '/auth.php';
