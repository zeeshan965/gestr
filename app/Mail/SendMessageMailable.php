<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMessageMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $organizer;
    public $full_name;
    public $meal_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct ( $data )
    {
        $this -> data = $data;
        if ( $data[ 'email' ] == $this -> data[ 'meal' ] -> user -> email )
            $this -> full_name = ' ' . $this -> data[ 'meal' ] -> user -> full_name;
        else $this -> full_name = '';
        $this -> organizer = $this -> data[ 'meal' ] -> user -> full_name;
        $this -> meal_name = $this -> data[ 'meal' ] -> recipient_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build ()
    {
        return $this -> subject ( $this -> data[ 'Subject' ] )
            -> markdown ( 'emails.message' );
    }
}
