<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPostUpdateMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $meal_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct ( $data )
    {
        $this -> data = $data;
        $this -> data[ 'id' ] = $data[ 'meal' ] -> unique_id;
        $this -> meal_name = $this -> data[ 'meal' ] -> recipient_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build ()
    {
        return $this -> subject ( "New update - Gestr for {$this -> meal_name}" )
            -> markdown ( 'emails.post_update' );
    }
}
