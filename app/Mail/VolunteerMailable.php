<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class VolunteerMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $volunteer;
    public $assignedUser;
    public $volunteer_name;
    public $meal_name;
    public $train_name;
    public $date;
    public $user;
    public $self = false;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @param $data
     */
    public function __construct ( $data )
    {
        $this -> data = $data;
        $this -> volunteer = $data[ 'volunteer' ];
        $this -> user = $data[ 'user' ];
        $this -> meal_name = $data[ 'volunteer' ] -> body;

        $this -> date = $data[ 'volunteer' ] -> dateSlot -> dateslots -> format ( 'l, F d, Y' );
        $this -> train_name = $data[ 'volunteer' ] -> dateSlot -> meal -> recipient_name;
        $this -> assignedUser = $this -> volunteer -> assignedUser;
        $this -> volunteer_name = ucfirst ( $this -> assignedUser -> first_name ) . ' ' . ucfirst ( $this -> assignedUser -> last_name );
        $this -> data[ 'id' ] = $data[ 'volunteer' ] -> dateSlot -> meal -> unique_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build ()
    {
        $subject = $this -> data[ 'user' ] -> email === $this -> to[ 0 ][ 'address' ] ?
            "{$this -> volunteer_name} has signed up to provide a meal" :
            "{$this -> data[ 'user' ] -> full_name} has signed you up to provide a meal.";
        $participant = $this -> data[ 'volunteer' ] -> dateSlot -> meal -> participants -> pluck ( 'user_id' ) -> toArray ();
        if ( in_array ( $this -> user -> id, $participant ) )
            return $this -> subject ( $subject ) -> markdown ( 'emails.existed_user_volunteer' );
        else
            return $this -> data[ 'user' ] -> email === $this -> to[ 0 ][ 'address' ] ?
                $this -> subject ( $subject ) -> markdown ( 'emails.self_volunteer' ) :
                $this -> subject ( $subject ) -> markdown ( 'emails.volunteer' );
    }
}
