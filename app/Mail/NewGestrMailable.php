<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewGestrMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $meal_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct ( $data )
    {
        $this -> data = $data;
        $this -> data[ 'id' ] = $data[ 'meal' ] -> unique_id;
        $this -> meal_name = $this -> data[ 'meal' ] -> recipient_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build ()
    {
        $subject = $this -> data[ 'user' ] -> email === $this -> to[ 0 ][ 'address' ] ?
            "Gestr for {$this -> meal_name}" :
            "{$this -> data[ 'user' ] -> full_name} created a Gestr page for you! Important, please verify. {$this -> meal_name}";
        return $this -> data[ 'user' ] -> email === $this -> to[ 0 ][ 'address' ] ?
            $this -> subject ( $subject ) -> markdown ( 'emails.new_gestr' ) :
            $this -> subject ( $subject ) -> markdown ( 'emails.gestr' );
    }
}
