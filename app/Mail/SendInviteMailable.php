<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInviteMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $full_name;
    public $meal_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct ( $data )
    {
        $this -> data = $data;
        $this -> full_name = $data[ 'user' ][ 'first_name' ] . '' . $data[ 'user' ][ 'last_name' ];
        $this -> meal_name = $this -> data[ 'meal' ] -> recipient_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build ()
    {
        if ( $this -> data [ 'CC' ] == true || $this -> data [ 'CC' ] == 'true' )
            return $this -> subject ( "{$this -> full_name} has invited you to join the Gestr for {$this -> meal_name}" )
                -> markdown ( 'emails.email_invite' ) -> cc ( $this -> data[ 'user' ] -> email );
        return $this -> subject ( "{$this -> full_name} has invited you to join the Gestr for {$this -> meal_name}" )
            -> markdown ( 'emails.email_invite' );
    }
}
