<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ChangePasswordMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @param $user
     */
    public function __construct ( $user )
    {
        $this -> user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build ()
    {
        return $this -> subject ( "Your password was updated" ) -> markdown ( 'emails.change_password' );
    }
}
