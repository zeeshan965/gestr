<?php

namespace App\Models;

use App\Mail\NewGestrMailable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use StdClass;

class BookMeal extends Model
{
    protected $fillable = [
        'url', 'organizer', 'recipient_name', 'recipient_email', 'recipient_address', 'recipient_city', 'recipient_state', 'recipient_postal_code',
        'recipient_phone', 'adults_cook_for', 'kids_cook_for', 'delivery_time', 'special_instructions', 'fave_meals_rest', 'least_fave_meals',
        'restrictions', 'package_name', 'package_payment', 'unique_id', 'photo', 'user_id', 'body'
    ];

    /**
     * @return BelongsTo
     */
    public function user ()
    {
        return $this -> belongsTo ( User::class, 'user_id', 'id' );
    }

    /**
     * @return HasMany
     */
    public function allSlots ()
    {
        return $this -> hasMany ( MealDateSlots::class, 'mt_pack_id' )
            -> with ( [ 'events', 'volunteer' ] )
            -> orderBy ( 'meal_date_slots.dateslots', 'ASC' );
    }

    /**
     * @return HasMany
     */
    public function dateSlots ()
    {
        $carbon = Carbon ::now () -> format ( 'Y-m-d' );
        return $this -> hasMany ( MealDateSlots::class, 'mt_pack_id' )
            -> with ( [ 'volunteer', 'events' ] )
            -> where ( DB ::raw ( "date_format(dateslots,'%Y-%m-%d')" ), '>=', $carbon )
            -> orderBy ( 'meal_date_slots.dateslots', 'ASC' );
    }

    /**
     * @return HasMany
     */
    public function oldDateSlots ()
    {
        $carbon = Carbon ::now () -> format ( 'Y-m-d' );
        return $this -> hasMany ( MealDateSlots::class, 'mt_pack_id' )
            -> with ( [ 'volunteer', 'events' ] )
            -> where ( DB ::raw ( "date_format(dateslots,'%Y-%m-%d')" ), '<', $carbon )
            -> orderBy ( 'meal_date_slots.dateslots', 'ASC' );
    }

    /**
     * @return HasMany
     */
    public function recipients ()
    {
        return $this -> hasMany ( InviteUser::class, 'book_meal_id' );
    }

    /**
     * @return HasOne
     */
    public function meal_recipient ()
    {
        return $this -> hasOne ( Recipient::class, 'book_meal_id' ) -> orderBy ( 'id', 'ASC' );
    }

    /**
     * @return Collection
     */
    public function get_all_meal_recipients ()
    {
        return $this -> hasMany ( Recipient::class, 'book_meal_id' ) -> get ();
    }

    /**
     * Get the post's image.
     */
    public function attachment ()
    {
        return $this -> morphOne ( Attachment::class, 'attachable' );
    }

    /**
     * @return HasMany
     */
    public function posts ()
    {
        return $this -> hasMany ( MealPost::class, 'book_meal_id' ) -> orderBy ( 'id', 'DESC' );
    }

    /**
     * @return HasMany
     */
    public function participants ()
    {
        return $this -> hasMany ( Participants::class, 'book_meal_id' );
    }

    /**
     * @return HasManyThrough
     */
    public function volunteers ()
    {
        return $this -> hasManyThrough ( Volunteer::class, MealDateSlots::class,
            'mt_pack_id', 'slot_id', 'id', 'id' );
    }

    /**
     * @return BelongsToMany
     */
    public function users ()
    {
        return $this->belongsToMany(User::class, 'book_meal_users', 'book_meal_id', 'user_id');
    }

    /**
     * @param $request
     * @return mixed
     */
    public static function insert_meal ( $request )
    {
        $user = Auth ::user ();
        $meal = self ::create ( [
            'unique_id' => md5 ( uniqid ( rand (), true ) ),
            'url' => $request -> url,
            'organizer' => $request -> organizer,
            'recipient_name' => $request -> recipient_name,
            'recipient_email' => $request -> recipient_email,
            'recipient_address' => $request -> recipient_address,
            'recipient_city' => $request -> recipient_city,
            'recipient_state' => $request -> recipient_state,
            'recipient_postal_code' => $request -> recipient_postalcode,
            'recipient_phone' => $request -> recipient_phone,
            'adults_cook_for' => $request -> adults_cook_for,
            'kids_cook_for' => $request -> kids_cook_for,
            'delivery_time' => $request -> delivery_time,
            'special_instructions' => $request -> special_instructions,
            'fave_meals_rest' => $request -> fave_meals_rest,
            'least_fave_meals' => $request -> least_fave_meals,
            'restrictions' => $request -> restrictions,
            'package_name' => $request -> package_name,
            'package_payment' => $request -> package_payment,
            'user_id' => Auth ::id (),
        ] );

        if ( $request -> recipient_email != $user -> email )
            Recipient ::insert_recipient ( $request -> recipient_email, $meal -> id, $user -> id, 1 );
        self ::send_email ( $meal, $request );
        //$meal -> user () -> associate ( $user ) -> save ();
        return $meal;
    }

    /**
     * @param $id
     * @param $data
     * @return mixed
     */
    public static function update_meal ( $id, $data )
    {
        $meal = self ::where ( 'unique_id', $id ) -> first ();
        $meal -> update ( $data );
        return $meal;
    }

    /**
     * @param $meal
     * @param $request
     * @return string
     */
    private static function send_email ( $meal, $request )
    {
        if ( $meal -> recipient_email !== Auth ::user () -> email ) $users[] = $meal -> recipient_email;
        $users[] = Auth ::user () -> email;
        $data = [ 'meal' => $meal, 'user' => Auth ::user (), 'request' => $request ];
        foreach ( $users as $email ) Mail ::to ( $email ) -> send ( new NewGestrMailable( $data ) );
    }

    /**
     * @param $id
     * @param $array
     * @return array
     */
    public static function fetchDateslots ( $id, $array ) : array
    {
        $events = [];
        foreach ( $array as $item ) {
            $obj = new StdClass();
            $obj -> Date = $item -> dateslots -> format ( 'Y/m/d' );
            $obj -> format_date = $item -> dateslots -> format ( 'Y-m-d' );
            $obj -> Title = $item -> volunteer == null ? 'Available' : $item -> volunteer -> body;
            $slot_id = base64_encode ( $item -> id );
            $obj -> Link = $item -> volunteer == null ? url ( "/trains/{$id}/volunteer/{$slot_id}" ) :
                url ( "/trains/{$id}/volunteer/{$slot_id}/edit/{$item -> volunteer -> unique_id}" );
            array_push ( $events, $obj );
        }
        return $events;
    }

    /**
     * @param $id
     * @param $array
     * @return array
     */
    public static function fetchPlusDateslots ( $id, $array ) : array
    {
        $events = [];
        foreach ( $array as $item ) {
            $slot_id = base64_encode ( $item -> id );
            $obj = new StdClass();
            $obj -> Date = $item -> dateslots -> format ( 'Y/m/d' );
            $obj -> event_data = [];
            foreach ( $item -> events as $k => $option ) {
                $event_id = base64_encode ( $option -> id );
                $event = new StdClass();
                $event -> category = $option -> category;
                $event -> link = $item -> volunteer == null || $item -> volunteer -> event_id != $option -> id ?
                    url ( "/trains/{$id}/volunteer/{$slot_id}?event_id={$event_id}" ) :
                    url ( "/trains/{$id}/volunteer/{$slot_id}/edit/{$item -> volunteer -> unique_id}?event_id={$event_id}" );
                $event -> edit = $item -> volunteer == null || $item -> volunteer -> event_id != $option -> id ? false : true;
                $event -> needed = $option -> needed;
                $event -> title = "{$option -> category}: {$option -> needed}";
                $obj -> event_data[ $k ] = $event;
            }
            $events[ $item -> dateslots -> format ( 'Y/m/d' ) ] = $obj;
        }
        return $events;
    }

    /**
     * @param $meal
     * @return array[]
     */
    public static function fetch_organizers ( $meal ) : array
    {
        $data = [];
        $organizer_check = false;
        $temp_email = '';
        $meal_recipients = $meal -> get_all_meal_recipients ();
        if ( isset( $meal_recipients[ 0 ] -> email ) && $meal_recipients[ 0 ] -> email == $meal -> recipient_email ) {
            $data[] = [ "name" => $meal -> recipient_name, "email" => $meal -> recipient_email, "type" => "Recipient", "actions" => "" ];
        }

        foreach ( $meal_recipients as $recipient ) {
            if ( $recipient -> email != $meal -> recipient_email ) {
                if( $recipient->organizer_status == 1 ){
                    $organizer_check = true;
                    $temp_email = $recipient -> email;
                    $data[] = [
                        "name" => explode ( "@", $recipient -> email )[ 0 ], "email" => $recipient -> email,
                        "type" => "Primary Organizer",
                        "actions" => ''
                    ];
                }
            }
        }

        if( !$organizer_check ){
            $data[] = [
                "name" => $meal -> user -> full_name, "email" => $meal -> user -> email,
                "type" => $meal -> user -> email == $meal -> recipient_email ? "Recipient/ Primary Organizer" : "Primary Organizer",
                "actions" => ""
            ];
        }

        foreach ( $meal_recipients as $recipient ) {
            if ( $recipient -> email != $meal -> recipient_email && $recipient -> email != $temp_email ) {
                $data[] = [
                    "name" => explode ( "@", $recipient -> email )[ 0 ], "email" => $recipient -> email,
                    "type" => $recipient->organizer_status == 0 ? "Secondary Organizer" : 'Primary Organizer',
                    "actions" => $recipient->organizer_status == 0 ? '<a class="btn btn-primary make-primary" data-id="' . $recipient -> id . '" title="Make Primary" href="javascript:;">
                                  <i class="fa fa-arrow-up"></i></a> &nbsp; 
                                  <a title="Remove Organizer" data-id="' . $recipient -> id . '" class="btn btn-danger remove-organizer" href="javascript:;">
                                  <i class="fa fa-trash"></i> </a>' : ''
                ];
            }
        }

        if( $organizer_check ){
            $data[] = [
                "name" => $meal -> user -> full_name, "email" => $meal -> user -> email,
                "type" => "Secondary Organizer",
                "actions" => '<a class="btn btn-primary make-primary" data-id="0" title="Make Primary" href="javascript:;">
                                  <i class="fa fa-arrow-up"></i></a> &nbsp;'
            ];
        }
        return $data;
    }

    /**
     * @param $meal
     * @return array[]
     */
    public static function fetch_participants ( $book_meal ) : array
    {
        $data = [];
        $original_recipients = $book_meal -> get_all_meal_recipients ();
        $meal_recipients = $book_meal -> recipients () -> groupBy ( 'meal_recipients.email' ) -> get ();

        // dd($book_meal -> meal_recipient -> email, $book_meal, $original_recipients, $meal_recipients);

        $data[] = [
            "name" => $book_meal -> user -> full_name,
            "email" => $book_meal -> user -> email,
            "actions" => '<a href="/trains/' . $book_meal -> unique_id . '/message?user=' . $book_meal -> user -> id . '" class="btn btn-primary btn-sm"><i class="fa fa-envelope" alt="Send email" title="Send email"></i></a>'
        ];

        foreach ( $original_recipients as $recipient ) {
            $data[] = [
                "name" => explode ( "@", $recipient -> email )[ 0 ],
                "email" => $recipient -> email,
                "actions" => '<a href="/trains/' . $book_meal -> unique_id . '/message?meal=' . $recipient -> id . '" class="btn btn-primary btn-sm"><i class="fa fa-envelope" alt="Send email" title="Send email"></i></a>'
            ];
        }

        foreach ( $meal_recipients as $recipient ) {
            $data[] = [
                "name" => explode ( "@", $recipient -> email )[ 0 ],
                "email" => $recipient -> email,
                "actions" => '<a href="/trains/' . $book_meal -> unique_id . '/message?participant=' . $recipient -> id . '" class="btn btn-primary btn-sm"><i class="fa fa-envelope" alt="Send email" title="Send email"></i></a> 
                            <a href="#" data-id="' . $recipient -> id . '" class="btn btn-participants-remove btn-danger btn-sm"><i class="fa fa-trash" alt="Remove" title="Remove"></i></a>'
            ];
        }
        return $data;
    }
}
