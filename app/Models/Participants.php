<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Participants extends Model
{
    /**
     * @var string[]
     */
    protected $guarded = [ 'id' ];

    /**
     * @return BelongsTo
     */
    public function meal ()
    {
        return $this -> belongsTo ( BookMeal::class, 'book_meal_id' );
    }

    /**
     * @param $meal
     * @param $user
     * @param $status
     * @return mixed
     */
    public static function insert_participant ( $meal, $user, $status )
    {
        return self ::updateOrCreate (
            [ 'book_meal_id' => $meal -> id, 'email' => $user -> email, 'user_id' => $user -> id ],
            [ 'unique_id' => md5 ( uniqid ( rand (), true ) ), 'status' => $status ] );
    }
}
