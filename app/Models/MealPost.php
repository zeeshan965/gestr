<?php

namespace App\Models;

use App\Facades\Helper;
use App\Mail\SendPostUpdateMailable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Mail;

class MealPost extends Model
{
    protected $guarded = [ 'id' ];

    /**
     * Get the post's image.
     */
    public function attachment ()
    {
        return $this -> morphOne ( Attachment::class, 'attachable' );
    }

    /**
     * @return BelongsTo
     */
    public function user ()
    {
        return $this -> belongsTo ( User::class, 'user_id' );
    }

    /**
     * @return BelongsTo
     */
    public function meal ()
    {
        return $this -> belongsTo ( BookMeal::class, 'book_meal_id' );
    }

    /**
     * @param $id
     * @param $request
     * @param $user
     * @return mixed
     */
    public static function insert_post ( $id, $request, $user )
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> with ( 'recipients' ) -> first ();
        $post = self ::create ( [
            'unique_id' => md5 ( uniqid ( rand (), true ) ),
            'book_meal_id' => $meal -> id,
            'title' => $request -> title ?? '',
            'body' => $request -> text ?? '',
            'user_id' => $user -> id ?? '',
        ] );
        if ( $request -> hasFile ( 'files' ) ) {
            $path = Helper ::upload_attachment ( $request -> all () );
            $post -> attachment () -> save ( new Attachment( [ 'url' => $path ] ) );
        }
        $data = [ 'user' => $user, 'meal' => $meal, 'mealPost' => $post -> unique_id, 'request' => $request -> all () ];
        Mail ::to ( $user[ 'email' ] ) -> send ( new SendPostUpdateMailable( $data ) );
        return $post;
    }

    /**
     * @param $id
     * @param $request
     * @param $user
     * @return mixed
     */
    public static function update_post ( $id, $request, $user )
    {
        $post = self ::with ( 'attachment' ) -> find ( $request -> post ( 'id' ) );
        if ( $request -> has ( 'remove_image' ) && $request -> post ( 'remove_image' ) == 1 && $post -> attachment () !== null )
            $post -> attachment () -> delete ();
        $post -> update ( [
            'title' => $request -> title ?? '',
            'body' => $request -> text ?? '',
        ] );
        if ( $request -> hasFile ( 'files' ) ) {
            $path = Helper ::upload_attachment ( $request -> all () );
            $post -> attachment () -> save ( new Attachment( [ 'url' => $path ] ) );
        }
        return $post;
    }
}
