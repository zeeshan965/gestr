<?php

namespace App\Models;

use App\Facades\Helper;
use App\Mail\VolunteerMailable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;

class Volunteer extends Model
{
    /**
     * @var string[]
     */
    protected $guarded = [ 'id' ];


    public function getAssignedUserAttribute ( $value )
    {
        $data = json_decode ( $value );
        if ( Request ::segment ( 1 ) == 'trains' && Request ::segment ( 3 ) == 'remove' && Request ::method () == 'POST' ) return $data;
        if ( isset( $data -> id ) && $data -> id == '-1' ) {
            if ( Request ::segment ( 1 ) == 'trains' && Request ::segment ( 3 ) == 'volunteer' &&
                ( Request ::method () == 'POST' && Request ::has ( 'assignedUser' ) && Request ::has ( 'description' ) ) ) return $data;
            else return "{$data -> first_name} {$data -> last_name}";
        } else {
            if ( Request ::segment ( 1 ) == 'trains' && Request ::segment ( 3 ) == 'volunteer' &&
                ( Request ::method () == 'POST' && Request ::has ( 'assignedUser' ) && Request ::has ( 'description' ) ) ) return $this -> user;
            else return $this -> user -> full_name;
        }
    }

    /**
     * @return BelongsTo
     */
    public function dateSlot ()
    {
        return $this -> belongsTo ( MealDateSlots::class, 'slot_id' ) -> with ( [ 'meal', 'events' ] );
    }

    /**
     * @return BelongsTo
     */
    public function user ()
    {
        return $this -> belongsTo ( User::class, 'user_id' );
    }

    /**
     * @param $id
     * @param $slot_id
     * @param $request
     * @param $user
     * @return mixed
     */
    public static function insert_data ( $id, $slot_id, $request, $user )
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> with ( 'recipients' ) -> first ();
        $volunteer = self ::create ( [
            'slot_id' => base64_decode ( $slot_id ),
            'event_id' => $request -> post ( 'event_id' ),
            'user_id' => $user -> id ?? '',
            'unique_id' => md5 ( uniqid ( rand (), true ) ),
            'body' => $request -> description ?? '',
            'note' => $request -> notes ?? '',
            'one_day_reminder' => $request -> has ( 'OneDayReminder' ) ? 1 : 0,
            'one_week_reminder' => $request -> has ( 'OneWeekReminder' ) ? 1 : 0,
            'assignedUser' => json_encode ( $request -> assignedUser ) ?? '',
        ] );

        $volunteer -> load ( [ 'user', 'dateSlot' ] );
        self ::send_email ( $volunteer, $request );
        return $volunteer;
    }

    /**
     * @param $volunteer
     * @param $request
     */
    private static function send_email ( $volunteer, $request )
    {
        $volunteer_email = $volunteer -> assignedUser -> email ?? '';
        $users[] = $volunteer -> user -> email;
        if ( $volunteer_email !== $volunteer -> user -> email ) $users[] = $volunteer_email;
        $data = [ 'volunteer' => $volunteer, 'user' => $volunteer -> user, 'request' => $request ];
        foreach ( $users as $email ) Mail ::to ( $email ) -> send ( new VolunteerMailable( $data ) );
    }

    /**
     * @param $id
     * @param $slot_id
     * @param $v_id
     * @param $request
     * @param $user
     * @return mixed
     */
    public static function update_data ( $id, $slot_id, $v_id, $request, $user )
    {
        $volunteer = Volunteer ::with ( 'dateSlot', 'user' ) -> where ( 'unique_id', $v_id ) -> first ();
        $volunteer -> update ( [
            'body' => $request -> description ?? '',
            'note' => $request -> notes ?? '',
            'one_day_reminder' => $request -> has ( 'OneDayReminder' ) ? 1 : 0,
            'one_week_reminder' => $request -> has ( 'OneWeekReminder' ) ? 1 : 0,
        ] );
        return $volunteer;
    }
}
