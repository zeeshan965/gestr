<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Request;

class MealDateSlots extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'dateslots', 'mt_pack_id', 'category', 'needed'
    ];

    /**
     * @return BelongsTo
     */
    public function meal ()
    {
        return $this -> belongsTo ( BookMeal::class, 'mt_pack_id' ) -> with ( 'participants' );
    }

    /**
     * @return HasOne
     */
    public function volunteer ()
    {
        return $this -> hasOne ( Volunteer::class, 'slot_id' ) -> with ( 'user' );
    }

    /**
     * @return HasMany
     */
    public function events ()
    {
        return $this -> hasMany ( DateSlotEvents::class, 'date_slot_id' ) -> with ( 'volunteer' );
    }

    /**
     * @param $value
     * @return Carbon
     */
    public function getDateSlotsAttribute ( $value )
    {
        if ( Request ::segment ( 1 ) == 'trains' && Request ::segment ( 3 ) == 'update_calendar' ) return $value;
        return Carbon ::parse ( $value );
    }

    /**
     * @param $request
     * @param $meal
     * @return mixed
     */
    public static function insert_date_slots ( $request, $meal )
    {
        $rt = json_decode ( $request -> dateslots, true );
        foreach ( $rt as $order ) {
            $slot = self ::create ( [
                'dateslots' => $order[ 'dateslots' ],
                'mt_pack_id' => $meal -> id,
            ] );
            if ( isset( $order[ 'events' ] ) && count ( $order[ 'events' ] ) > 0 ) {
                foreach ( $order[ 'events' ] as $event )
                    DateSlotEvents ::create ( [ 'date_slot_id' => $slot -> id,
                        'category' => $event[ 'category' ],
                        'needed' => $event[ 'needed' ]
                    ] );
            }
        }
        $meal -> load ( 'dateSlots' );
        return $meal;
    }

}
