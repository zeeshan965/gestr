<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class InviteUser extends Model
{
    protected $table = 'meal_recipients';

    /**
     * @var string[]
     */
    protected $fillable = [ 'unique_id', 'email', 'book_meal_id', 'body' ];

    /**
     * @return BelongsTo
     */
    public function meal ()
    {
        return $this -> belongsTo ( BookMeal::class, 'book_meal_id' );
    }

    /**
     * @param $request
     * @param $id
     * @return mixed
     */
    public static function insert_recepient ( $request, $id )
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $inviteUser = self ::create ( [
            'unique_id' => md5 ( uniqid ( rand (), true ) ),
            'book_meal_id' => $meal -> id,
            'body' => $request -> Body ?? '',
            'email' => $request -> To ?? ''
        ] );
        $inviteUser -> load ( 'meal' );
        return $inviteUser;
    }
}
