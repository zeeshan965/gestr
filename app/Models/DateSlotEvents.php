<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Request;

class DateSlotEvents extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'date_slot_id', 'category', 'needed'
    ];

    /**
     * @param $value
     * @return string
     */
    public function getCategoryAttribute ( $value )
    {
        switch ( $value ) {
            case 1:
                return 'Meal';
            case 2:
                return 'Groceries';
            case 3:
                return 'Help';
            case 4:
                return 'Visit';
            case 5:
                return 'Ride';
            case 6:
                return 'Child Care';
            case 7:
                return 'Communication';
        }
    }

    /**
     * @param $value
     * @return int|string
     */
    public static function getCategory ( $value )
    {
        return self ::categoryByString ( $value );
    }

    /**
     * @param $value
     * @return int
     */
    private static function categoryByString ( $value )
    {
        switch ( $value ) {
            case 'Meal':
                return 1;
            case 'Groceries':
                return 2;
            case 'Help':
                return 3;
            case 'Visit':
                return 4;
            case 'Ride':
                return 5;
            case 'Child Care':
                return 6;
            case 'Communication':
                return 7;
            default:
                return self ::categoryByNumber ( $value );
        }
    }

    /**
     * @param $value
     * @return string
     */
    private static function categoryByNumber ( $value )
    {
        return intval ( $value );
    }

    /**
     * @return BelongsTo
     */
    public function dateSlot ()
    {
        return $this -> belongsTo ( MealDateSlots::class, 'date_slot_id' );
    }

    /**
     * @return HasOne
     */
    public function volunteer ()
    {
        return $this -> hasOne ( Volunteer::class, 'event_id' ) -> with ( 'user' );
    }
}
