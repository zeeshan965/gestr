<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Recipient extends Model
{
    /**
     * @var string[]
     */
    protected $guarded = [ 'id' ];

    /**
     * @return BelongsTo
     */
    public function meal ()
    {
        return $this -> belongsTo ( BookMeal::class, 'book_meal_id' );
    }

    /**
     * @return BelongsTo
     */
    public function user ()
    {
        return $this -> belongsTo ( User::class, 'user_id' );
    }

    /**
     * @param $email
     * @param $meal_id
     * @param $user_id
     * @param $status
     * @return mixed
     */
    public static function insert_recipient ( $email, $meal_id, $user_id, $status )
    {
        return self ::updateOrCreate (
            [ 'book_meal_id' => $meal_id, 'email' => $email, 'user_id' => $user_id ],
            [ 'unique_id' => md5 ( uniqid ( rand (), true ) ), 'status' => $status ] );
    }
}
