<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $guarded = [ 'id' ];

    /**
     * Get the parent attachable_id model (user or post).
     */
    public function attachable ()
    {
        return $this -> morphTo ();
    }
}
