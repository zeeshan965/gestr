<?php

namespace App\Providers;

use App\Support\Helpers;
use Illuminate\Support\ServiceProvider;

class HelperServicProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register ()
    {
        $this -> app -> bind ( 'helper', Helpers::class );
    }
}
