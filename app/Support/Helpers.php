<?php

namespace App\Support;

use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Spatie\IcalendarGenerator\Components\Calendar;
use Spatie\IcalendarGenerator\Components\Event;
use Spatie\IcalendarGenerator\Properties\TextProperty;

/**
 * Class Helpers
 * @package App\Support
 */
class Helpers
{
    /**
     * @param $request
     * @return mixed
     */
    public function upload_attachment ( array $request ) : string
    {
        $fileName = time () . '_' . $request[ 'files' ] -> getClientOriginalName ();
        $path = $request[ 'files' ] -> move ( public_path ( '/uploads' ), $fileName );
        return $path -> getFilename ();
    }

    /**
     * @param $name
     * @param $description
     * @param $location
     * @param $start
     * @param $end
     * @param $unique_id
     */
    public function saveEvent ( $name, $description, $location, $start, $end, $unique_id )
    {
        $event = Event ::create () -> name ( $name ) -> description ( $description ) -> uniqueIdentifier ( $unique_id )
            -> createdAt ( Carbon ::now () -> toDateTime () ) -> startsAt ( new \DateTime( $start ) ) -> endsAt ( new \DateTime( $end ) )
            -> address ( $location ) -> withoutTimezone ();
        return Calendar ::create ()
            -> name ( "Gestr for {$name}" )
            -> productIdentifier ( 'Gestr v1.0' )
            -> appendProperty ( TextProperty ::create ( 'METHOD', 'PUBLISH' ) )
            -> withoutAutoTimezoneComponents ()
            -> event ( $event ) -> get ();
    }

    /**
     * @param $users
     * @param $mailAble
     */
    public function send_email ( $users, $mailAble )
    {
        foreach ( $users as $email ) {
            Mail ::to ( $email ) -> send ( $mailAble );
        }
        dd (222);
    }
}

