<?php

namespace App\Http\Controllers;

use App\Facades\Helper;
use App\Mail\SendInviteMailable;
use App\Mail\SendMessageMailable;
use App\Models\Attachment;
use App\Models\BookMeal;
use App\Models\InviteUser;
use App\Models\Participants;
use App\Models\Recipient;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class TrainsController extends Controller
{
    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function fetch_events ( $id, Request $request )
    {
        $meal = BookMeal ::with ( 'dateSlots', 'recipients' ) -> where ( 'unique_id', $id ) -> first ();
        if ( $meal -> package_payment == 'Plus' ) $dates = BookMeal ::fetchPlusDateslots ( $id, $meal -> dateSlots );
        else $dates = BookMeal ::fetchDateslots ( $id, $meal -> dateSlots );
        return response () -> json ( [ 'status' => 'success', 'data' => $dates ], 200 );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function all_events ( $id, Request $request )
    {
        $meal = BookMeal ::with ( 'allSlots', 'recipients' ) -> where ( 'unique_id', $id ) -> first ();
        if ( $meal -> package_payment == 'Plus' ) $dates = BookMeal ::fetchPlusDateslots ( $id, $meal -> allSlots );
        else $dates = BookMeal ::fetchDateslots ( $id, $meal -> allSlots );
        return response () -> json ( [ 'status' => 'success', 'data' => $dates ], 200 );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function trains ( $id, Request $request )
    {
        $intro = $request -> has ( 'intro' );
        $meal = BookMeal ::with ( 'dateSlots', 'oldDateSlots', 'recipients', 'attachment' ) -> where ( 'unique_id', $id ) -> first ();
        $user = $meal -> user;
        //Train Detail page is public, whenever the new user open it up we will add those new comers to participants
        if ( Auth ::check () && Auth ::user () -> email !== $user -> email )
            Participants ::insert_participant ( $meal, Auth ::user (), 1 );
        $url = url ( 'trains', [ $meal -> unique_id ] );
        // dd($meal->users->where('id', Auth::user() -> id));
        return view ( 'trains', compact ( 'meal', 'intro', 'user', 'url' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function disable_tips ( $id, Request $request )
    {
        $meal = BookMeal :: where ( 'unique_id', $id ) -> first ();
        $meal -> intro = 1;
        $meal -> update ();
        return response () -> json ( [ 'status' => 'success' ], 200 );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function upload ( $id, Request $request )
    {
        $validator = $this -> validator ( $request -> all () );
        $message = $validator -> getMessageBag () -> get ( 'files' )[ 0 ] ?? '';
        if ( $validator -> fails () )
            return response () -> json ( [ 'status' => 'error', 'messages' => $message ], 500 );

        $fileModel = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $path = Helper ::upload_attachment ( $request -> all () );
        $fileModel -> attachment () -> save ( new Attachment( [ 'url' => $path ] ) );
        //$fileModel -> update ( [ 'photo' => $path -> getFilename () ] );
        return response () -> json ( [ 'status' => 'success', 'data' => url ( "/uploads/{$path}" ) ], 200 );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function remove ( $id, Request $request )
    {
        $participant = Participants ::where ( 'book_meal_id', $request -> post ( 'id' ) ) -> first ();
        $meal = BookMeal ::with ( 'volunteers' ) -> find ( $request -> post ( 'id' ) );
        foreach ( $meal -> volunteers as $volunteer ) {
            if ( $volunteer -> user_id == Auth ::id () || $volunteer -> assignedUser -> email == Auth ::user () -> email ) {
                $volunteer -> delete ();
            }
        }
        $participant -> delete ();
        return redirect () -> to ( '/dashboard' );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function remove_photo ( $id, Request $request )
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $meal -> attachment () -> delete ();
        return response () -> json ( [ 'status' => 'success' ], 200 );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function activate_train ( $id, Request $request )
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $url = url ( 'trains', [ $meal -> unique_id ] );
        $invitation_body = "This is the Gestr page for {$meal -> recipient_name}: \n {$url}";
        $invitation_body .= "\n What is Gestr? \n There are times in our lives when friends and family ask, \"What can I do to help out?\" The answer is usually to help them with a meal. When many friends provide support through a meal, Gestr keeps everyone organized.";
        $invitation_body .= "\n Gestr.com is a free meal calendar tool that makes planning meals among a wide group easy and less stressful.";
        $invitation_body .= "\n Please share this email with others.";
        $invitation_body = rawurlencode ( nl2br ( $invitation_body ) );
        return view ( 'activate_train', compact ( 'meal', 'id', 'invitation_body' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function invite ( $id, Request $request )
    {
        $user = Auth ::user ();
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $url = url ( 'trains', [ $meal -> unique_id ] );
        $subject = "{$user -> first_name} {$user -> last_name} has invited you to join the Gestr page for {$meal -> recipient_name}";
        $invitation_body = $subject;
        $invitation_body .= "\nWhat is Gestr? \nWhen a friend is in need, everyone asks \"What can I do to help?\" The answer is always to provide support through a meal. When many friends provide support through a meal, Gestr keeps everyone organized.";
        $invitation_body .= "\nGestr.com is a free meal calendar tool that makes planning meals among a wide group easy and less stressful.";
        $invitation_body .= "\nPlease share this email with others.";
        return view ( 'invite', compact ( 'meal', 'id', 'subject', 'invitation_body' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function message ( $id, Request $request )
    {
        $meal = BookMeal ::with ( 'meal_recipient' ) -> where ( 'unique_id', $id ) -> first ();
        if ( $request -> has ( 'user' ) ) $email = User ::find ( $request -> get ( 'user' ) ) -> email ?? '';
        if ( $request -> has ( 'meal' ) ) {
            $meal_recipient = $meal -> meal_recipient -> where ( 'id', $request -> get ( 'meal' ) ) -> first ();
            $email = $meal_recipient -> email ?? '';
        }
        if ( $request -> has ( 'participant' ) ) {
            $meal_participant = $meal -> recipients -> where ( 'id', $request -> get ( 'participant' ) ) -> first ();
            $email = $meal_participant -> email ?? '';
        }
        return view ( 'message', compact ( 'email', 'id', 'meal' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function participant_message ( $id, Request $request )
    {
        $meal = BookMeal ::with ( 'meal_recipient' ) -> where ( 'unique_id', $id ) -> first ();
        $email = '';
        $participant_subject = '';
        $participant_body = '';
        if ( $request -> has ( 'type' ) && $request -> get ( 'type' ) == 'others' ) {
            $message_type = 'others';
            $participant_subject = Auth::user()->first_name.' has invited you to join the Meal Train page for '.$meal -> recipient_name;
            $participant_body = 
$participant_subject.'

What is Meal Train?
When a friend is in need, everyone asks "What can I do to help?" The answer is always to provide support through a meal. When many friends provide support through a meal, Meal Train keeps everyone organized.

MealTrain.com is a free meal calendar tool that makes planning meals among a wide group easy and less stressful.

Please share this email with others.';
        } else {
            $message_type = 'all';
        }
        
        return view ( 'send_participant_message', compact ( 'email', 'id', 'meal', 'message_type', 'participant_subject', 'participant_body' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function invite_confirm ( $id, Request $request )
    {
        $user = Auth ::user ();
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $url = url ( 'trains', [ $meal -> unique_id ] );
        $subject = "{$user -> first_name} {$user -> last_name} has invited you to join the Gestr page for {$meal -> recipient_name}";
        $invitation_body = $subject;
        $invitation_body .= "\nWhat is Gestr? \nWhen a friend is in need, everyone asks \"What can I do to help?\" The answer is always to provide support through a meal. When many friends provide support through a meal, Gestr keeps everyone organized.";
        $invitation_body .= "\nGestr.com is a free meal calendar tool that makes planning meals among a wide group easy and less stressful.";
        $invitation_body .= "\nPlease share this email with others.";
        return view ( 'invite_confirm', compact ( 'meal', 'id', 'subject', 'invitation_body' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function message_confirm ( $id, Request $request )
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        return view ( 'message_confirm', compact ( 'meal', 'id' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function send_invite ( $id, Request $request )
    {
        $user = Auth ::user ();
        $data = $request -> all ();
        $data[ 'user' ] = $user;
        $data[ 'id' ] = $id;
        $inviteUser = InviteUser ::insert_recepient ( $request, $id );
        $data[ 'inviteUser' ] = $inviteUser -> unique_id;
        $data[ 'meal' ] = $inviteUser -> meal;
        Mail ::to ( $data[ 'To' ] ) -> send ( new SendInviteMailable( $data ) );
        return redirect () -> to ( "trains/{$id}/invite/confirm" );
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function send_message ( $id, Request $request )
    {
        $data = $request -> all ();
        $data[ 'id' ] = $id;
        $data[ 'meal' ] = BookMeal ::with ( 'user' ) -> where ( 'unique_id', $id ) -> first ();
        if ( $data[ 'meal' ] -> user -> email != $request -> post ( 'email' ) ) $users[] = $request -> post ( 'email' );
        if ( $request -> has ( 'CC' ) && $request -> post ( 'CC' ) == true ) $users[] = $data[ 'meal' ] -> user -> email;
        foreach ( $users as $user ) {
            $data[ 'email' ] = $user;
            Mail ::to ( $user ) -> send ( new SendMessageMailable( $data ) );
        }
        return redirect () -> to ( "trains/{$id}/message/confirm" );
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function send_participant_message ( $id, Request $request )
    {
        $data = $request -> all ();
        $data[ 'id' ] = $id;
        $data[ 'meal' ] = BookMeal ::with ( 'user' ) -> where ( 'unique_id', $id ) -> first ();

        if( $request -> get ( 'message_type' ) == 'others' ){
            $users[] = $request -> post ( 'email' );
        } else {
            $book_meal = $data[ 'meal' ];
            $original_recipients = $book_meal -> get_all_meal_recipients ();
            $meal_recipients = $book_meal -> recipients () -> groupBy ( 'meal_recipients.email' ) -> get ();

            $users[] = $book_meal -> user -> email;
            foreach ($original_recipients as $recipient) {
                $users[] = $recipient -> email;
            }
            foreach ($meal_recipients as $recipient) {
                $users[] = $recipient -> email;
            }
        }
        if ( $request -> has ( 'CC' ) && $request -> post ( 'CC' ) == true ) $users[] = $data[ 'meal' ] -> user -> email;        
        foreach ( $users as $user ) {
            $data[ 'email' ] = $user;
            Mail ::to ( $user ) -> send ( new SendMessageMailable( $data ) );
        }

        return redirect () -> to ( "trains/{$id}/message/confirm" );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function edit_story ( $id, Request $request )
    {
        $validator = Validator ::make ( $request -> all (), [
            'body' => 'required'
        ] );
        $message = $validator -> getMessageBag () -> get ( 'body' )[ 0 ] ?? '';
        if ( $validator -> fails () )
            return response () -> json ( [ 'status' => 'error', 'messages' => $message ], 500 );

        $model = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $model -> body = $request -> input ( 'body' );
        $model -> save ();
        return response () -> json ( [ 'status' => 'success', 'message' => $model -> body ], 200 );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function edit_calendar ( $id, Request $request )
    {
        $meal = BookMeal ::with ( 'dateSlots' ) -> where ( 'unique_id', $id ) -> first ();
        return view ( 'edit_calendar', compact ( 'meal', 'id' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function print ( $id, Request $request )
    {
        $meal = BookMeal ::with ( 'dateSlots', 'oldDateSlots', 'recipients', 'attachment' ) -> where ( 'unique_id', $id ) -> first ();
        $user = $meal -> user;
        $url = url ( 'trains', [ $meal -> unique_id ] );
        return view ( 'print_trains', compact ( 'meal', 'user', 'url' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function edit ( $id, Request $request )
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $url = url ( "/trains/{$id}" );
        return view ( 'edit_train', compact ( 'meal', 'url' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function organizers ( $id, Request $request ) : JsonResponse
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $organizers = BookMeal ::fetch_organizers ( $meal );
        return response () -> json ( [ 'status' => 'success', 'data' => $organizers ] );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function participants ( $id, Request $request ) : JsonResponse
    {
        $book_meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        // dd($meal);
        $participants = BookMeal ::fetch_participants ( $book_meal );
        return response () -> json ( [ 'status' => 'success', 'data' => $participants ] );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function remove_participants ( $id, Request $request ) : JsonResponse
    {
        $participant = InviteUser ::find ( $request -> post ( 'participant_id' ) );
        $status = $participant -> delete ();
        return response () -> json ( [ 'status' => 'success', 'data' => $status ] );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function create_organizer ( $id, Request $request ) : JsonResponse
    {
        if ( ! $request -> has ( 'organizer_email' ) )
            return response () -> json ( [ 'status' => 'error', 'message' => 'organizer_email is required' ], 500 );

        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $user = User ::where ( 'email', $request -> post ( 'organizer_email' ) ) -> first ();
        if ( $user === null ) return response () -> json ( [ 'status' => 'error' ], 400 );

        $recipient = Recipient ::insert_recipient ( $request -> organizer_email, $meal -> id, $user -> id, 0 );
        return response () -> json ( [ 'status' => 'success', 'data' => $recipient ] );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function make_primary ( $id, Request $request ) : JsonResponse
    {
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        if( $request -> post ( 'organizer_id' ) == 0 ) {
            Recipient::where('book_meal_id', $meal->id)->update(['organizer_status' => 0]);
            \DB::table('book_meal_users')->where('book_meal_id', $meal->id)->delete();
        } else {
            $organizer = Recipient ::find ( $request -> post ( 'organizer_id' ) );
            
            $meal->users()->sync($organizer->user->id);
            $organizer->update(['organizer_status' => 1]);
        }
        return response () -> json ( [ 'status' => 'success', 'data' => $meal ] );
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function remove_organizer ( $id, Request $request ) : JsonResponse
    {
        $organizer = Recipient ::find ( $request -> post ( 'organizer_id' ) );
        $status = $organizer -> delete ();
        return response () -> json ( [ 'status' => 'success', 'data' => $status ] );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function update ( $id, Request $request )
    {
        $validator = $this -> train_validator ( $request -> all () );
        if ( $validator -> fails () )
            return response () -> json ( [ 'status' => 'error', 'messages' => $validator -> getMessageBag () -> toArray () ], 500 );

        try {
            $meal = BookMeal ::update_meal ( $id, $request -> except ( [ '_token' ] ) );
            return response () -> json ( [ 'status' => 'success', 'data' => $meal -> toArray () ] );

        } catch ( Exception $ex ) {
            return response () -> json ( [ 'status' => 'failed', 'error' => 'Error saving meal information, Redirecting to Home' ] );
        }

    }

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function train_validator ( array $data )
    {
        return Validator ::make ( $data, [
            'recipient_name' => 'required|string|max:255', 'recipient_email' => 'required|string|email|max:255',
            'recipient_address' => 'required', 'recipient_city' => 'required', 'recipient_state' => 'required',
            'recipient_postalcode' => 'required', 'recipient_phone' => 'required', 'adults_cook_for' => 'required',
            'kids_cook_for' => 'required', 'delivery_time' => 'required', 'special_instructions' => 'required',
            'fave_meals_rest' => 'required', 'least_fave_meals' => 'required', 'restrictions' => 'required',
        ] );

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator ( array $data )
    {
        return Validator ::make ( $data, [
            'files' => 'required|mimes:jpeg,jpg,png,gif,bmp'
        ] );
    }
}
