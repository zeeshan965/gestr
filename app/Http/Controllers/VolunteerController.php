<?php

namespace App\Http\Controllers;

use App\Facades\Helper;
use App\Models\DateSlotEvents;
use App\Models\MealDateSlots;
use App\Models\Volunteer;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class VolunteerController extends Controller
{
    /**
     * @param $id
     * @param $post_id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function volunteer ( $id, $slotId, Request $request )
    {
        $event_id = $request -> has ( 'event_id' ) ? base64_decode ( $request -> get ( 'event_id' ) ) : null;
        $event = DateSlotEvents ::find ( $event_id );
        $slot_id = base64_decode ( $slotId );
        $slot = MealDateSlots ::with ( 'meal' ) -> find ( $slot_id );
        $meal = $slot -> meal;
        $url = url ( "trains/{$id}/volunteer/{$slotId}" );
        $date = Carbon ::now () -> format ( 'M d, Y' );
        $volunteer = null;
        $v_id = null;
        return view ( 'volunteer', compact ( 'meal', 'id', 'slotId', 'slot', 'url', 'date', 'volunteer', 'v_id', 'event' ) );
    }

    /**
     * @param $id
     * @param $slot_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function store ( $id, $slot_id, Request $request )
    {
        $volunteer = Volunteer ::insert_data ( $id, $slot_id, $request, Auth ::user () );
        $url = "trains/{$id}/volunteer/{$slot_id}/confirm/{$volunteer -> unique_id}";
        return redirect () -> to ( $url );
    }

    /**
     * @param $id
     * @param $slot_id
     * @param $v_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function confirm_volunteer ( $id, $slot_id, $v_id, Request $request )
    {
        $volunteer = Volunteer ::where ( 'unique_id', $v_id ) -> first ();
        $event = DateSlotEvents ::find ( $volunteer -> event_id );
        $meal = $volunteer -> dateSlot != null ? $volunteer -> dateSlot -> meal : null;
        $url = url ( "trains/{$id}/volunteer/{$slot_id}/calendar_invite/{$v_id}" );
        $google_invite = $this -> prepareInviteLink ( $meal, $id, $volunteer, $event );
        return view ( 'volunteer_confirm', compact ( 'google_invite', 'meal', 'volunteer', 'id', 'url', 'event' ) );
    }

    /**
     * @param $meal
     * @param $id
     * @param $volunteer
     * @param $event
     * @return string
     */
    private function prepareInviteLink ( $meal, $id, $volunteer, $event ) : string
    {
        $first_date = $meal -> allSlots -> first () -> dateslots -> format ( 'Ymd' );
        $last_date = $meal -> allSlots -> last () -> dateslots -> format ( 'Ymd' );
        $website = url ( "/trains/{$id}" );
        $need_type = $event == null ? 'Meal' : ucfirst ( $event -> category );
        $body = $event == null ? $volunteer -> body : "{$event -> needed} \n Description: {$volunteer -> body}";
        $google_invite = "https://www.google.com/calendar/event?action=TEMPLATE&text=" .
            rawurlencode ( nl2br ( "Gestr for {$meal -> recipient_name}" ) ) . "&dates=" .
            rawurlencode ( nl2br ( "{$first_date}/{$last_date}" ) ) . "&sprop=name:gestr.com&sprop=website:{$website}&details=" .
            rawurlencode ( nl2br ( "You are scheduled to provide {$need_type} to \n {$meal -> recipient_name} \n " ) ) .
            rawurlencode ( nl2br ( "Phone: {$meal -> recipient_phone} \n " ) ) .

            rawurlencode ( nl2br ( "{$need_type}: {$body} \n Note: {$volunteer -> note} \n\n " ) ) .
            rawurlencode ( nl2br ( "To see all the details of this Gestr follow this url: \n {$website}" ) );
        //rawurlencode ( nl2br ( "&location={$meal -> recipient_address} {$meal -> recipient_city} " ) ) .
        //rawurlencode ( nl2br ( "{$meal -> recipient_state} {$meal -> recipient_postal_code}&trp=false" ) );
        return $google_invite;
    }

    /**
     * @param $id
     * @param $slot_id
     * @param $v_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function calendar_invite ( $id, $slot_id, $v_id, Request $request )
    {
        $volunteer = Volunteer ::with ( 'dateSlot' ) -> where ( 'unique_id', $v_id ) -> first ();
        $event = DateSlotEvents ::find ( $volunteer -> event_id );
        $meal = $volunteer -> dateSlot -> meal;
        $name = $meal -> recipient_name;
        $phone = $meal -> recipient_phone;
        $location = "{$meal -> recipient_address} {$meal -> recipient_city} {$meal -> recipient_state} {$meal -> recipient_postal_code}";
        $start = $meal -> allSlots -> first () -> dateslots -> format ( 'Ymd' );
        $end = $meal -> allSlots -> last () -> dateslots -> format ( 'Ymd' );
        $unique_id = md5 ( uniqid ( rand (), true ) );
        $url = url ( "trains/{$id}" );
        $need_type = $event == null ? 'Meal' : ucfirst ( $event -> category );
        $body = $event == null ? $volunteer -> body : "{$event -> needed} \n Description: {$volunteer -> body}";
        $description = "You are scheduled to provide a {$need_type} to \n {$name} \n Phone: {$phone} \n {$need_type}: {$body} \n " .
            "Note: {$volunteer -> note} \n\n To see all the details of this Gestr follow this url: \n {$url}";
        $calendar = Helper ::saveEvent ( $name, $description, $location, $start, $end, $unique_id );
        return response ( $calendar, 200, [ 'Content-Type' => 'text/calendar',
            'Content-Disposition' => 'attachment; filename="Gestr_' . $v_id . '.ics"',
            'charset' => 'utf-8',
        ] );
    }

    /**
     * @param $id
     * @param $slotId
     * @param $v_id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function edit_volunteer ( $id, $slotId, $v_id, Request $request )
    {
        $event_id = $request -> has ( 'event_id' ) ? base64_decode ( $request -> get ( 'event_id' ) ) : null;
        $event = DateSlotEvents ::find ( $event_id );
        $volunteer = Volunteer ::with ( 'dateSlot', 'user' ) -> where ( 'unique_id', $v_id ) -> first ();
        $slot = $volunteer -> dateSlot;
        $meal = $slot -> meal;
        $slot_id = base64_decode ( $slotId );
        $url = url ( "trains/{$id}/volunteer/{$slotId}/update/{$v_id}" );
        $date = Carbon ::now () -> format ( 'M d, Y' );
        return view ( 'volunteer', compact ( 'meal', 'id', 'slotId', 'slot', 'url', 'date', 'volunteer', 'v_id', 'event' ) );
    }

    /**
     * @param $id
     * @param $slot_id
     * @param $v_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update_volunteer ( $id, $slot_id, $v_id, Request $request )
    {
        Volunteer ::update_data ( $id, $slot_id, $v_id, $request, Auth ::user () );
        $url = "trains/{$id}/volunteer/{$slot_id}/confirm/{$v_id}";
        return redirect () -> to ( $url );
    }

    /**
     * @param $id
     * @param $slot_id
     * @param $v_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete_volunteer ( $id, $slot_id, $v_id, Request $request )
    {
        $volunteer = Volunteer ::where ( 'unique_id', $v_id ) -> first ();
        $event_id = $request -> post ( 'event_id' );
        if ( $event_id == null ) {
            $dateSlot = MealDateSlots ::where ( 'id', base64_decode ( $slot_id ) ) -> first ();
            $dateSlot -> delete ();
        } else {
            $event = $volunteer -> dateSlot -> events () -> find ( $event_id );
            $event -> delete ();
            if ( $volunteer -> dateSlot -> events -> count () === 1 )
                MealDateSlots ::where ( 'id', base64_decode ( $slot_id ) ) -> first () -> delete ();
        }
        $volunteer -> dateSlot () -> dissociate () -> save ();
        $volunteer -> delete ();
        //Mail ::to ( $user[ 'email' ] ) -> send ( new SendPostUpdateMailable( $data ) );
        return redirect () -> to ( "trains/{$id}" );
    }

    /**
     * @param $id
     * @param $slot_id
     * @param $v_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function cancel_booking ( $id, $slot_id, $v_id, Request $request )
    {
        $volunteer = Volunteer ::where ( 'unique_id', $v_id ) -> first ();
        $volunteer -> delete ();
        //Mail ::to ( $user[ 'email' ] ) -> send ( new SendPostUpdateMailable( $data ) );
        return redirect () -> to ( "trains/{$id}" );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator ( array $data )
    {
        return Validator ::make ( $data, [
            'files' => 'required|mimes:jpeg,jpg,png,gif,bmp'
        ] );
    }
}
