<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return View
     */
    public function create ()
    {
        return view ( 'auth.register' );
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator ( array $data )
    {
        return Validator ::make ( $data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => [ 'required', 'confirmed', Rules\Password ::defaults () ],
        ] );
    }

    /**
     * Handle an incoming registration request.
     *
     * @param Request $request
     * @return RedirectResponse
     *
     */
    public function store ( Request $request )
    {
        if ( $request -> ajax () ) {
            $validator = $this -> validator ( $request -> all () );
            if ( $validator -> fails () )
                return response () -> json ( [ 'status' => 'error', 'messages' => $validator -> getMessageBag () -> toArray () ], 500 );
        } else {
            $request -> validate ( [
                'first_name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => [ 'required', 'confirmed', Rules\Password ::defaults () ],
            ] );
        }

        $user = User ::create ( [
            'first_name' => $request -> first_name,
            'last_name' => $request -> last_name,
            'email' => $request -> email,
            'password' => Hash ::make ( base64_decode ( $request -> password ) ),
        ] );
        event ( new Registered( $user ) );
        Auth ::login ( $user );
        if ( $request -> ajax () ) {
            return response () -> json ( [ 'status' => 'success', 'data' => Auth ::user (),
                'intended' => URL ::to ( RouteServiceProvider::HOME ), 'csrf' => csrf_token () ] );
        } else {
            return redirect ( RouteServiceProvider::HOME );
        }

    }
}
