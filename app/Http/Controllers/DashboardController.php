<?php

namespace App\Http\Controllers;

use App\Models\BookMeal;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke ( Request $request )
    {
        $user = Auth ::user ();
        $shared_meal_ids = $user->book_meals->pluck('id')->toArray();
        // $shared_user_id = $meals[0]->users->where('id', $user -> id) ? $meals[0]->users->where('id', $user -> id)->id : '';
        $meals = BookMeal ::with ( [ 'user', 'dateSlots' ] ) -> where ( 'user_id', $user -> id ) -> orWhereIn('id', $shared_meal_ids)
            -> orWhereHas ( 'participants', function ( $q ) use ( $user ) {
                $q -> where ( 'user_id', $user -> id );
                $q -> where ( 'email', $user -> email );
            } ) -> orderByDesc ( 'id' ) -> get ();
        // dd($meals);
        // dd($meals[0]->users()->attach([2]));
        // dd($meals[0]->users->where('id', $user -> id), in_array($user->id, $shared_meal_ids), $user->id, $shared_meal_ids);
        return view ( 'dashboard', compact ( 'meals', 'user', 'shared_meal_ids' ) );
    }
}
