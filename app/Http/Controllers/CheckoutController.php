<?php

namespace App\Http\Controllers;

use App\Models\BookMeal;
use App\Models\DateSlotEvents;
use App\Models\MealDateSlots;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Date;

class CheckoutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke ( Request $request )
    {
        return view ( 'checkout' );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function basic_plan_register ( Request $request )
    {
        return view ( 'basic_plan_register' );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function plus_plan_register ( Request $request )
    {
        return view ( 'plus_plan_register' );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store ( Request $request )
    {
        if ( ! $request -> has ( 'dateslots' ) ) return response () -> json ( [ 'status' => 'failed', 'error' => 'Incomplete Parameters' ] );
        try {
            $meal = BookMeal ::insert_meal ( $request );
            $meal = MealDateSlots ::insert_date_slots ( $request, $meal );
            return response () -> json ( [ 'status' => 'success', 'data' => $meal -> toArray () ] );

        } catch ( Exception $ex ) {
            return response () -> json ( [ 'status' => 'failed', 'error' => 'Error saving meal information, Redirecting to Home' ] );
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update_calendar ( $id, Request $request )
    {
        $meal = BookMeal ::with ( 'allSlots' ) -> where ( 'unique_id', $id ) -> first ();
        $dateSlots = $meal -> allSlots -> pluck ( 'dateslots' );
        $data = json_decode ( $request -> input ( 'data' ) );
        if ( $meal -> package_payment == 'Plus' ) {
            //Plus Plan logic
            $data = [];
            foreach ( json_decode ( $request -> input ( 'data' ) ) as $item ) {
                array_push ( $data, $item -> dateslots );
            }
            foreach ( $data as $item ) {
                $slot = MealDateSlots ::firstOrCreate ( [ 'dateslots' => $item, 'mt_pack_id' => $meal -> id ] );
                $slots = json_decode ( $request -> input ( 'data' ), true );
                $find = array_search ( $item, array_column ( $slots, 'dateslots' ) );
                if ( $find != - 1 && $find !== false ) {
                    if ( isset( $slots[ $find ][ 'events' ] ) && count ( $slots[ $find ][ 'events' ] ) > 0 ) {
                        foreach ( $slots[ $find ][ 'events' ] as $event ) {
                            DateSlotEvents ::firstOrCreate ( [
                                'date_slot_id' => $slot -> id,
                                'category' => DateSlotEvents ::getCategory ( $event[ 'category' ] ),
                                'needed' => $event[ 'needed' ]
                            ] );
                        }
                    }
                }
            }
        } else {
            foreach ( $dateSlots as $dateSlot ) if ( ! in_array ( $dateSlot, $data ) ) {
                $slot = $meal -> allSlots -> where ( 'dateslots', $dateSlot ) -> first ();
                //Extra check to make sure if volunteer we will not delete the slot
                if ( $slot -> volunteer == null ) $slot -> delete ();
            }
            foreach ( $data as $item ) {
                if ( ! in_array ( $item, $dateSlots -> toArray () ) ) {
                    $meal -> allSlots () -> create ( [ 'dateslots' => $item ] );
                }
            }
        }

        return redirect () -> route ( 'trains', [ $id ] );
    }
}
