<?php

namespace App\Http\Controllers;

use App\Models\BookMeal;
use App\Models\MealPost;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MealPostsController extends Controller
{
    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function updates ( $id, Request $request )
    {
        $intro = $request -> has ( 'intro' );
        $meal = BookMeal ::with ( 'recipients', 'posts.user', 'user' ) -> where ( 'unique_id', $id ) -> first ();
        $user = $meal -> user;
        $url = url ( 'trains', [ $meal -> unique_id ] );

        return view ( 'updates', compact ( 'meal', 'intro', 'user', 'url' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function new_updates ( $id, Request $request )
    {
        $user = Auth ::user ();
        $meal = BookMeal ::where ( 'unique_id', $id ) -> first ();
        $url = url ( "trains/{$id}/updates/store" );
        $date = Carbon ::now () -> format ( 'M d, Y' );
        return view ( 'new_update', compact ( 'meal', 'id', 'user', 'url', 'date' ) );
    }

    /**
     * @param $id
     * @param $post_id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function edit_post ( $id, $post_id, Request $request )
    {
        $user = Auth ::user ();
        $post = MealPost ::with ( 'meal' ) -> where ( 'unique_id', $post_id ) -> first ();
        $meal = $post -> meal;
        $url = url ( "trains/{$id}/updates/{$post_id}/update" );
        $date = Carbon ::now () -> format ( 'M d, Y' );
        return view ( 'new_update', compact ( 'meal', 'id', 'user', 'url', 'post_id', 'date', 'post' ) );
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function store_updates ( $id, Request $request )
    {
        $mealPost = MealPost ::insert_post ( $id, $request, Auth ::user () );
        $url = "trains/{$id}/updates/{$mealPost -> unique_id}/confirm";
        return redirect () -> to ( $url );
    }

    /**
     * @param $id
     * @param Request $request
     * @return RedirectResponse
     */
    public function post_update ( $id, Request $request )
    {
        MealPost ::update_post ( $id, $request, Auth ::user () );
        $url = "trains/{$id}/updates";
        return redirect () -> to ( $url );
        //return redirect () -> back () -> withInput ();
    }

    /**
     * @param $id
     * @param Request $request
     * @return Application|Factory|View
     */
    public function update_confirm ( $id, $post_id, Request $request )
    {
        $post = MealPost ::with ( 'meal' ) -> where ( 'unique_id', $post_id ) -> first ();
        $meal = $post -> meal ?? null;
        $url = url ( "trains/{$id}" );
        return view ( 'update_confirm', compact ( 'meal', 'id', 'url', 'post_id', 'post' ) );
    }

    /**
     * @param $id
     * @param $post_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete_update ( $id, $post_id, Request $request )
    {
        $post = MealPost ::where ( 'unique_id', $post_id ) -> first ();
        $post -> delete ();
        return redirect () -> to ( "trains/{$id}/updates" );
    }
}
