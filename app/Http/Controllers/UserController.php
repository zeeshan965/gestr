<?php

namespace App\Http\Controllers;

use App\Mail\ChangePasswordMailable;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules;

class UserController extends Controller
{

    /**
     * @return Application|Factory|View
     */
    public function profile ()
    {
        $user = Auth ::user ();
        return view ( 'profile', compact ( 'user' ) );
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function update ( Request $request )
    {
        $user = User ::find ( Auth ::id () );
        $user -> first_name = $request -> post ( 'first_name' );
        $user -> last_name = $request -> post ( 'last_name' );
        $user -> save ();
        return redirect () -> back () -> with ( 'message', 'Your profile updated successfully!' );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function update_password ( Request $request )
    {
        $validator = Validator ::make ( $request -> all (), [
            'password' => [ 'required', 'confirmed', Rules\Password ::defaults () ],
        ] );
        if ( $validator -> fails () )
            return response () -> json ( [ 'status' => 'error', 'messages' => $validator -> getMessageBag () -> toArray () ], 500 );
        $user = User ::find ( Auth ::id () );
        $password = base64_decode ( $request -> currentPassword );
        if ( ! Hash ::check ( $password, $user -> password ) )
            return response () -> json ( [ 'status' => 'error', 'messages' => 'Current password is not correct!' ], 500 );
        if ( Hash ::check ( base64_decode ( $request -> password ), $user -> password ) )
            return response () -> json ( [ 'status' => 'error', 'messages' => 'Please choose different password' ], 500 );
        $user -> password = Hash ::make ( base64_decode ( $request -> password ) );
        $user -> save ();
        Mail ::to ( $user -> email ) -> send ( new ChangePasswordMailable( $user ) );
        return response () -> json ( [ 'status' => 'success', 'message' => 'Password changed successfully' ] );
    }
}
