<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string upload_attachment( $request )
 * @method static string saveEvent( $request )
 * @method static string send_email( $users, $data )
 * Class Helper
 * @package App\Facades
 */
class Helper extends Facade
{
    protected static function getFacadeAccessor () : string
    {
        return 'helper';
    }
}
